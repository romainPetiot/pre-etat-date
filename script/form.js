if (document.forms.namedItem("createUser")) {
    document.forms.namedItem("createUser").addEventListener('submit', function(e) {
        document.getElementById('createUserSubmitBtn').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("createUser");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', ped_js.resturl + 'createUser', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('createUserSubmitBtn').disabled = false;
                type_ped = document.getElementById("type_ped").value;
                if (type_ped == "assist") {
                    location.href = ped_js.formAssist;
                } else if (type_ped == "full") {
                    location.href = ped_js.form2url;
                } else if (type_ped == "coordonnees") {
                    location.href = ped_js.formCoordonateLight;
                } else {
                    location.href = ped_js.carturl;
                }
            }
        };
        xhr.send(formData);
    }); 
}

if (document.getElementsByClassName('required-alt').length){
    var els = document.getElementsByClassName('required-alt');
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("blur", function(e) {
            var requiredAlt;
            if(el.value !== ''){
                requiredAlt = el.dataset.requiredAlt;
                document.getElementById(requiredAlt).required = false;
            }
            else{
                requiredAlt = el.dataset.requiredAlt;
                document.getElementById(requiredAlt).required = true;
                el.required = false;
            }
        });

        if(el.value !== ''){
            requiredAlt = el.dataset.requiredAlt;
            document.getElementById(requiredAlt).required = false;
        }
        else{
            
            requiredAlt = el.dataset.requiredAlt;
            document.getElementById(requiredAlt).required = true;
            el.required = false;
        }
    });         
}

if (document.forms.namedItem("createCoordonate")) {

    document.forms.namedItem("createCoordonate").addEventListener('submit', function(e) {
        document.getElementById('createCoordonateSubmitBtn').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("createCoordonate"); 
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', ped_js.resturl + 'createCoordonate', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('createCoordonateSubmitBtn').disabled = false;
                location.href = ped_js.form3url;
            }
        };
        xhr.send(formData);
    });
}
if (document.forms.namedItem("createCoordonateLight")) {
    document.forms.namedItem("createCoordonateLight").addEventListener('submit', function(e) {
        document.getElementById('createCoordonateSubmitBtn').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("createCoordonateLight");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', ped_js.resturl + 'createCoordonateLight', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('createCoordonateSubmitBtn').disabled = false;
                location.href = ped_js.carturl;
            }
        };
        xhr.send(formData);
    });
}


if (document.forms.namedItem("createBilan")) {
    document.forms.namedItem("createBilan").addEventListener('submit', function(e) {
        document.getElementById('createBilanSubmitBtn').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("createBilan");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', ped_js.resturl + 'createBilan', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('createBilanSubmitBtn').disabled = false;
                location.href = ped_js.form4url;
            }
        };
        xhr.send(formData);
    });
}

if (document.forms.namedItem("createAnnex")) {
    document.forms.namedItem("createAnnex").addEventListener('submit', function(e) {
        document.getElementById('createAnnexSubmitBtn').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("createAnnex");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', ped_js.resturl + 'createAnnex', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('createAnnexSubmitBtn').disabled = false;
                location.href = ped_js.form5url;
            }
        };
        xhr.send(formData);
    });
}

if (document.forms.namedItem("formAssist")) { 
    
    document.forms.namedItem("formAssist").addEventListener('submit', function(e) {
        document.getElementById('formAssistSubmitBtn').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("formAssist");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest(); 
        xhr.open('POST', ped_js.resturl + 'formAssist', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('formAssistSubmitBtn').disabled = false;
                //console.log(xhr.response);
                location.href = ped_js.carturl; 
            }
        };
        xhr.send(formData);
    });

    document.forms.namedItem("formAssistSyndic").addEventListener('submit', function(e) {
        document.getElementById('formAssistSyndicSubmitBtn').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("formAssistSyndic");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', ped_js.resturl + 'formAssistSyndic', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('formAssistSyndicSubmitBtn').disabled = false;
                console.log(xhr.response);
                location.href = ped_js.carturl;
            }
        };
        xhr.send(formData);
    });


    document.getElementById("switchForm").addEventListener('click', function(e) {
        document.getElementById("formAssist").classList.add("hide");
        document.getElementById("formAssistSyndic").classList.remove("hide");
    });
    document.getElementById("switchForm2").addEventListener('click', function(e) {
        document.getElementById("formAssist").classList.remove("hide");
        document.getElementById("formAssistSyndic").classList.add("hide");
    });
    

}



if (document.forms.namedItem("formAdmin")) {

    var debut = Date.now();
    
    document.forms.namedItem("formAdmin").addEventListener('submit', function(e) {
        //document.getElementById('formAdminSubmitBtn').disabled = true; //limit le problème des doubles clic
        e.submitter.disabled = true;
        e.preventDefault();

        var fin = Date.now();
        var timer = fin - debut;
        var form = document.forms.namedItem("formAdmin");
        var formData = new FormData(form);
        formData.append('timer', Math.round(timer/1000));
        formData.append('action', e.submitter.id); 
        var xhr = new XMLHttpRequest();
        xhr.open('POST', ped_js.resturl + 'formAdmin', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200 && e.submitter.id == "formAdminSubmitBtn") {
                e.submitter.disabled = false;
                location.href = ped_js.listAdmin; 
            }
            else if (xhr.status === 200 && e.submitter.id == "formAdminSubmitBtnVisualialiser") {
                e.submitter.disabled = false;
                var queryString = window.location.search;
                var urlParams = new URLSearchParams(queryString);
                var product_id = urlParams.get('product_id');
                var url = 'https://' + window.location.hostname + '/wp-content/uploads/pdf/pre-etat-date' + product_id + '.pdf?date='+Date.now();
                window.open(url, '_blank'); 
            }
        };
        xhr.send(formData); 
    });

    document.getElementById("cpCoordonate").addEventListener('change', function(e) {
        if(document.getElementById("cityCoordonate").value == ''){

            requestURL = 'https://datanova.laposte.fr/api/records/1.0/search/?dataset=laposte_hexasmal&q='+this.value+'&rows=1&facet=nom_de_la_commune';
            request = new XMLHttpRequest();
            request.open('GET', requestURL);
            request.responseType = 'json';
            request.send();
            
            request.onload = function() {
              const superHeroes = request.response;
              document.getElementById('cityCoordonate').value = superHeroes.records[0].fields.nom_de_la_commune;
              if(document.getElementById('cityOwner1').value == ''){
                document.getElementById('cityOwner1').value = superHeroes.records[0].fields.nom_de_la_commune;
              }
            }
            
        }
    });

    document.addEventListener("DOMContentLoaded", coherence());
}
function coherence (){
    document.getElementById('check').value = "false";
    diff = diffCoherence(document.getElementById('montantAppelDecompteChargeN1').value, document.getElementById('montantDepenseDecompteChargeN1').value, 15);
    document.getElementById('diffBugetN1Ask').innerHTML = diff;

    diff = diffCoherence(document.getElementById('budgetN1').value, document.getElementById('depenseN1').value, 15);
    document.getElementById('diffBugetN1Real').innerHTML = diff;

    diff = diffCoherence(document.getElementById('montantAppelDecompteChargeN2').value, document.getElementById('montantDepenseDecompteChargeN2').value, 15);
    document.getElementById('diffBugetN2Ask').innerHTML = diff;

    diff = diffCoherence(document.getElementById('budgetN2').value, document.getElementById('depenseN2').value, 15);
    document.getElementById('diffBugetN2Real').innerHTML = diff;

    document.getElementById('AlertDettesCopro').innerHTML = '<span style="color:green">OK</span>';
    if(document.getElementById('dettesCopro').value == 0){
        document.getElementById('AlertDettesCopro').innerHTML = '<span style="color:red">Impayés copro</span>';
        document.getElementById('check').value = "true";
    }

    document.getElementById('AlertDettesFournisseur').innerHTML = '<span style="color:green">OK</span>';
    if(document.getElementById('dettesFournisseurs').value == 0){
        document.getElementById('AlertDettesFournisseur').innerHTML = '<span style="color:red">Dettes fournisseurs</span>';
        document.getElementById('check').value = "true";
    }
    

    document.getElementById('AlertDettesFournisseur').innerHTML = '<span style="color:green">OK</span>';
    if(document.getElementById('dettesFournisseurs').value == 0){
        document.getElementById('AlertDettesFournisseur').innerHTML = '<span style="color:red">Dettes fournisseurs</span>';
        document.getElementById('check').value = "true";
    }

    diff = diffCoherence((document.getElementById('montantAppelDecompteChargeN2').value * 4), (document.getElementById('montantDepenseDecompteChargeN2').value / 6), 10);
    document.getElementById('AlertAvanceTreso').innerHTML = diff;
    
    document.getElementById('AlertFondsTravaux').innerHTML = '<span style="color:green">OK</span>';
    var nb = formule_condit(document.getElementById('montantFondsAlurADF').value, document.getElementById('montantFondsAlurACP').value);
	var appelFondsAlur = document.getElementById('appelFondsAlur').value;
	if((nb + appelFondsAlur) > 0){
		if(nb == 0 || appelFondsAlur == 0){
            document.getElementById('AlertFondsTravaux').innerHTML = '<span style="color:red">Fonds Travaux</span>';
            document.getElementById('check').value = "true";
        }
	}
}

function diffCoherence (val1, val2, equart){
    if(val1 == 0){val1=0.01;}
    if(val2 == 0){val2=0.01;}
    var diff =  Math.round((val1-val2)/val2*100);
    var color = 'green';
    if(diff > equart || diff < (equart * -1)){
        var color = 'red'
        document.getElementById('check').value = "true";
    }
    return '<span style="color:'+color+'">'+diff+' %</span>';
}

function formule_condit(val1, val2){
	if(val1 > 0){
		return val1;
	}
	else if(val2 == 0){
		return 0;
	}
	return val2 / document.getElementById('Tantiemesgeneraux').value * (
		document.getElementById('tantiemeCoordonate').value + 
		document.getElementById('tantiemeCoordonate2').value + 
		document.getElementById('tantiemeCoordonate3').value + 
		document.getElementById('tantiemeCoordonate4').value + 
		document.getElementById('tantiemeCoordonate5').value + 
		document.getElementById('tantiemeCoordonate6').value + 
		document.getElementById('tantiemeCoordonate7').value );
}

if (document.forms.namedItem("formAdminFilePDF")) {
    document.forms.namedItem("formAdminFilePDF").addEventListener('submit', function(e) {
        document.getElementById('formAdminFilePDFSubmitBtn').disabled = true; //limit le problème des doubles clic
        e.preventDefault();
        var fin = Date.now();
        var timer = fin - debut;
        var form = document.forms.namedItem("formAdminFilePDF");
        var formData = new FormData(form);
        formData.append('timer', Math.round(timer/1000));
        xhr = new XMLHttpRequest();
        xhr.open('POST', ped_js.resturl + 'formAdminFilePDF', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('formAdminFilePDFSubmitBtn').disabled = false;

                //history.go(-1);
                location.href = ped_js.listAdmin;
                //console.log(xhr.response);
            }
        };
        xhr.send(formData);
    });
}
if (document.forms.namedItem("formAdminConfirm")) {
    document.forms.namedItem("formAdminConfirm").addEventListener('submit', function(e) {
        console.log(e);
        //document.getElementById('formAdminConfirmSubmitBtn').disabled = true; //limit le problème des doubles clic
        e.preventDefault();
        var form = document.forms.namedItem("formAdminConfirm");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', ped_js.resturl + 'formAdminConfirm', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                //document.getElementById('formAdminConfirmSubmitBtn').disabled = false;

                //history.go(-1);
                location.href = ped_js.listAdmin; 
                //console.log(xhr.response);
            }
        };
        xhr.send(formData);
    });
}
if (document.forms.namedItem("formAdminAsk")) {
    document.forms.namedItem("formAdminAsk").addEventListener('submit', function(e) {

        document.getElementById('formAdminAskSubmitBtn').disabled = true; //limit le problème des doubles clic
        e.preventDefault();
        
        var form = document.forms.namedItem("formAdminAsk");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', ped_js.resturl + 'formAdminAsk', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('formAdminAskSubmitBtn').disabled = false;
                location.reload();
                //console.log(xhr.response); 
            }
        };
        xhr.send(formData);
    });
    

    var els = document.getElementsByClassName('formAdminAskBtn');
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) {
            e.preventDefault();
            document.getElementById("email-title").value = el.dataset.title;
            document.getElementById("email-body").value = el.dataset.message;
        });
    });
    
}

// Show - hide landlord names on form-coordonnate.php
// 1 - Button "Add landlord"

var count = 2;
var prefix = "owner";
var prevent = 0;

if (document.getElementById("moreOwner")) {
    document.getElementById("moreOwner").addEventListener('click', function(e) {

        e.preventDefault();
        var ownername = prefix.concat(count);

        // Il existe des propriétaires à afficher
        if (count >= 2) {

            document.getElementById("lessOwner").classList.remove("hidden");

            if (prevent > 0) {
                // Nombre maximum de propriétaire atteint : on fait disparaitre le bouton
                alert("10 propriétaires maximum");
                prevent = 0;
            }

            if (count < 11) {
                // les propriétaires apparaissent
                document.getElementById(ownername).classList.remove("hidden");
                document.getElementById("moreOwner").classList.remove("disabled");
                count++;
            }

            if (count === 11) {
                // Nombre maximum de propriétaire atteint : on fait disparaitre le bouton
                document.getElementById("moreOwner").classList.add("disabled");
                document.getElementById(ownername).classList.remove("hidden");
                count = 12;
            }

            if (count > 11) {
                // On prévient les erreurs : on réinitialise la valeur count
                prevent++;
                count = 12;
            }

        } else {
            // Il y a une erreur, on réinitialise la variable
            alert("Erreur");
            count = 2;
        }


    });
}

// 2 - Button "Remove landlord"

if (document.getElementById("lessOwner")) {
    document.getElementById("lessOwner").addEventListener('click', function(e) {

        e.preventDefault();

        // Il existe des propriétaires à afficher
        if (count >= 2) {

            count--;
            var ownername = prefix.concat(count);
            document.getElementById("lessOwner").classList.remove("hidden");

            if (count == 2) {
                // On a un seul propriétaire : on cache le bouton "lessOwner" 
                document.getElementById("lessOwner").classList.add("hidden");
                //count = 3;
            }

            if (count <= 10) {
                // les propriétaires disparaissent
                document.getElementById(ownername).classList.add("hidden");
                document.getElementById("firstnameOwner" + count).value = "";
                document.getElementById("lastnameOwner" + count).value = "";
            }

            if (count > 10) {
                // En cas d'erreur : on prévient l'utilisateur qu'il ne peut pas ajouter davantage de propriétaires
                count = 10;
                document.getElementById(prefix.concat(10)).classList.add("hidden");
                document.getElementById("moreOwner").classList.remove("disabled");
            }

        } else {
            // Il y a une erreur, on réinitialise la variable
            alert("Erreur");
            count = 2;
        }

    });
}


if (document.forms.namedItem("creaPreEtatDateRest")) {
    document.forms.namedItem("creaPreEtatDateRest").addEventListener('submit', function(e) {

        e.preventDefault();

        var form = document.forms.namedItem("creaPreEtatDateRest");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', ped_js.resturl + 'creaPreEtatDateRest', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                console.log(xhr.response);
            }
        };
        xhr.send(formData);
    });
}

if (document.forms.namedItem("contactForm")) {
    document.forms.namedItem("contactForm").addEventListener('submit', function(e) {
        document.getElementById('sendMessage').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("contactForm");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', ped_js.resturl + 'contactForm', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendMessage').disabled = false;
                document.getElementById('sendMessage').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessage').classList.add("showResponseMessage");
            }
        };
        xhr.send(formData);
    });
}

if (document.getElementsByClassName('menu-admin-ped').length){
    var els = document.getElementsByClassName('menu-admin-ped');
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) {
            var els2 = document.getElementsByClassName('elm-admin-ped');
            Array.prototype.forEach.call(els2, function(el2) {
                el2.style.display = 'none';
            });
            document.getElementById(el.dataset.showId).style.display = 'block';
        });
    });
}


if (document.forms.namedItem("formAdminMessageOrder")) {
    document.forms.namedItem("formAdminMessageOrder").addEventListener('submit', function(e) {

        e.preventDefault();

        var form = document.forms.namedItem("formAdminMessageOrder");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', ped_js.resturl + 'formAdminMessageOrder', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                console.log(xhr.response);
                location.reload();
            }
        };
        xhr.send(formData);
    });
}

if (document.forms.namedItem("formAdminAddFile")) {
    document.forms.namedItem("formAdminAddFile").addEventListener('submit', function(e) {

        e.preventDefault();
        document.getElementById('formAdminCorrectSubmitBtn').disabled = true;
        document.getElementById('formAdminCorrectSubmitBtn').value="Attendre ...";
        var form = document.forms.namedItem("formAdminAddFile");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', ped_js.resturl + 'formAdminAddFile', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('formAdminCorrectSubmitBtn').disabled = false;
                document.getElementById('formAdminCorrectSubmitBtn').value="Envoyer";
                //console.log(xhr.response);
                location.reload();
            }
        };
        xhr.send(formData);
    });
}


if (document.forms.namedItem("formAdminInstruction")) {
    document.forms.namedItem("formAdminInstruction").addEventListener('submit', function(e) {
        e.preventDefault();
        var form = document.forms.namedItem("formAdminInstruction");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', ped_js.resturl + 'formAdminInstruction', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                console.log(xhr.response);
                location.reload();
            }
        };
        xhr.send(formData);
    });
}
if (document.forms.namedItem("formAdminCreatePEDManual")) {
    document.forms.namedItem("formAdminCreatePEDManual").addEventListener('submit', function(e) {
        e.preventDefault();
        var form = document.forms.namedItem("formAdminCreatePEDManual");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', ped_js.resturl + 'formAdminCreatePEDManual', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                the_get = xhr.response.replaceAll('"', '');
                //console.log(the_get);
                location.href = ped_js.PEDAdmin+'?'+the_get;
            }
        };
        xhr.send(formData);
    });
}
function search_users_with_price(val){
    
    Array.prototype.forEach.call(users_with_price, function(user_with_price) {
        console.log(user_with_price.user +' - '+ val);
        if(user_with_price.user == val){
            document.getElementById('ped-price').value = user_with_price.price;
        }
    });
}

if (document.forms.namedItem("formAdminRedactor")) {
    document.forms.namedItem("formAdminRedactor").addEventListener('submit', function(e) {
        e.preventDefault();
        var form = document.forms.namedItem("formAdminRedactor");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', ped_js.resturl + 'formAdminRedactor', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                console.log(xhr.response);
                location.reload();
            }
        };
        xhr.send(formData);
    });
}


function copyValue(value, target){
    el = document.getElementById(target);
    if(el.value == ''){
        el.value = value;
    }
}