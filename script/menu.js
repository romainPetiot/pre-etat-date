function toggleMenuMobile() {
  var menuClasses = document.getElementById("menu-mobile").classList;
  if (menuClasses.contains("menu-open")) {
    menuClasses.remove("menu-open");
  } else {
    menuClasses.add("menu-open");
  }
}
