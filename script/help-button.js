// Open & Close menu (for mobile devices)
function togglePopup() {
    document.getElementById("popup").classList.toggle("is-open");
}

// When we click outside of the box, popup disappears
// We use #overlay to detect the click
document.addEventListener("DOMContentLoaded", function(event) {
    if (document.getElementById("overlay")) {
        document.getElementById("overlay").addEventListener("click", function(e) {
            togglePopup();
        });
    }

    if (document.getElementsByClassName('read_more').length){
        var elements = document.getElementsByClassName('read_more');

        for (var i = 0; i < elements.length; i++) {
            elements[i].addEventListener('click', (function(i) {
                return function() {
                    this.parentNode.classList.toggle('readMore');
                };
            })(i), false);
        }        
    }
});


