<?php

get_header();
?>

	<div class="frontpage"><!-- .front-page -->

		<div id="primary">
			<main id="main">

			<?php
			while ( have_posts() ) :
				the_post();
			?>

				<!-- Bloc 1 - Frontpage-Header -->
				<section class="frontpage-header bloc-has-2-parts">
					<!-- Bloc 1 - Part 1 -->
					<div class="frontpage-header-part1 yellow-background ">
						<div class="image-container part-1 desktop-half-wrapper">
							<?php
								$image = get_field('home-1-img');
								$size = 'visuel-home'; // (thumbnail, medium, large, full or custom size)
								if( $image ) {
									echo wp_get_attachment_image( $image, $size );
								}
							?>
						</div>
					</div>
					<!-- Bloc 1 - Part 2 -->
					<div class="part-2 frontpage-header-part2 desktop-half-wrapper responsive-padding ">
						<h1><?php the_field('home-1-title');?></h1>
						<h2 class="baseline"><?php the_field('home-1-content');?></h2>
						<a class="cta-standard" href="<?php the_field('home-1-link-bouton');?>"><?php the_field('home-1-label-bouton');?></a>
					</div>
				</section>
				<!-- Bloc 1 - Frontpage-Header -->

				<!-- Bloc 2 - Frontpage-Advantage -->
				<section class="frontpage-advantage bloc-has-2-parts">
					<!-- Bloc 2 - Part 1 -->
					<div class="frontpage-advantage-part1 part-1 desktop-half-wrapper responsive-padding  wrapper-defines-height ">
						<h3 class="h1-like"><?php the_field('home-2-title');?></h3>
						<?php the_field('home-2-content');?>
						<?php 
							$buttonSection2 =  get_field('home-2-link-bouton');
							if ($buttonSection2) { ?>
								<a class="cta-standard" href="<?php echo $buttonSection2;?>"><?php the_field('home-2-label-bouton');?></a>
						<?php 
						}?>
					</div>
					<!-- Bloc 2 - Part 1 -->
					<!-- Bloc 2 - Part 2 -->
					<div class="frontpage-advantage-part2 part-2 desktop-half-wrapper responsive-padding ">
						<?php
							while ( have_rows('home-2-items') ) : the_row();
							?>
							<div class="frontpage-advantage-element">
								<?php
								$image = get_sub_field('picto');
								$size = 'picto'; // (thumbnail, medium, large, full or custom size)
								if( $image ) {
									echo wp_get_attachment_image( $image, $size , false, array('class' => 'frontpage-advantage-picture'));
								}
								?>
								<div>
									<h3 class="frontpage-advantage-name"><?php the_sub_field('title');?></h3>
									<p class="frontpage-advantage-text"><?php the_sub_field('content');?></p>
								</div>
							</div>
							<?php
							endwhile;
						?>
					</div>
				</section>
				<!-- Bloc 2 - Frontpage-Advantage -->

				<!-- Bloc 3 - Frontpage-Help -->
				<section class="frontpage-help bloc-has-2-parts">
					<!-- Bloc 3 - Part 1 -->
					<div class="frontpage-help-part1 part-1 desktop-half-wrapper responsive-padding  wrapper-defines-height">
						<h2 class="h1-like"><?php the_field('home-3-title');?></h2>
					</div>
					<!-- Bloc 3 - Part 2 -->
					<div class="part-2 desktop-half-wrapper responsive-padding  wrapper-defines-height">
						<div class="frontpage-help-part2">
							<?php the_field('home-3-content');?>
						</div>
					</div>
				</section>
				<!-- Bloc 3 - Frontpage-Help -->

				<!-- Bloc 4 - Frontpage-Timeline -->
				<section class="frontpage-timeline wrapper wrapper-defines-height">
					<!-- Bloc 4 - Title -->
					<h2 class="h1-like"><?php the_field('home-4-title');?></h2>
					<!-- Bloc 4 - Timeline list-->
					<ol class="part-1 frontpage-timeline-list">
						<?php $ind=1; while ( have_rows('home-4-steps') ) : the_row(); ?>
							<li class="frontpage-timeline-step">
								<p class="small-text">Étape <?php echo $ind++;?></p>
								<h3 class="frontpage-timeline-title"><?php the_sub_field('title');?></h3>
								<?php the_sub_field('content');?>
							</li>
						<?php endwhile;?>
					</ol>
				</section>
				<!-- Bloc 4 - Frontpage-Timeline -->

				<!-- Bloc 5 - Frontpage-Help -->
				<section class="frontpage-help bloc-has-2-parts">
					<!-- Bloc 5 - Part 1 -->
					<div class="frontpage-help-part1 part-1 desktop-half-wrapper responsive-padding  wrapper-defines-height">
						<h2 class="h1-like"><?php the_field('home-5-title');?></h2>
					</div>
					<!-- Bloc 5 - Part 2 -->
					<div class="part-2 desktop-half-wrapper responsive-padding  wrapper-defines-height">
						<div class="frontpage-help-part2">
							<?php the_field('home-5-content');?>
							<?php if(!empty(get_field('home-5-label-bouton'))):?>
							<a class="cta-white" href="<?php the_field('home-5-link-bouton');?>"><?php the_field('home-5-label-bouton');?></a>
							<?php endif;?>
						</div>
					</div>
				</section>
				<!-- Bloc 5 - Frontpage-Help -->

				<!-- Bloc 6 - Frontpage-Price -->
				<section class="frontpage-price grey-background" id="frontpage-price">
					<div class="wrapper">
						<!-- Bloc 6 - Title -->
						<h2 class="h1-like"><?php the_field('home-6-title');?></h2>
						<!-- Bloc 6 - Price container-->
						<div class="frontpage-price-container">
							<!--Price - Element 1 -->
							<article class="frontpage-price-element">
								<!-- Element 1 - Header -->
								<header class="frontpage-price-header">
									<div class="frontpage-price-title">
										<img class="frontpage-price-title" src="<?php echo get_stylesheet_directory_uri(); ?>/image/pre-etat-date-icon.svg" alt="Pré-état daté">	
										<h2><?php the_field('home-6-title-plan1');?></h2>
									</div>
									<em class="frontpage-price-data"><?php the_field('home-6-price-plan1');?></em>
									<p class="frontpage-price-text ">prix unique<p>
								</header>
								<!-- Element 1 : Option -->
								<div class="frontpage-price-option">
									<ul class="frontpage-price-list">
									<?php while ( have_rows('home-6-arguments-plan1') ) : the_row(); ?>
										<li class="frontpage-price-arg"><?php the_sub_field('content');?></li>
									<?php endwhile;?>
									</ul>
									<?php 
									$buttonPlan1 =  get_field('home-6-link-bouton-plan1');
									if ($buttonPlan1) { ?>
										<a class="cta-standard" href="<?php echo $buttonPlan1;?>"><?php the_field('home-6-label-plan1');?></a>
									<?php 
									}?>
								</div>
							</article>
							<!--Price - Element 1 -->
							<!--Price - Element 2 -->
							<article class="frontpage-price-element">
								<!-- Element 2 - Header -->
								<header class="frontpage-price-header">
									<div class="frontpage-price-title">
										<img class="frontpage-price-title" src="<?php echo get_stylesheet_directory_uri(); ?>/image/pre-etat-date-icon.svg" alt="Pré-état daté">	
										<h2><?php the_field('home-6-title-plan2');?></h2>
									</div>
									
									<em class="frontpage-price-data"><?php the_field('home-6-price-plan2');?></em>
									<p class="frontpage-price-text ">prix unique<p>
								</header>
								<!-- Element 2 : Option -->
								<div class="frontpage-price-option">
									<ul class="frontpage-price-list">
									<?php while ( have_rows('home-6-arguments-plan2') ) : the_row(); ?>
										<li class="frontpage-price-arg"><?php the_sub_field('content');?></li>
									<?php endwhile;?>
									</ul>
									<?php 
									$buttonPlan2 =  get_field('home-6-link-bouton-plan2');
									if ($buttonPlan2) { ?>
										<a class="cta-standard cta-plan-2" href="<?php echo $buttonPlan2;?>"><?php the_field('home-6-label-plan2');?></a>
									<?php 
									}?>
								</div>
							</article>
							<!--Price - Element 2 -->
							<!--Price - Element 3 -->
							<article class="frontpage-price-element">
								<!-- Element 3 - Header -->
								<header class="frontpage-price-header">
									<div class="frontpage-price-title">
										<h2><?php the_field('home-6-title-syndic');?></h2>
									</div>
									<em class="frontpage-price-data"><?php the_field('home-6-price-syndic');?></em>
									<p class="frontpage-price-text ">prix en moyenne*<p>
								</header>
								<!-- Element 3 : Option -->
								<div class="frontpage-price-option">
									<ul class="frontpage-price-list">
										<?php while ( have_rows('home-6-arguments-against') ) : the_row(); ?>
											<li class="frontpage-price-arg"><?php the_sub_field('content');?></li>
										<?php endwhile;?>
									</ul>
								</div>
							</article><!--Price - Element 3 -->
						</div><!-- .frontpage-price-container -->
						<div class="frontpage-price-legal center"><?php the_field('home-6-source');?></div>

					</div><!--.wrapper -->
				</section>

				<!-- Bloc 7 - Frontpage-Help -->
				<section class="frontpage-help bloc-has-2-parts">
					<!-- Bloc 7 - Part 1 -->
					<div class="frontpage-help-part1 part-1 desktop-half-wrapper responsive-padding  wrapper-defines-height">
						<h2 class="h1-like"><?php the_field('home-7-title');?></h2>
					</div>
					<!-- Bloc 7 - Part 2 -->
					<div class="part-2 desktop-half-wrapper responsive-padding  wrapper-defines-height">
						<div class="frontpage-help-part2">
							<?php the_field('home-7-content');?>
						</div>
					</div>
				</section>
				<!-- Bloc 7 - Frontpage-Help -->
				<!-- Testimonials -->
				<?php get_template_part('template-parts/bloc', 'testimonial'); ?>
				<!-- Testimonials -->

			<?php endwhile; // End of the loop.?>
			</main><!-- #main -->
		</div><!-- #primary -->

	</div><!-- .front-page -->

<?php
get_footer();
