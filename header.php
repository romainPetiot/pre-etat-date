<?php
/**
 * The header for our theme
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Susty
 */
$display_menu_partner = false;
if(is_cart() || is_checkout() || is_page()){
	global $woocommerce;
	$items = $woocommerce->cart->get_cart();
	
	foreach($items as $item => $values) { 
		$id_ped = get_post_meta($values['data']->get_id(), '_id_ped', true);
		if(get_field("footer", $id_ped) == "empty"){
			//include('footer-partner.php');
			$display_menu_partner = true;
		}
	} 	
}
elseif(get_post_type() == "ped"){
	if(get_field("footer", $post->ID) == "empty"){
		$id_ped = $post->ID;
		$display_menu_partner = true;
	}
}

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/image/favicon.png" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Accéder au contenu', 'susty' ); ?></a>
	<a class="skip-link screen-reader-text" href="#skip-to-menu"><?php esc_html_e( 'Accéder au menu', 'susty' ); ?></a>
	<!-- Topbar Header -->
	<header id="masthead">
		<div class="wrapper">
			<nav class="menu-topbar">
				<!-- Custom Logo -->
				<div class="custom-logo">
					<?php
					if($display_menu_partner):
						?>
						<a href="<?php echo get_the_permalink($id_ped); ?>" title="Accueil du partenaire">
						<?php
						$image = get_field('logo_partner', $id_ped);
						$size = 'logo'; // (thumbnail, medium, large, full or custom size)
						if( $image ) :
							echo wp_get_attachment_image( $image, $size );
						else:
						?>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/logo-pre-etat-date.svg" alt="Pr&eacute;-&eacute;tat-dat&eacute; : Simple, rapide et en ligne !">
					
						<?php
						endif;?>
						</a>
						<?php
					else:
					?>
					<a id="skip-to-menu" href="<?php echo get_home_url() ?>" title="Accueil du site">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/logo-pre-etat-date.svg" alt="Pr&eacute;-&eacute;tat-dat&eacute; : Simple, rapide et en ligne !">
					</a>
					<?php
					endif;
					?>
				</div>
				<!-- End of Custom Logo -->
				<!-- Topbar Menu -->
				<?php
				if(!$display_menu_partner):
				?>
				<div id="menu">
					<button id="menu-button-open" onclick="toggleMenuMobile()" class="desktop-hidden tablet-hidden">
						<div class="button-burger">
							<span class="button-line horizontal-line"></span>
							<span class="button-line horizontal-line"></span>
							<span class="button-line horizontal-line"></span>
						</div>
						<label>Menu</label>
					</button>
					<!-- Menu Mobile -->
					<div id="menu-mobile">
						<button  id="menu-button-close" onclick="toggleMenuMobile()" class="desktop-hidden tablet-hidden">
							<div class="button-square">
								<span class="button-line diagonal-line"></span>
								<span class="button-line diagonal-line"></span>
							</div>
							<label>Fermer</label>
						</button>
						<div>
							<?php
								echo ihag_menu('primary');
							?>
						</div>
					</div>
					<!-- End of Menu Mobile -->
					<!-- Call to action - Homepage only -->
					<?php
						if ( is_front_page() ) :
							
							?>
							<nav class="topbar-cta">
								<a href="<?php echo get_field("intro", "option"); ?>" ><?php esc_html_e( 'Commencer', 'susty' ); ?></a>
							</nav>
							<?php 
							
						endif;
					?>
					<!-- End of Call to action - Homepage only -->
				</div><!-- #menu -->
				<?php
					endif;
				?>
			</nav><!-- .menu-topbar -->
		</div><!-- .wrapper -->
	</header>
	<!-- End of Topbar Header -->

	<div id="content" tabindex="-1">
<?php
