<?php
 
 
add_action('wp_enqueue_scripts', 'site_scripts', 999);
function site_scripts()
{
    global $wp_styles;

    //wp_deregister_script('jquery');

    wp_enqueue_script('script', get_stylesheet_directory_uri() . '/script.js', false, THEME_VERSION, 'all');
    
    wp_localize_script('script','ped_js',array(
      'resturl'   => get_bloginfo('url') . '/wp-json/ped/',
      'carturl'   => wc_get_cart_url(),
      //'form1url'   => get_field('form-email', 'option'),
      'form2url'   => get_field('form-coordonate', 'option'),
      'form3url'   => get_field('form-bilan', 'option'),
      'form4url'   => get_field('form-annexes', 'option'),
      'form5url'   => get_field('form-recap', 'option'),
      'listAdmin'   => get_field('page_admin_ped_listing', 'option'),
      'PEDAdmin'   => get_field('page_admin_ped', 'option'),
      'formAssist'   => get_field('form-assist', 'option'),
      'formCoordonateLight'   => get_field('form-coordonate-light', 'option'),
    ) );
    
    wp_localize_script( 'script', 'wpApiSettings', array(
		'root' => esc_url_raw( rest_url() ), 
		'nonce' => wp_create_nonce( 'wp_rest' )
	) );
    
}