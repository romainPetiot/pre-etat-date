<?php

get_header();
?>

<div class="standard-page">

	<div class="page-title narrow-wrapper">
		<?php if(is_singular()):?>
			<?php the_title( '<h1 class="print">', '</h1>' ); ?>
		<?php elseif(is_archive()):?>
			<h1 class="print"><?php echo single_cat_title( '', false );?></h1>
		<?php endif;?>
	</div>

	<div id="primary" class="narrow-wrapper">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

</div><!-- .standard-page -->

<?php
get_footer();
