<?php

add_action('rest_api_init', function() {
	

	register_rest_route( 'ped', 'formAdmin', 
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'formAdmin',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
	register_rest_route( 'ped', 'formAdminFilePDF', 
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'formAdminFilePDF',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
	register_rest_route( 'ped', 'formAdminConfirm', 
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'formAdminConfirm',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
	register_rest_route( 'ped', 'formAdminAsk', 
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'formAdminAsk',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
	register_rest_route( 'ped', 'formAdminMessageOrder', 
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'formAdminMessageOrder',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
	register_rest_route( 'ped', 'formAdminAddFile', 
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'formAdminAddFile',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
	register_rest_route( 'ped', 'formAdminInstruction', 
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'formAdminInstruction',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
	register_rest_route( 'ped', 'formAdminCreatePEDManual', 
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'formAdminCreatePEDManual',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
	register_rest_route( 'ped', 'formAdminRedactor', 
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'formAdminRedactor',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
	
});


add_action('add_meta_boxes','initialisation_metaboxes');
function initialisation_metaboxes(){
	add_meta_box('id_ma_meta', 'Actions Pré-état daté', 'SetPED', 'shop_order', 'side', 'high');
}
function SetPED($order){
	$order = new WC_Order($order->ID);
	if ( !empty($order) ) {
		$items = $order->get_items();
		foreach ( $items as $item ) {
			echo '<a class="button" href="'.get_field("page_admin_ped", "option").'?product_id='.$item->get_product_id().'&order_id='.$order->ID.'">Renseigner Pré-état daté</a>';			
			$product = new WC_Product($item->get_product_id());
			foreach ( $product->get_downloads() as $file_id => $file ) {
				echo '<a class="button" target="_BLANK" href="'.$file['file'].'">'.$file['name'].'</a>';			
			}
		}

		if($order->get_status() != 'completed'){
			echo '<p><a class="button" href="'.admin_url().'post.php?post='.$order->get_id().'&action=edit&changePayment=cheque&paymentTitle=Chèque&order='.$order->get_id().'">Paiement par cheque</a></p>';			
			echo '<p><a class="button" href="'.admin_url().'post.php?post='.$order->get_id().'&action=edit&changePayment=bacs&paymentTitle=Virement&order='.$order->get_id().'">Paiement par virement</a></p>';			
		}

	}
}

add_action('init', function() {
    if(isset($_GET['changePayment']) ){
		$order = new WC_Order($_GET['order']);
		update_post_meta($_GET['order'], '_payment_method', $_GET['changePayment']);
		update_post_meta($_GET['order'], '_payment_method_title', $_GET['paymentTitle']);
		update_post_meta($_GET['order'], '_date_paid', time());
		update_post_meta($_GET['order'], '_paid_date', date("Y-m-d H:i:s"));
		$order->update_status( 'wc-processing' );

    }   
});

add_action( 'save_post', 'save_product_generate_preetatdate', 10,3 );
function save_product_generate_preetatdate( $post_id, $post, $update ) {
    if ( !$update ){
        return;
    }
    if ( 'product' === $post->post_type ) {
        creaPreEtatDate($post_id);
    }
}

function formAdmin( WP_REST_Request $request ){
	if ( check_nonce() ) {
		$params = $request->get_params();
		$product_attributes = get_post_meta($params['product_id'], '_product_attributes', true);
		if(!is_array($product_attributes)){
			$product_attributes = [];
		}
		foreach($params as $name => $value){
			$product_attributes[] = array (
				'name' => htmlspecialchars( stripslashes( $name ) ), // set attribute name
				'value' => $value, // set attribute value
				'position' => 1,
				'is_visible' => 1,
				'is_variation' => 1,
				'is_taxonomy' => 0
			);
		}
		update_post_meta($params['product_id'], '_product_attributes', $product_attributes);

		//add_post_meta($params['product_id'], '_referral_person', $params['user_id'], true);
		creaPreEtatDate($params["product_id"]);
		
			if(get_field("send_ped_partner",$params['product_id'])){
				update_post_meta( $params['order_id'], '_billing_email', get_field("partner_email",$params['product_id']) );
			}
		
		if(isset($params['action']) && $params['action'] == "formAdminSubmitBtn"){
			formAdminStatusOrder($params);
			update_post_meta( $params['order_id'], 'remove_list_ped', 1 );
		}
		
		return new WP_REST_Response( '', 200 );
	}
}
function formAdminFilePDF( WP_REST_Request $request ){
	if ( check_nonce() ) {
		$params = $request->get_params();

		if(isset($_FILES['file_ped'])){
			$upload_dir   = wp_upload_dir();
			$uploaddirimg = $upload_dir['basedir'].'/pdf/';
			$post_id = $params['product_id'];
			if(!is_dir($uploaddirimg)){
				mkdir($uploaddirimg, 0755);
			}		
			
			$filename = $uploaddirimg.'/pre-etat-date'.$post_id.'.pdf';
			if (move_uploaded_file($_FILES['file_ped']['tmp_name'], $filename)) {
				$filename = basename($upload_dir['basedir'].'/pdf/pre-etat-date'.$post_id.'.pdf');
							echo $filename;
				$wp_filetype = wp_check_filetype($filename, null );
				$attachment = array(
					'post_mime_type' => $wp_filetype['type'],
					'post_parent' => $post_id,
					'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
					'post_content' => '',
					'post_status' => 'inherit'
				);
				$attachment_id = wp_insert_attachment( $attachment, $upload_dir['basedir'].'/pdf/pre-etat-date'.$post_id.'.pdf', $post_id );

				$file_name = "pre-etat-date.pdf";
				$file_url  = wp_get_attachment_url( $attachment_id );
				$download_id = md5( $file_url );

				global $product;
				$product = wc_get_product($post_id); 
		
				// Creating an empty instance of a WC_Product_Download object
				$pd_object = new WC_Product_Download();

				// Set the data in the WC_Product_Download object
				$pd_object->set_id( $download_id );
				$pd_object->set_name( $file_name );
				$pd_object->set_file( $file_url );

				// Get an instance of the WC_Product object (from a defined product ID)
				$product = wc_get_product( $post_id ); // <=== Be sure it's the product ID

				// Get existing downloads (if they exist)
				$downloads = [];//$product->get_downloads();

				// Add the new WC_Product_Download object to the array
				$downloads[$download_id] = $pd_object;

				// Set the complete downloads array in the product
				$product->set_downloads($downloads);
				$product->save(); // Save the data in database

			}
		}	

		formAdminStatusOrder($params);
		return new WP_REST_Response( '', 200 );
	}
}
function formAdminStatusOrder($params){
	$user = get_user_by("ID", $params['user_id']);
	$roles = ( array ) $user->roles;
	$role = $roles[0];
	$order = new IHAG_WC_Order($params['order_id']);
	$order->set_message_difficulty($params['difficulty_message']);
	if($role == "redactor"){
		$order->set_difficulty($params['difficulty_level']);
		$order->set_timer($params['timer']);
		$difficulty_level = (int) $params['difficulty_level'];
		if($difficulty_level > 0 || $params['check'] == 'true'){
			$order->set_writing_status( 'to_check' );	
		}
		else{
			$number_order_to_check = get_field("number_order_to_check", "user_".$params['user_id']);
			$rand = random_int(1, 10);
			update_post_meta($params['product_id'], '_rand_order_to_check', $rand);
			if($rand > $number_order_to_check){
				$order->set_writing_status( 'to_check' );
			}
			else{
				$time_to_check_seconde = ( (int)get_field("time_to_check", "user_".$params['user_id']) * 60);
				$timer = $params['timer'];
				if($timer > $time_to_check_seconde){
					$order->set_writing_status( 'to_check' );
				}
				else{
					$order->set_writing_status( 'completed' );
					//$order->update_status( 'completed' ); // commande passe terminée sur tache cron ihag_order_completed_func
				}				
			}
		}
	}
	else{
		//$order->update_status( 'completed' ); // commande passe terminée sur tache cron ihag_order_completed_func
		$order->set_writing_status( 'completed' );
	}	
}

function formAdminConfirm(WP_REST_Request $request){
	if ( check_nonce() ) {
		$params = $request->get_params();
		$order = new IHAG_WC_Order($params['order_id']);
		if($params['action'] == "completed"){
			$order->set_writing_status( 'completed' );
			//$order->update_status( 'completed' );
		}
		else if($params['action'] == "processing"){
			$order->set_writing_status( 'processing' );
			//$order->update_status( 'processing' );
		}
		else if($params['action'] == "to_check"){
			$order->set_writing_status( 'to_check' );
			//$order->update_status( 'proces-to-update' );
		}
		else if($params['action'] == "force_completed"){
			$order->update_status( 'completed' ); //la commande passe en completed et envoie le mail avant la tache cron
		}
		return new WP_REST_Response( $order->get_writing_status(  ), 200 );
	}
}

function formAdminAsk(WP_REST_Request $request){
	if ( check_nonce() ) {
		$params = $request->get_params();
		$order = new IHAG_WC_Order($params['order_id']);

		add_filter('wp_mail_content_type', function( $content_type ) {
			return 'text/html';
		});
		$subject = $_POST['email-title'];
		$body = nl2br($_POST['email-body']);
		$headers[] = 'From: '.get_bloginfo('name').' <'. get_option( 'admin_email') .'>'."\r\n";
		$headers[] = 'Cc: Pre-etat date en ligne <contact@pre-etat-date-en-ligne.com>';
		wp_mail( $order->get_billing_email(), $subject, $body, $headers);
		$order->set_writing_status( 'pending' );
		//$order->update_status( 'proces-wait-costu' );
		/*if($order->get_status() == 'wc-pending'){
			$order->update_status( 'wait-doc-no-pay' );
		}
		else{
			$order->update_status( 'proces-wait-costu' );
		}*/
		
		return new WP_REST_Response( '', 200 );
	}
}

function formAdminMessageOrder(WP_REST_Request $request){
	if ( check_nonce() ) {
		$params = $request->get_params();
		update_post_meta( $params['order_id'], 'message_ped', nl2br( $params['adminMessageOrder'] ) );
		return new WP_REST_Response( '', 200 );
	}
}
function formAdminInstruction(WP_REST_Request $request){
	if ( check_nonce() ) {
		$params = $request->get_params();
		update_post_meta( $params['order_id'], 'adminMessageInstruction', nl2br( $params['adminMessageInstruction'] ) );
		return new WP_REST_Response( '', 200 );
	}
}


function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}

function formAdminAddFile(WP_REST_Request $request){
	if ( check_nonce() ) {
		$params = $request->get_params();

		$tab_pdf_gallery = get_post_meta($params['product_id'], 'pdf-gallery', true );
		$tab_temp_pdf_gallery = array();
		$bool_delete = false;
		if(($tab_pdf_gallery)){
			foreach($tab_pdf_gallery as $pdf_gallery){
				if(isset($params['delete-pdf-gallery-'.$pdf_gallery]) && !empty(isset($params['delete-pdf-gallery-'.$pdf_gallery]))){
					$bool_delete = true;
					wp_delete_attachment($pdf_gallery);
				}
				else{
					$tab_temp_pdf_gallery[] = $pdf_gallery;
				}
			}
		}
		if($bool_delete){
			update_post_meta($params['product_id'], 'pdf-gallery', $tab_temp_pdf_gallery );
		}
		if(isset($_FILES)){
			if ( ! function_exists( 'wp_crop_image' ) ) {
				include_once( ABSPATH . 'wp-admin/includes/image.php' );
			}
			$upload_dir   = wp_upload_dir();
			$uploaddirimg = $upload_dir['basedir'].'/';
			if(!is_dir($uploaddirimg)){
				mkdir($uploaddirimg, 0755);
			}

			if(isset($_FILES['pdf-gallery'])){
				$files = reArrayFiles($_FILES['pdf-gallery']);
				foreach($files as $file){
					error_log($file['name']);
					$filename = $uploaddirimg . basename($file['name']);
					if (move_uploaded_file($file['tmp_name'], $filename)) {
						$wp_filetype = wp_check_filetype($filename, null );
						$attachment = array(
							'guid'           => $wp_upload_dir['url'] . '/'.basename($file['name']),
							'post_mime_type' => $wp_filetype['type'],
							'post_parent' => $post_id,
							'post_title' => preg_replace('/\.[^.]+$/', '', basename($file['name'])),
							'post_content' => '',
							'post_status' => 'inherit'
						);
						$attachment_id = wp_insert_attachment( $attachment, $filename);
	
						$tab_pdf_gallery = get_post_meta($params['product_id'], 'pdf-gallery', true );
						if(empty($tab_pdf_gallery)){$tab_pdf_gallery = array();}
						$tab_pdf_gallery[] = $attachment_id;
						update_post_meta($params['product_id'], 'pdf-gallery', $tab_pdf_gallery );
					}
				}
			}
		}
	}
}
function formAdminCreatePEDManual(WP_REST_Request $request){
	if ( check_nonce() ) {
		$params = $request->get_params();
		$email = sanitize_email($params['admin-ped-email']);
		$user = get_user_by('login', $email);
		if ($user){
			$user_id = $user->ID;
		}
		else{
			$user_id = wc_create_new_customer( $email, $email, wp_generate_password() );
		}	
		//$product_id = createProduct($user_id, $email, $params['id_ped'] );
		$post_id = wp_insert_post(array(
			'post_title' => 'manual - '.$email,
			'post_type' => 'product',
			'post_status' => 'publish',
			'post_content' => ''		
		));
		update_post_meta( $post_id, '_downloadable', 'yes' );
		update_post_meta( $post_id, '_price', $params['ped-price']  );
		update_post_meta( $post_id, '_regular_price', $params['ped-price'] );
		update_post_meta( $post_id, '_user_id', $user_id );

		global $woocommerce;


		$address = array(
			'email'      =>  $email,
		);

		$order = wc_create_order();
		$order->add_product( wc_get_product($post_id), 1); // This is an existing SIMPLE product
		$order->set_address( $address, 'billing' );
		$order->calculate_totals();
		//ajouter paiement cheque ou fin de mois
		update_post_meta( $order->get_id(), '_payment_method', 'systempaystd' ); //set payment method
		update_post_meta( $order->get_id(), '_payment_method_title', 'systempaystd' ); //set payment title

		$user_meta = get_userdata($user_id);
		$user_roles = $user_meta->roles;
		if( in_array('month_paiement', $user_roles) ){// c'est un clientr au mois
			update_post_meta( $order->get_id(), '_payment_method', 'month' ); //set payment method
			update_post_meta( $order->get_id(), '_payment_method_title', 'Paiement au mois' ); //set payment title
		}

		update_post_meta( $order->get_id(), 'relance', 'manual' ); //neutralise la relance panier abandonné
		
		//$order->update_status("processing", 'PED manuel', TRUE);  
		$order->update_status("pending", 'PED manuel', TRUE);
		 
		return new WP_REST_Response( 'product_id='.$post_id.'&order_id='.$order->get_id(), 200 );
	}
}

/*function ihag_woocommerce_order_status_completed( $order_id ) {
	global $woocommerce;
	$order = new WC_Order($order_id);
	$id_transaction = get_post_meta($order_id, 'Transaction ID', true );
	$payment_method = get_post_meta( $order->id, '_payment_method', true );
	if(empty($id_transaction) && $payment_method == "systempaystd" && $order->get_total() > 0){
		$order->update_status("pending", '', TRUE);  
	}	//Peut-être pas necessaire
}
add_action( 'woocommerce_order_status_completed', 'ihag_woocommerce_order_status_completed', 10, 1 );*/

function formAdminRedactor(WP_REST_Request $request){
	if ( check_nonce() ) {
		$params = $request->get_params();
		$order = new IHAG_WC_Order($params['order_id']);
		$order->set_referral($params['ped-redactor']);
		return new WP_REST_Response( '', 200 );
	}
}

// ADDING 2 NEW COLUMNS WITH THEIR TITLES (keeping "Total" and "Actions" columns at the end)
add_filter( 'manage_edit-shop_order_columns', 'custom_shop_order_column', 20 );
function custom_shop_order_column($columns)
{
    $reordered_columns = array();

    // Inserting columns to a specific location
    foreach( $columns as $key => $column){
        $reordered_columns[$key] = $column;
        if( $key ==  'order_status' ){
            // Inserting after "Status" column
            $reordered_columns['costumer'] = __( 'Client','ihag');
			$reordered_columns['method'] = __( 'Mode de paiement','ihag');
        }
    }
    return $reordered_columns;
}

// Adding custom fields meta data for each new column (example)
add_action( 'manage_shop_order_posts_custom_column' , 'custom_orders_list_column_content', 20, 2 );
function custom_orders_list_column_content( $column, $post_id )
{
    switch ( $column )
    {
        case 'method' :
            // Get custom post meta data
            $my_var_one = get_post_meta( $post_id, '_payment_method_title', true );
            if(!empty($my_var_one))
                echo $my_var_one;
            break;

        case 'costumer' :
            // Get custom post meta data$order = wc_get_order( $order_id );
			
			$order = wc_get_order( $post_id );
			$my_var_two = $order->get_billing_email();
            if(!empty($my_var_two))
                echo $my_var_two;

            break;
    }
}

// Be sure to rename this function..
function my_func() {
    global $wc_list_table;
    if ( $wc_list_table instanceof WC_Admin_List_Table_Orders ) {
        remove_action( 'restrict_manage_posts', array( $wc_list_table, 'restrict_manage_posts' ) );
    }
}
add_action( 'current_screen', 'my_func', 11 );
add_action( 'check_ajax_referer', 'my_func', 11 );