<?php

add_action('rest_api_init', function() {
	
	register_rest_route( 'ped', 'formAssist', 
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'formAssist',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);

	register_rest_route( 'ped', 'formAssistSyndic', 
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'addVarPED',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
	
});


function formAssist( WP_REST_Request $request ){
	if ( check_nonce() ) {
		include_once WC_ABSPATH . 'includes/wc-cart-functions.php';
		include_once WC_ABSPATH . 'includes/class-wc-cart.php';

		global $woocommerce;
		if ( is_null( $woocommerce->cart ) ) {
			wc_load_cart();
		}
		$params = $request->get_params();
		
		$items = $woocommerce->cart->get_cart();
		foreach($items as $item => $values) { 
			$product_id = $values['data']->get_id(); 
		} 
		$product_attributes = get_post_meta($product_id, '_product_attributes', true);
		if(!is_array($product_attributes)){
			$product_attributes = [];
		}

		$product = wc_get_product( $product_id );
		$titleProduct =  $product->get_title();
		$idAppelFonds = '';

		if(isset($_FILES['fileAppelFonds'])){
			$upload_dir   = wp_upload_dir();
			$uploaddirimg = $upload_dir['basedir'].'/fileAppelFonds/';
			if(!is_dir($uploaddirimg)){
				mkdir($uploaddirimg, 0755);
			}		
			$filename = $uploaddirimg . $product_id . '-'.$titleProduct.'-'.basename($_FILES['fileAppelFonds']['name']);
			if (move_uploaded_file($_FILES['fileAppelFonds']['tmp_name'], $filename)) {
				$wp_filetype = wp_check_filetype($filename, null );
				$attachment = array(
					'guid'           => $upload_dir['url'] . '/fileAppelFonds/' . $product_id . '-'.$titleProduct.'-'.basename($_FILES['fileAppelFonds']['name']),
					'post_mime_type' => $wp_filetype['type'],
					'post_parent' => $product_id,
					'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
					'post_content' => '',
					'post_status' => 'inherit'
				);
				$idAppelFonds = wp_insert_attachment( $attachment, $filename, $product_id );
				$product_attributes[] = array (
					'name' => htmlspecialchars( "fileAppelFonds" ), // set attribute name
					'value' => $idAppelFonds, // set attribute value
					'position' => 1,
					'is_visible' => 0,
					'is_variation' => 1,
					'is_taxonomy' => 0
				);

			}
		}	

		
		
		if(isset($_FILES['fileAnnexesComptables'])){
			$upload_dir   = wp_upload_dir();
			$uploaddirimg = $upload_dir['basedir'].'/fileAnnexesComptables/';
			if(!is_dir($uploaddirimg)){
				mkdir($uploaddirimg, 0755);
			}
			$filename = $uploaddirimg . $product_id . '-'.$titleProduct.'-'.basename($_FILES['fileAnnexesComptables']['name']);
			if (move_uploaded_file($_FILES['fileAnnexesComptables']['tmp_name'], $filename)) {
				$wp_filetype = wp_check_filetype($filename, null );
				$attachment = array(
					'guid'           => $upload_dir['url'] . '/fileAnnexesComptables/' . $product_id . '-'.$titleProduct.'-'.basename($_FILES['fileAnnexesComptables']['name']),
					'post_mime_type' => $wp_filetype['type'],
					'post_parent' => $product_id,
					'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
					'post_content' => '',
					'post_status' => 'inherit'
				);
				$idAnnexesComptables = wp_insert_attachment( $attachment, $filename, $product_id );
				$product_attributes[] = array (
					'name' => htmlspecialchars( "fileAnnexesComptables" ), // set attribute name
					'value' => $idAnnexesComptables, // set attribute value
					'position' => 1,
					'is_visible' => 0,
					'is_variation' => 1,
					'is_taxonomy' => 0
				);
			}
		}	

		

		if(isset($_FILES['fileDecompteChargen1'])){
			$upload_dir   = wp_upload_dir();
			$uploaddirimg = $upload_dir['basedir'].'/fileDecompteChargen1/';
			if(!is_dir($uploaddirimg)){
				mkdir($uploaddirimg, 0755);
			}
			$filename = $uploaddirimg . $product_id . '-'.$titleProduct.'-'.basename($_FILES['fileDecompteChargen1']['name']);
			if (move_uploaded_file($_FILES['fileDecompteChargen1']['tmp_name'], $filename)) {
				$wp_filetype = wp_check_filetype($filename, null );
				$attachment = array(
					'guid'           => $upload_dir['url'] . '/fileDecompteChargen1/' . $product_id . '-'.$titleProduct.'-'.basename($_FILES['fileDecompteChargen1']['name']),
					'post_mime_type' => $wp_filetype['type'],
					'post_parent' => $product_id,
					'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
					'post_content' => '',
					'post_status' => 'inherit'
				);
				$idFileDecompteChargen1 = wp_insert_attachment( $attachment, $filename, $product_id );
				$product_attributes[] = array (
					'name' => htmlspecialchars( "fileDecompteChargen1" ), // set attribute name
					'value' => $idFileDecompteChargen1, // set attribute value
					'position' => 1,
					'is_visible' => 0,
					'is_variation' => 1,
					'is_taxonomy' => 0
				);
			
			}
		}	

		if(isset($_FILES['fileDecompteChargen2'])){
			$upload_dir   = wp_upload_dir();
			$uploaddirimg = $upload_dir['basedir'].'/fileDecompteChargen2/';
			if(!is_dir($uploaddirimg)){
				mkdir($uploaddirimg, 0755);
			}
			$filename = $uploaddirimg . $product_id . '-'.$titleProduct.'-'.basename($_FILES['fileDecompteChargen2']['name']);
			if (move_uploaded_file($_FILES['fileDecompteChargen2']['tmp_name'], $filename)) {
				$wp_filetype = wp_check_filetype($filename, null );
				$attachment = array(
					'guid'           => $upload_dir['url'] . '/fileDecompteChargen2/' . $product_id . '-'.$titleProduct.'-'.basename($_FILES['fileDecompteChargen2']['name']),
					'post_mime_type' => $wp_filetype['type'],
					'post_parent' => $product_id,
					'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
					'post_content' => '',
					'post_status' => 'inherit'
				);
				$idFileDecompteChargen2 = wp_insert_attachment( $attachment, $filename, $product_id );
				$product_attributes[] = array (
					'name' => htmlspecialchars( "fileDecompteChargen2" ), // set attribute name
					'value' => $idFileDecompteChargen2, // set attribute value
					'position' => 1,
					'is_visible' => 0,
					'is_variation' => 1,
					'is_taxonomy' => 0
				);
			}
		}	

		
		$product_attributes[] = array (
			'name' => htmlspecialchars( stripslashes( "soldeJour" ) ), // set attribute name
			'value' => $params['soldeJour'], // set attribute value
			'position' => 1,
			'is_visible' => 1,
			'is_variation' => 1,
			'is_taxonomy' => 0
		);

		update_post_meta($product_id, '_product_attributes', $product_attributes);
		return new WP_REST_Response( '', 200 );
	}
}
