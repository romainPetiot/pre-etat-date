<?php

function createProduct(int $id_user, string $email, int $id_ped ){
	
	$post_id = wp_insert_post(array(
		'post_title' => get_the_title($id_ped).' - '.$email,
		'post_type' => 'product',
		'post_status' => 'publish',
		'post_content' => ''		
	));
	update_post_meta( $post_id, '_downloadable', 'yes' );

	$price = (get_field("price_ped", 'user_'.$id_user)) ? get_field("price_ped", 'user_'.$id_user) : get_field("price", $id_ped);
	update_post_meta( $post_id, '_price', $price );
	update_post_meta( $post_id, '_regular_price', get_field("price", $id_ped) );
	update_post_meta( $post_id, '_user_id', $id_user );
	update_post_meta( $post_id, '_id_ped', $id_ped );

	return $post_id;
}
