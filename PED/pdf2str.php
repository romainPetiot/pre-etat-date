<?php

function get_values_files($id_product){
    $parser = new \Smalot\PdfParser\Parser();
    $values = array();
    //$pdfList = ['wp-content/themes/pre-etat-date/pdf-a-analyser/AF_CC_20230101_9413.pdf','wp-content/themes/pre-etat-date/pdf-a-analyser/Appel de fonds.pdf', 'wp-content/themes/pre-etat-date/pdf-a-analyser/Régul N-1.pdf', 'wp-content/themes/pre-etat-date/pdf-a-analyser/Régul N-2.pdf','wp-content/themes/pre-etat-date/pdf-a-analyser/RG_CC_20191001_9413.pdf','wp-content/themes/pre-etat-date/pdf-a-analyser/RG_CC_20201001_9413.pdf'];
    global $product;
    $product = wc_get_product($id_product);
    $pdfList = array();
    foreach ( $product->get_attributes() as $attribute ):
        $attribute_data = $attribute->get_data(); // Get the data in an array
        if($attribute_data['name'] == 'fileAppelFonds' || $attribute_data['name'] == 'fileDecompteChargen1' || $attribute_data['name'] == 'fileDecompteChargen1' || $attribute_data['name'] == 'fileDecompteChargen2'){
            $parsedPdf = $parser->parseFile(get_attached_file($attribute_data['value']));
            $pdfContent = $parsedPdf->getPages()[0]->getDataTm();
            foreach ($pdfContent as $text) {
                if ($text[1] == 'FONCIA') {
                    $values = docTypeFoncia($pdfContent, $values);
                }
            }
        }
    endforeach;

    return $values;
}
function docTypeFoncia($content, $values){

    foreach ($content as $line) {
        if ($line[1] == 'APPELS DE PROVISIONS') {
            $owner = [];
            $owner['name'] = $content[20][1];
            $owner['address'] = $content[21][1];
            if ($content[23][0][4] == '316.85') { // Si 2 lignes d'adresse
                $owner['address2'] = $content[22][1];
                $owner['zip'] = preg_split("/(?<=[0-9]{5}) /",$content[23][1], -1,)[0];
                $owner['city'] = preg_split("/(?<=[0-9]{5}) /",$content[23][1], -1,)[1];
            }else { // Si pas 2 lignes d'adresse
                $owner['zip'] = preg_split("/(?<=[0-9]{5}) /",$content[22][1], -1,)[0];
                $owner['city'] = preg_split("/(?<=[0-9]{5}) /",$content[22][1], -1,)[1];
            }
            // copropriétaire = zone B ET C
            $values['owner'] = $owner;

            $copropriete = [];
            if ($content[23][0][4] != '316.85'){ // Si pas de 2eme ligne d'adresse copropriétaire alors content[n] = content[n - 1]
                if ($content[36][0][4] == '86') { // Si 3 lignes d'adresse de la propriété
                    $copropriete['address'] = $content[33][1];
                    $copropriete['address2'] = $content[34][1] . ' ' . $content[35][1];
                    $copropriete['zip'] = preg_split("/(?<=[0-9]{5}) /",$content[36][1], -1,)[0];
                    $copropriete['city'] = preg_split("/(?<=[0-9]{5}) /",$content[36][1], -1,)[1];
                }else{ // Si 2 lignes d'adresse de la propiété
                    $copropriete['address'] = $content[33][1];
                    $copropriete['address2'] = $content[34][1];
                    $copropriete['zip'] = preg_split("/(?<=[0-9]{5}) /",$content[35][1], -1,)[0];
                    $copropriete['city'] = preg_split("/(?<=[0-9]{5}) /",$content[35][1], -1,)[1];
                }
            }else{ // 2 lignes d'adresse du copropriétaires
                if ($content[37][0][4] == '86') {
                    //	si 3 lignes d'adresse propriété
                    $copropriete['address'] = $content[34][1];
                    $copropriete['address2'] = $content[35][1] . ' ' . $content[36][1];
                    $copropriete['zip'] = preg_split("/(?<=[0-9]{5}) /",$content[37][1], -1,)[0];
                    $copropriete['city'] = preg_split("/(?<=[0-9]{5}) /",$content[37][1], -1,)[1];
                }else{
                    //	si 2 lignes d'adresse propriété
                    $copropriete['address'] = $content[34][1];
                    $copropriete['address2'] = $content[35][1];
                    $copropriete['zip'] = preg_split("/(?<=[0-9]{5}) /",$content[36][1], -1,)[0];
                    $copropriete['city'] = preg_split("/(?<=[0-9]{5}) /",$content[36][1], -1,)[1];
                }
            }
            $values['copro'] = $copropriete;

            $i = 0;
            $values['appel_copro'] = array();
            foreach ($content as $line) {
                switch ($line[1]) {
                    case 'Total appel Copropriétaire': // Si string 'Total appel Copropriétaire' alors result = i + 1
                        $values['appel_copro'][] = str_replace(',','.', str_replace('.', '',$content[$i + 1][1]));
                        break;
                    case 'DEPENSES GENERALES' : // Si string 'DEPENSES GENERALES' OU 'CHARGES GENERALES' alors result = i + 4
                    case 'CHARGES GENERALES':
                        $values['tentieme-general'] = str_replace(',','.', str_replace('.', '',$content[$i + 4][1]));
                        break;
                    case 'Avance': // Si string 'Avance' alors result = i - 1
                        $values['avances'] = str_replace(',','.', str_replace('.', '',$content[$i - 1][1]));
                        break;
                    case 'Autres avances': // Si string 'Autres avances' alors result = i + 1
                        $values['avancesautres'] = str_replace(',','.', str_replace('.', '',$content[$i + 1][1]));
                        break;
                }
                $i++;
            }


        }elseif ($line[1] == 'CHARGES DE COPROPRIETE') {
            $i = 0;
            foreach ($content as $line) {
                if ($line[1] == 'Déduction des appels de provisions') {
                    $values['deduction'] = abs((float)str_replace(',','.', str_replace('.', '',$content[$i - 2][1])));//A
                }elseif ($line[1] == 'Total copropriétaire') {
                    $values['totalcopro'] = str_replace(',','.', str_replace('.', '',$content[$i + 1][1]));//B
                }
                $i++;
            }
        }
    }
    return $values;
}