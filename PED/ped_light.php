<?php
//fonction générique qui enregistre toutes les variables du form dans les attributs produits
function addVarPED( WP_REST_Request $request ){
	if ( check_nonce() ) {
		include_once WC_ABSPATH . 'includes/wc-cart-functions.php';
		include_once WC_ABSPATH . 'includes/class-wc-cart.php';

		global $woocommerce;
		if ( is_null( $woocommerce->cart ) ) {
			wc_load_cart();
		}
		$items = $woocommerce->cart->get_cart();

		$params = $request->get_params();
		foreach($items as $item => $values) { 
			$product_id = $values['data']->get_id(); 
		}
		$product_attributes = get_post_meta($product_id, '_product_attributes', true);
		if(!$product_attributes){
			$product_attributes = array();
		}
		foreach($params as $name => $value){
			$product_attributes[] = array (
				'name' => htmlspecialchars( stripslashes( $name ) ), // set attribute name
				'value' => $value, // set attribute value
				'position' => 1,
				'is_visible' => 1,
				'is_variation' => 1,
				'is_taxonomy' => 0
			);
		}
		update_post_meta($product_id, '_product_attributes', $product_attributes);

		return new WP_REST_Response($product_attributes , 200 );
	}
}
