<?php
/*add_filter( 'jwt_auth_whitelist', function ( $endpoints ) {
    $your_endpoints = array(
        '/wp-json/ped/*',
    );
    return array_unique( array_merge( $endpoints, $your_endpoints ) );
} );
*/
add_action('rest_api_init', function() {
	register_rest_route( 'ped', 'createUser',
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'createUser',
			'permission_callback' 	=> array(),
            //'permission_callback' 	=> function() { return ''; },
			'args' 					=> array(),
		)
	);

});


function createUser( WP_REST_Request $request ) {

	if ( check_nonce() ) {
		include_once WC_ABSPATH . 'includes/wc-cart-functions.php';
		include_once WC_ABSPATH . 'includes/class-wc-cart.php';

		$params = $request->get_params();
		global $woocommerce;
		if ( is_null( $woocommerce->cart ) ) {
			wc_load_cart();
		}
		
		$email = sanitize_email($params['email']);
		$user = get_user_by('login', $email);
		if ($user){
			$user_id = $user->ID;
		}
		else{
			$user_id = wc_create_new_customer( $email, $email, wp_generate_password() );
		}	
		wp_clear_auth_cookie();
		wp_set_current_user ( $user_id );
		wp_set_auth_cookie  ( $user_id );
		
		/*$product_id = createProduct($user_id, $email, $params['id_ped'] );
		
			
		$woocommerce->cart->empty_cart();
		$woocommerce->cart->add_to_cart( $product_id,1 );*/

		//return new WP_REST_Response( $params['assist'], 200 ); 
		return new WP_REST_Response( '', 200 ); 
	}
}


add_action('admin_menu', 'ihag_user_sub_menu_customers');
function ihag_user_sub_menu_customers() {
    add_submenu_page(
        'tools.php',
        'toolsCustomers',
        'Export clients',
        'manage_options',
        'toolsCustomers',
        'toolsCustomers',
    );
    global $submenu;
}


function toolsCustomers(){
	global $wpdb,$post;
    ?>
    <div class="wrap">
        <h1>Export Clients </h1>
        <a href="?report=exportCustomers" class="button">Export clients</a>
    <?php

	
}

add_action('init', function() {
    if(isset($_GET['report']) && $_GET['report'] == 'exportCustomers'){
		set_time_limit(0);
        exportCustomers();
    }   
});

function exportCustomers(){
    global $wpdb;
    
    $output_filename = "user_".date("Y-m-d H:i:s").'.csv';
    $output_handle = @fopen( 'php://output', 'w' );
    header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
    header( 'Content-Description: File Transfer' );
    header( 'Content-type: text/csv' );
    header( 'Content-Disposition: attachment; filename=' . $output_filename );
    header( 'Expires: 0' );
    header( 'Pragma: public' );
    // Insert header row
    

	global $product;
	$args = array(
		'post_type'         => 'shop_order',
		'posts_per_page'    => -1,
		'post_status'    => array('wc-completed'),
	);
	$orders = get_posts($args);
	$tab_author = [];
	foreach ( $orders as $order ){
		$author = get_post_meta($order->ID, '_billing_email', true);
		$cp = '';
		//if(! in_array($author, $tab_author)){
			$order = new IHAG_WC_Order($order->ID);
			//if(! $order->autonome){
				$items = $order->get_items();
				foreach ( $items as $item ) {
					$product = new WC_Product($item->get_product_id());
					$attributes = get_post_meta($item->get_product_id(), '_product_attributes', TRUE);
					if($attributes){
						foreach ( $attributes as $attribute ){							//;
							if(isset($attribute["name"]) && $attribute["name"] == 'cpCoordonate'){
								$cp = $attribute["value"];
								$tab_author[] = $author;
								fputcsv( $output_handle, array(
                                    $order->get_date_created(),
									$author,
									$cp
								),";" );
								break;
							}
						}
					}
				}
			//}
		//}
	}	
	
    fclose( $output_handle );
    exit();
}