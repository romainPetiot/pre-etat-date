<?php

function ihag_custom_role_redactor() {
	//remove_role( 'redactor' );
	//remove_role( 'senior_redactor' );
    add_role( 'redactor', __("Rédacteur", "ped"), get_role( 'administrator' )->capabilities );
	add_role( 'senior_redactor', __("Rédacteur confirmé", "ped"), get_role( 'administrator' )->capabilities );
}
add_action( 'init', 'ihag_custom_role_redactor' );

function ihag_no_admin_access_for_redactor()
{
    if ( !is_admin() || (is_user_logged_in() && isset( $GLOBALS['pagenow'] ) AND 'wp-login.php' === $GLOBALS['pagenow'] ) ) {
        return;
    }
	if( is_user_logged_in() ) {
		$user = wp_get_current_user();
		$roles = ( array ) $user->roles;
		foreach($roles as $role){
			if($role == "redactor" || $role == "senior_redactor"){
				exit( wp_redirect( get_field("page_admin_ped_listing", "option") ) );
				return;
			}
		}
	}
}
add_action( 'admin_init', 'ihag_no_admin_access_for_redactor', 100 );


add_action('admin_menu', 'register_my_custom_submenu_page');
function register_my_custom_submenu_page() {
    add_submenu_page( 'woocommerce', 'Listing PED', 'Listing PED', 'edit_posts', 'listing-ped', 'ihag_listing_ped' ); 
}
function ihag_listing_ped() {
    exit( wp_redirect( get_field("page_admin_ped_listing", "option") ) );
	return;
}

add_filter( 'cron_schedules', 'cron_five_minutes' );
 

 function cron_five_minutes( $schedules ) {
 	// Adds once weekly to the existing schedules.
 	$schedules['fiveminutes'] = array(
 		'interval' => 60 * 5,
 		'display' => __( 'fiveminutes' )
 	);
 	return $schedules;
 }
/* 
if ( ! wp_next_scheduled( 'ihag_free_ped_redactor' ) ) {
	wp_schedule_event( time(), 'fiveminutes', 'ihag_free_ped_redactor' );
}
add_action( 'ihag_free_ped_redactor', 'ihag_free_ped_redactor_function' );
function ihag_free_ped_redactor_function(){
	$args = array(
		'post_type'         => 'shop_order',
		'posts_per_page'    => -1,
		'post_status'    => array('wc-pending','wc-processing'),
	);
	$orders = get_posts($args);
	foreach ( $orders as $order ){
		$order = new IHAG_WC_Order($order->ID); 
		$order->ped_redactor_free();
	}
	
}*/