<?php

//define('FPDF_FONTPATH',dirname(__DIR__, 1).'/font');
require_once(dirname(__DIR__, 1).'/vendor/setasign/fpdf/fpdf.php');
require_once(dirname(__DIR__, 1).'/vendor/setasign/fpdi/src/autoload.php');
use setasign\Fpdi\Fpdi;
use setasign\Fpdi\PdfReader;
function formule_condit($val1, $val2, $attr){
	if(!empty($val1) && $val1 > 0){
		return $val1;
	}
	else if($val2 == 0){
		return 0;
	}
	return $val2 / $attr['Tantiemesgeneraux'] * (
		$attr['tantiemeCoordonate'] + 
		$attr['tantiemeCoordonate2'] + 
		$attr['tantiemeCoordonate3'] + 
		$attr['tantiemeCoordonate4'] + 
		$attr['tantiemeCoordonate5'] + 
		$attr['tantiemeCoordonate6'] + 
		$attr['tantiemeCoordonate7'] );
}
function add_zero($pdf, $y, $x=145){
	$euro = iconv('UTF-8', 'windows-1252','€');
	$pdf->SetY($y);
	$pdf->SetX($x);
	$pdf->Cell(50,9,number_format(0, 2, ',', '').$euro,0,0,'R');
	return $pdf;
}
function creaPreEtatDate(int $post_id){//générer lors de la récap
	if ( ! function_exists( 'media_handle_upload' ) ) {
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		require_once( ABSPATH . 'wp-admin/includes/media.php' );
	}
	global $product;
	$product = wc_get_product($post_id); 
	$attr = array();
	foreach ( $product->get_attributes() as $attribute ):
		$attribute_data = $attribute->get_data(); // Get the data in an array
		//$attr[$attribute_data['name']] = iconv('UTF-8', 'windows-1252', $attribute_data['options'][0]);
		$attr[$attribute_data['name']] = iconv('UTF-8', 'windows-1252', $attribute_data['value']);
	endforeach;
	$euro = iconv('UTF-8', 'windows-1252','€');

	$pdf = new Fpdi();
	$pageCount = $pdf->setSourceFile(get_stylesheet_directory().'/pdf/pre-etat-date.pdf');
	$pdf->SetFont('Arial','',10);
	//$pdf->fontpath  = get_stylesheet_directory().'/font';
	//$pdf->AddFont('SourceSansPro', '', 'SourceSansPro-Regular.ttf');

	//page 1
	$pageId = $pdf->importPage(1, PdfReader\PageBoundaries::MEDIA_BOX);
	$pdf->addPage();
	$pdf->useImportedPage($pageId, 0, 0, 210);

	//Adresse du bien
	$x = 14;
	$pdf->SetY(32);
	$pdf->SetX($x);
	$pdf->MultiCell(120,5,"Le ".date("d/m/Y"));
	
	$pdf->SetFont('Arial','',9);
	$x = 16;
	$pdf->SetY(50);
	$pdf->SetX($x);
	$adresse = "SDC"."\n".$attr['addressCoordonate']."\n";
	if(!empty($attr['address2Coordonate'])){
		$adresse .= $attr['address2Coordonate']."\n";
	}
	$adresse .= $attr['cpCoordonate'].' '.$attr['cityCoordonate'];
	$pdf->MultiCell(57,5,$adresse);
	
	//Cordonnées proprio cédant
	$cordonnees = "";
	$x = 77;
	$pdf->SetY(50);
	$pdf->SetX($x);
	$cordonnees .= $attr['firstnameOwner1'].' '.$attr['lastnameOwner1']."\n";
	if(!empty($attr['firstnameOwner2']) || !empty($attr['lastnameOwner2'])){
		$cordonnees .= "Et ".$attr['firstnameOwner2'].' '.$attr['lastnameOwner2']."\n";
	}
	if(!empty($attr['firstnameOwner3']) || !empty($attr['lastnameOwner3'])){
		$cordonnees .= "Et ".$attr['firstnameOwner3'].' '.$attr['lastnameOwner3']."\n";
	}
	if(!empty($attr['firstnameOwner4']) || !empty($attr['lastnameOwner4'])){
		$cordonnees .= "Et ".$attr['firstnameOwner4'].' '.$attr['lastnameOwner4']."\n";
	}
	if(!empty($attr['firstnameOwner5']) || !empty($attr['lastnameOwner5'])){
		$cordonnees .= "Et ".$attr['firstnameOwner5'].' '.$attr['lastnameOwner5']."\n";
	}
	if(!empty($attr['firstnameOwner6']) || !empty($attr['lastnameOwner6'])){
		$cordonnees .= "Et ".$attr['firstnameOwner6'].' '.$attr['lastnameOwner6']."\n";
	}
	if(!empty($attr['firstnameOwner7']) || !empty($attr['lastnameOwner7'])){
		$cordonnees .= "Et ".$attr['firstnameOwner7'].' '.$attr['lastnameOwner7']."\n";
	}

	$cordonnees .= $attr['addressOwner1']."\n";
	if(!empty($attr['address2Owner1'])){
		$cordonnees .= $attr['address2Owner1']."\n";
	}
	$cordonnees .= $attr['cpOwner1'].' '.$attr['cityOwner1']."\n";
	$pdf->MultiCell(57,5,$cordonnees);

	//Numéro de lot
	$x = 136;
	$pdf->SetY(50);
	$pdf->SetX($x);
	$lotCoordonate = $attr['lotCoordonate'];
	$lotCoordonate .= (isset($attr['lotCoordonate2']) && !empty($attr['lotCoordonate2'])) ? "\n".$attr['lotCoordonate2'] : '';
	$lotCoordonate .= (isset($attr['lotCoordonate3']) && !empty($attr['lotCoordonate3'])) ? "\n".$attr['lotCoordonate3'] : '';
	$lotCoordonate .= (isset($attr['lotCoordonate4']) && !empty($attr['lotCoordonate4'])) ? "\n".$attr['lotCoordonate4'] : '';
	$lotCoordonate .= (isset($attr['lotCoordonate5']) && !empty($attr['lotCoordonate5'])) ? "\n".$attr['lotCoordonate5'] : '';
	$lotCoordonate .= (isset($attr['lotCoordonate6']) && !empty($attr['lotCoordonate6'])) ? "\n".$attr['lotCoordonate6'] : '';
	$lotCoordonate .= (isset($attr['lotCoordonate7']) && !empty($attr['lotCoordonate7'])) ? "\n".$attr['lotCoordonate7'] : '';
	$pdf->MultiCell(28,5,$lotCoordonate);

	$pdf->SetFont('Arial','',10);

	//date
	$x = 16;
	$pdf->SetY(215);
	$pdf->SetX($x);
	$pdf->Cell(60,10,date("d/m/Y"));

	//Demandeur
	$x = 107;
	$pdf->SetY(215);
	$pdf->SetX($x);
	if(!empty($attr['firstnameOwner2']) || !empty($attr['lastnameOwner2'])){
		//$pdf->Cell(90,10,$attr['firstnameOwner1'].' '.$attr['lastnameOwner1'].' Et '.$attr['firstnameOwner2'].' '.$attr['lastnameOwner2']);
		$pdf->Cell(60,10,$attr['firstnameOwner1'].' '.$attr['lastnameOwner1']);
		$pdf->ln(5);
		$pdf->SetX($x);
		$pdf->Cell(60,10,'Et '.$attr['firstnameOwner2'].' '.$attr['lastnameOwner2']);
	}
	else{
		$pdf->Cell(60,10,$attr['firstnameOwner1'].' '.$attr['lastnameOwner1']);
	}
	$pdf->ln(5);
	$pdf->SetX($x);
	$pdf->Cell(60,10,$attr['addressOwner1']);
	if(!empty($attr['address2Owner1'])){
		$pdf->ln(5);
		$pdf->SetX($x);
		$pdf->Cell(60,10,$attr['address2Owner1']);
	}
	$pdf->ln(5);
	$pdf->SetX($x);
	$pdf->Cell(60,10,$attr['cpOwner1'].' '.$attr['cityOwner1']);

	
	//page 2
	$pageId = $pdf->importPage(2, PdfReader\PageBoundaries::MEDIA_BOX);
	$pdf->addPage();
	$pdf->useImportedPage($pageId, 0, 0, 210);
	
	if($attr['situationDateDuJour'] < 0){
		$pdf->SetFont('Arial','',9);
		$x = 145;
		$pdf->SetY(60);
		$pdf->SetX($x);
		$pdf->Cell(50,10,number_format(-$attr['situationDateDuJour'], 2, ',', '').$euro,0,0,'R');

		$pdf->SetY(152);
		$pdf->SetX($x);
		$pdf->SetFont('Arial','B',9);
		$pdf->Cell(50,9,number_format(-$attr['situationDateDuJour'] , 2, ',', '').$euro,0,0,'R');
		$pdf->SetY(197);
		$pdf->SetX($x);
		$pdf->Cell(50,9,number_format(-$attr['situationDateDuJour'] , 2, ',', '').$euro,0,0,'R');
		$pdf->SetFont('Arial','',9);
	}
	else{
		$pdf->SetFont('Arial','',9);
		$pdf->SetY(60);
		$pdf->SetX(145);
		$pdf->Cell(50,10,number_format(0, 2, ',', '').$euro,0,0,'R');
		
		$pdf->SetY(152);
		$pdf->SetX(145);
		$pdf->Cell(50,9,number_format(0 , 2, ',', '').$euro,0,0,'R');
		
		$pdf->SetY(197);
		$pdf->SetX(145);
		$pdf->Cell(50,9,number_format(0, 2, ',', '').$euro,0,0,'R');
	}

	//beg 0
	$pdf = add_zero($pdf, 66);
	$pdf = add_zero($pdf, 73);
	$pdf = add_zero($pdf, 80);
	$pdf = add_zero($pdf, 98);
	$pdf = add_zero($pdf, 103);
	$pdf = add_zero($pdf, 109);
	$pdf = add_zero($pdf, 127);
	$pdf = add_zero($pdf, 132);
	$pdf = add_zero($pdf, 140);
	//$pdf = add_zero($pdf, 152);
	$pdf = add_zero($pdf, 178);
	$pdf->SetFont('Arial','B',9);
	$pdf = add_zero($pdf, 183);
	//end 0
	
	$pdf->SetY(215);
	$pdf->SetX(15);
	$pdf->SetFont('Arial','B',10);
	$control = 'Les sommes ont été contrôlées à la date du ';
	$pdf->MultiCell(200,6,iconv('UTF-8','windows-1252',$control.ucwords(date_i18n('l j F Y', current_time('timestamp')))));
	$pdf->SetFont('Arial','',10);

	//page 3
	$pageId = $pdf->importPage(3, PdfReader\PageBoundaries::MEDIA_BOX);
	$pdf->addPage();
	$pdf->useImportedPage($pageId, 0, 0, 210);
	$pdf->SetFont('Arial','',9);
	$a1 = formule_condit($attr['avanceTresoADF'], $attr['avanceTresoACP'], $attr);
	$x = 145;
	$pdf->SetY(46);
	$pdf->SetX($x);
	$pdf->Cell(50,9,number_format($a1, 2, ',', '').$euro,0,0,'R'); //A1

	$a2 = formule_condit($attr['avanceTresoADF18al6'], $attr['avanceTresoACP18al6'], $attr);
	$pdf->SetY(53);
	$pdf->SetX($x);
	$pdf->Cell(50,9,number_format($a2, 2, ',', '').$euro,0,0,'R'); //A2

	$pdf = add_zero($pdf, 61);
	$pdf = add_zero($pdf, 99);//B1

	if($attr['situationDateDuJour'] >= 0){
		$pdf->SetFont('Arial','',9);
		$pdf->SetY(132);
		$pdf->SetX($x);
		$c1 = $attr['situationDateDuJour'];
		$pdf->Cell(50,10,number_format($c1, 2, ',', '').$euro,0,0,'R'); // C1
	}
	else{
		$pdf->SetFont('Arial','',9);
		$pdf->SetY(132);
		$pdf->SetX($x);
		$pdf->Cell(50,10,number_format(0, 2, ',', '').$euro,0,0,'R');//C1
		$c1 = 0;
	}

	
	
	$pdf->SetY(146);
	$pdf->SetX($x);
	$pdf->SetFont('Arial','B',9);
	$total = $a1 + $a2 + $c1;
	$pdf->Cell(50,9,number_format($total, 2, ',', '').$euro,0,0,'R');//C1
	$pdf->SetFont('Arial','',9);

	$pdf->SetY(203);
	$pdf->SetX($x);
	$solution1 = $a1 + $a2;
	$pdf->Cell(50,10,number_format($solution1, 2, ',', '').$euro,0,0,'R');//solution1

	$pdf = add_zero($pdf, 232);

	//page 4
	$pageId = $pdf->importPage(4, PdfReader\PageBoundaries::MEDIA_BOX);
	$pdf->addPage();
	$pdf->useImportedPage($pageId, 0, 0, 210);
	
	$nb = formule_condit($attr['avanceTresoADF'], $attr['avanceTresoACP'], $attr);
	$x = 145;
	$pdf->SetY(46);
	$pdf->SetX($x);
	$pdf->Cell(50,9,number_format($nb, 2, ',', '').$euro,0,0,'R');

	$nb2 = formule_condit($attr['avanceTresoADF18al6'], $attr['avanceTresoACP18al6'], $attr);
	$pdf->SetY(52);
	$pdf->SetX($x);
	$pdf->Cell(50,9,number_format($nb2, 2, ',', '').$euro,0,0,'R');

	$pdf = add_zero($pdf, 58);

	$x = 119;
	$pdf->SetY(82);
	$pdf->SetX($x);
	$pdf->Cell(50,9,number_format($attr['appelFondsOrdinaire'], 2, ',', '').$euro,0,0,'R');
	$pdf->ln(7);
	$pdf->SetX($x);
	$pdf->Cell(50,9,number_format($attr['appelFondsOrdinaire'], 2, ',', '').$euro,0,0,'R');
	$pdf->ln(7);
	$pdf->SetX($x);
	$pdf->Cell(50,9,number_format($attr['appelFondsOrdinaire'], 2, ',', '').$euro,0,0,'R');


	$pdf->SetY(117);
	$pdf->SetX($x);
	$pdf->Cell(50,9,number_format(number_empty($attr['appelFondsAlur']), 2, ',', '').$euro,0,0,'R');
	$pdf->ln(7);
	$pdf->SetX($x);
	$pdf->Cell(50,9,number_format(number_empty($attr['appelFondsAlur']), 2, ',', '').$euro,0,0,'R');
	$pdf->ln(7);
	$pdf->SetX($x);
	$pdf->Cell(50,9,number_format(number_empty($attr['appelFondsAlur']), 2, ',', '').$euro,0,0,'R');

	
	//page 5
	$pageId = $pdf->importPage(5, PdfReader\PageBoundaries::MEDIA_BOX);
	$pdf->addPage();
	$pdf->useImportedPage($pageId, 0, 0, 210);

	$nb = formule_condit($attr['montantAppelDecompteChargeN1'], $attr['budgetN1'], $attr);
	$pdf->SetY(58);
	$pdf->SetX(53);
	$pdf->Cell(35,9,number_format($nb, 2, ',', '').$euro,0,0,'C');

	$nb = formule_condit($attr['montantDepenseDecompteChargeN1'], $attr['depenseN1'], $attr);
	$pdf->SetX(88);
	$pdf->Cell(35,9,number_format($nb, 2, ',', '').$euro,0,0,'C');

	$nb = formule_condit($attr['montantAppelDecompteTravauxN1'], $attr['budgetTravauxN1'], $attr);
	$pdf->SetX(123);
	$pdf->Cell(35,9,number_format($nb, 2, ',', '').$euro,0,0,'C');

	$nb = formule_condit($attr['montantDepenseDecompteTravauxN1'], $attr['depenseTravauxN1'], $attr);
	$pdf->SetX(158);
	$pdf->Cell(35,9,number_format($nb, 2, ',', '').$euro,0,0,'C');


	$nb = formule_condit($attr['montantAppelDecompteChargeN2'], $attr['budgetN2'], $attr);
	$pdf->ln(7);
	$pdf->SetX(53);
	$pdf->Cell(35,9,number_format($nb, 2, ',', '').$euro,0,0,'C');

	$nb = formule_condit($attr['montantDepenseDecompteChargeN2'], $attr['depenseN2'], $attr);
	$pdf->SetX(88);
	$pdf->Cell(35,9,number_format($nb, 2, ',', '').$euro,0,0,'C');

	$nb = formule_condit($attr['montantAppelDecompteTravauxN2'], $attr['budgetTravauxN2'], $attr);
	$pdf->SetX(123);
	$pdf->Cell(35,9,number_format($nb, 2, ',', '').$euro,0,0,'C');

	$nb = formule_condit($attr['montantDepenseDecompteTravauxN2'], $attr['depenseTravauxN2'], $attr);
	$pdf->SetX(158);
	$pdf->Cell(35,9,number_format($nb, 2, ',', '').$euro,0,0,'C');

	//page 6
	$pageId = $pdf->importPage(6, PdfReader\PageBoundaries::MEDIA_BOX);
	$pdf->addPage();
	$pdf->useImportedPage($pageId, 0, 0, 210);

	//10 et 11
	$pdf->SetFont('Arial','',10);
	if($attr['dettesCopro'] > 0){
		$pdf->SetY(48);
		$pdf->SetX(20);
		$pdf->Cell(6,9,'X');
		$pdf->SetX(145);
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(50,8,number_format($attr['dettesCopro'], 2, ',', '').$euro,0,0,'R');
	}
	else{
		$pdf->SetY(59);
		$pdf->SetX(20);
		$pdf->Cell(6,9,'X');
	}

	//12 et 13 annexeImpayeFournisseur
	$pdf->SetFont('Arial','',10);
	if($attr['dettesFournisseurs'] > 0){
		$pdf->SetY(90);
		$pdf->SetX(20);
		$pdf->Cell(6,9,'X');
		$pdf->SetX(145);
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(50,8,number_format($attr['dettesFournisseurs'], 2, ',', '').$euro,0,0,'R');
	}
	else{
		$pdf->SetY(101);
		$pdf->SetX(20);
		$pdf->Cell(6,9,'X');
	}


	$nb = formule_condit($attr['montantFondsAlurADF'], $attr['montantFondsAlurACP'], $attr);
	$appelFondsAlur = (!empty($attr['appelFondsAlur'])) ? $attr['appelFondsAlur'] : 0;
	$pdf->SetFont('Arial','',10);
	if(($nb + $appelFondsAlur) > 0){
		$pdf->SetY(132);
		$pdf->SetX(20);
		$pdf->Cell(6,9,'X');
		$pdf->SetY(139);
		$pdf->SetX(145);
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(50,8,number_format($nb, 2, ',', '').$euro,0,0,'R');

		$pdf->SetY(147);
		$pdf->SetX(145);
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(50,8,number_format($appelFondsAlur, 2, ',', '').$euro,0,0,'R');
	}
	else{
		$pdf->SetFont('Arial','',10); 
		$pdf->SetY(157);
		$pdf->SetX(20);
		$pdf->Cell(6,9,'X');
	}

	
	$upload_dir = wp_upload_dir();
	if(!is_dir($upload_dir['basedir'].'/pdf')){
		mkdir($upload_dir['basedir'].'/pdf');
	}
	$pdf->Output('F',$upload_dir['basedir'].'/pdf/pre-etat-date'.$post_id.'.pdf');


	$filename = basename($upload_dir['basedir'].'/pdf/pre-etat-date'.$post_id.'.pdf');
	/*$arrContextOptions=array(
		"ssl"=>array(
			"verify_peer"=>false, 
			"verify_peer_name"=>false,
		),
	); */ 
	/*$upload_file = wp_upload_bits($filename, null, file_get_contents($file, false, stream_context_create($arrContextOptions)));
	if (!$upload_file['error']) {*/
		$wp_filetype = wp_check_filetype($filename, null );
		$attachment = array(
			'post_mime_type' => $wp_filetype['type'],
			'post_parent' => $post_id,
			'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
			'post_content' => '',
			'post_status' => 'inherit'
		);
		$attachment_id = wp_insert_attachment( $attachment, $upload_dir['basedir'].'/pdf/pre-etat-date'.$post_id.'.pdf', $post_id );
		/*if (!is_wp_error($attachment_id)) {
			require_once(ABSPATH . "wp-admin" . '/includes/image.php');
			$attachment_data = wp_generate_attachment_metadata( $attachment_id, $upload_file['file'] );
			wp_update_attachment_metadata( $attachment_id,  $attachment_data );
		}*/
	//}

	$file_name = "pre-etat-date.pdf";
	$file_url  = wp_get_attachment_url( $attachment_id );
	$download_id = md5( $file_url );

	// Creating an empty instance of a WC_Product_Download object
	$pd_object = new WC_Product_Download();

	// Set the data in the WC_Product_Download object
	$pd_object->set_id( $download_id );
	$pd_object->set_name( $file_name );
	$pd_object->set_file( $file_url );

	// Get an instance of the WC_Product object (from a defined product ID)
	$product = wc_get_product( $post_id ); // <=== Be sure it's the product ID

	// Get existing downloads (if they exist)
	$downloads = [];//$product->get_downloads();

	// Add the new WC_Product_Download object to the array
	$downloads[$download_id] = $pd_object;

	// Set the complete downloads array in the product
	$product->set_downloads($downloads);
	$product->save(); // Save the data in database
	//@unlink($upload_dir['baseurl'].'/pdf-temp/pre-etat-date'.$post_id.'.pdf');
}

