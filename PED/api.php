<?php
require_once(dirname(__DIR__, 1).'/vendor/autoload.php');
use Firebase\JWT\Key;
use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;


add_action('rest_api_init', function() {
	register_rest_route( 'ped-api', 'new-order',
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'ped_api_new_order',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
});

add_action('rest_api_init', function() {
    register_rest_route( 'ped-api', 'new-token',
        array(
            'methods' 				=> 'POST', //WP_REST_Server::READABLE,
            'callback'        		=> 'ped_api_new_token',
            'permission_callback' 	=> array(),
            'args' 					=> array(),
        )
    );
});

function ped_api_new_token(WP_REST_Request $request){

    $creds = array(
        'user_login'    => $request['user'],
        'user_password' => $request['password'],
    );

    $user = wp_signon( $creds, false );
    if ( is_wp_error( $user ) ) {//check user bad
        $return = [];
        $return['code'] = "user_invalide";
        $return['message'] = $user->get_error_message();
        $return['status'] = 401;
        return new WP_REST_Response( $return, 401 );
    }

    $key = JWT_AUTH_SECRET_KEY;
    $payload = [
        'iss' => home_url('wp-json','https'),
        'aud' => home_url('wp-json','https'),
        'iat' => time(),
        'nbf' => time()
    ];

    $jwt = JWT::encode($payload, $key, 'HS256');

    JWT::$leeway = 60; // $leeway in seconds
    $decoded = JWT::decode($jwt, new Key($key, 'HS256'));

    $return = [];
    $return['code'] = "jwt_auth_token";
    $return['token'] = $jwt;
    $return['status'] = 200;
    return new WP_REST_Response( $return, 200 );

}

function ped_api_new_order(WP_REST_Request $request){
    if(!$request->get_header('authorization')){//check token
        $return = [];
        $return['code'] = "jwt_auth_no_auth_header";
        $return['message'] = "Authorization header not found";
        $return['status'] = 403;
        return new WP_REST_Response( $return, 403 );
    } 
    else if($request->get_header('authorization')){ // token not empty

        $key = JWT_AUTH_SECRET_KEY;
        try {
            $decoded = JWT::decode(str_replace(['Bearer', ' '],'',$request->get_header('authorization')), new Key($key, 'HS256'));
        } catch (InvalidArgumentException $e) {
            $return = [];
            $return['code'] = "InvalidArgumentException";
            $return['message'] = "provided key/key-array is empty or malformed.";
            $return['status'] = 401;
            return new WP_REST_Response( $return, 401 );
        }
        catch (DomainException $e) {
            $return = [];
            $return['code'] = "DomainException";
            $return['message'] = "provided key is invalid";
            $return['status'] = 401;
            return new WP_REST_Response( $return, 401 );
        }
        catch (SignatureInvalidException $e) {
            $return = [];
            $return['code'] = "SignatureInvalidException";
            $return['message'] = "provided JWT signature verification failed.";
            $return['status'] = 401;
            return new WP_REST_Response( $return, 401 );
        } catch (BeforeValidException $e) {
            $return = [];
            $return['code'] = "BeforeValidException";
            $return['message'] = "provided JWT is trying to be used before nbf or iat claim";
            $return['status'] = 401;
            return new WP_REST_Response( $return, 401 );
        } catch (ExpiredException $e) {
            //
            $return = [];
            $return['code'] = "ExpiredException";
            $return['message'] = "provided JWT is trying to be used after exp claim.";
            $return['status'] = 401;
            return new WP_REST_Response( $return, 401 );
        } catch (UnexpectedValueException $e) {
            // provided JWT is malformed OR
            // provided JWT is missing an algorithm / using an unsupported algorithm OR
            // provided JWT algorithm does not match provided key OR
            // provided key ID in key/key-array is empty or invalid.
            $return = [];
            $return['code'] = "UnexpectedValueException";
            $return['message'] = "UnexpectedValueException";
            $return['status'] = 401;
            return new WP_REST_Response( $return, 401 );
        }

        $creds = array(
            'user_login'    => $request['user'],
            'user_password' => $request['password'],
        );

        $user = wp_signon( $creds, false );
        if ( is_wp_error( $user ) ) {//check user bad
            $return = [];
            $return['code'] = "user_invalide";
            $return['message'] = $user->get_error_message();
            $return['status'] = 401;
            return new WP_REST_Response( $return, 401 );
        }
        elseif(empty($request['owner_name']) ||
            empty($request['extranet_user']) ||
            empty($request['extranet_password']) ||
            empty($request['property_address']) ||
            empty($request['owner_address']) ||
            empty($request['parcel']) ||
            empty($request['general_increment']) ||
            empty($request['parcel_increment'])

            ){//user ok && variable empty (bad)

            $return = [];
            $return['code'] = "variableEmpty";
            $return['message'] = "A variable is empty";
            $return['status'] = 400;
            return new WP_REST_Response( $return, 400 );
        }
        else{//all verif ok, processing request (new order)
            $order_id = ihag_new_order($user, $request);
            //sauvegarde des variables aux formats caratéristiques woocommerce

            $return = [];
            $return['code'] = "new_order_success";
            $return['message'] = "New order success";
            $return['status'] = 200;
            $return['order'] = $order_id;
            return new WP_REST_Response( $return, 200 );
        }

    }
}

function ihag_new_order($user, $params){

    $user_id = $user->ID;
    $email = $user->user_email;

    $post_id = wp_insert_post(array(
        'post_title' => 'API - '.$email,
        'post_type' => 'product',
        'post_status' => 'publish',
        'post_content' => ''		
    ));
    update_post_meta( $post_id, '_downloadable', 'yes' );
    update_post_meta( $post_id, '_price', get_field("price_ped", "user_".$user_id)  );
    update_post_meta( $post_id, '_regular_price', get_field("price_ped", "user_".$user_id) );
    update_post_meta( $post_id, '_user_id', $user_id );

    $product_attributes = get_post_meta($post_id, '_product_attributes', true);
    if(!$product_attributes){
        $product_attributes = array();
    }
    foreach($params as $name => $value){
        $product_attributes[] = array (
            'name' => htmlspecialchars( stripslashes( $name ) ), // set attribute name
            'value' => $value, // set attribute value
            'position' => 1,
            'is_visible' => 1,
            'is_variation' => 1,
            'is_taxonomy' => 0
        );
    }
    $product_attributes[] = array (
        'name' => 'lastnameOwner1', // set attribute name
        'value' => $params['owner_name'], // set attribute value
        'position' => 1,
        'is_visible' => 1,
        'is_variation' => 1,
        'is_taxonomy' => 0
    );

    /*$product_attributes[] = array (
        'name' => htmlspecialchars( stripslashes( $name ) ), // set attribute name
        'value' => $params['extranet_user'], // set attribute value
        'position' => 1,
        'is_visible' => 1,
        'is_variation' => 1,
        'is_taxonomy' => 0
    );

    $product_attributes[] = array (
        'name' => 'htmlspecialchars( stripslashes( $name ) )', // set attribute name
        'value' => $params['extranet_password'], // set attribute value
        'position' => 1,
        'is_visible' => 1,
        'is_variation' => 1,
        'is_taxonomy' => 0
    );*/

    $product_attributes[] = array (
        'name' => 'addressCoordonate', // set attribute name
        'value' => $params['property_address'], // set attribute value
        'position' => 1,
        'is_visible' => 1,
        'is_variation' => 1,
        'is_taxonomy' => 0
    );

    $product_attributes[] = array (
        'name' => 'addressOwner1', // set attribute name
        'value' => $params['owner_address'], // set attribute value
        'position' => 1,
        'is_visible' => 1,
        'is_variation' => 1,
        'is_taxonomy' => 0
    );

    $product_attributes[] = array (
        'name' => 'lotCoordonate', // set attribute name
        'value' => $params['parcel'], // set attribute value
        'position' => 1,
        'is_visible' => 1,
        'is_variation' => 1,
        'is_taxonomy' => 0
    );

    $product_attributes[] = array (
        'name' => 'Tantiemesgeneraux', // set attribute name
        'value' => $params['general_increment'], // set attribute value
        'position' => 1,
        'is_visible' => 1,
        'is_variation' => 1,
        'is_taxonomy' => 0
    );

    $product_attributes[] = array (
        'name' => 'tantiemeCoordonate', // set attribute name
        'value' => $params['parcel_increment'], // set attribute value
        'position' => 1,
        'is_visible' => 1,
        'is_variation' => 1,
        'is_taxonomy' => 0
    );

    update_post_meta($post_id, '_product_attributes', $product_attributes);

    global $woocommerce;

    $address = array(
        'email'      =>  $email,
        'first_name' => $user->first_name,
        'last_name' => $user->last_name,
    );

    $order = wc_create_order();

    $order->add_product( wc_get_product($post_id), 1); // This is an existing SIMPLE product
    $order->set_address( $address, 'billing' );
    $order->calculate_totals();
    
    update_post_meta( $order->get_id(), '_payment_method', 'month' ); //set payment method
    update_post_meta( $order->get_id(), '_payment_method_title', 'Paiement au mois' ); //set payment title

    update_post_meta( $order->get_id(), 'relance', 'manual' ); //neutralise la relance panier abandonné
    
    $order->update_status("pending", 'PED manuel', TRUE);

    return $order->get_id();
}
