<?php
use setasign\Fpdi\Fpdi;
use setasign\Fpdi\PdfReader;

function ihag_custom_role_month_paiement() {
    add_role( 'month_paiement', __("Paiement au mois", "ped"), get_role( 'customer' )->capabilities );
}
add_action( 'init', 'ihag_custom_role_month_paiement' );




function is_month_paiement(){
    global $current_user;
    $user_id = get_current_user_id();
	if($user_id == 0){return false;}
	$user_meta = get_userdata($user_id);
	$user_roles = $user_meta->roles;
	return in_array('month_paiement', $user_roles);
}


add_action('init', 'init_month_gateway_class');
function init_month_gateway_class(){

    class WC_Gateway_Month extends WC_Payment_Gateway {

        public $domain;

        /**
         * Constructor for the gateway.
         */
        public function __construct() {

            $this->domain = 'month_payment';

            $this->id                 = 'month';
            $this->icon               = apply_filters('woocommerce_custom_gateway_icon', '');
            $this->has_fields         = false;
            $this->method_title       = __( 'Paiement au mois', $this->domain );
            $this->method_description = __( 'Allows payments with custom gateway.', $this->domain );

            // Load the settings.
            $this->init_form_fields();
            $this->init_settings();

            // Define user set variables
            $this->title        = $this->get_option( 'title' );
            $this->description  = $this->get_option( 'description' );
            $this->instructions = $this->get_option( 'instructions', $this->description );
            $this->order_status = $this->get_option( 'order_status', 'completed' );

            // Actions
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
            add_action( 'woocommerce_thankyou_' . $this->id, array( $this, 'thankyou_page' ) );

            // Customer Emails
            add_action( 'woocommerce_email_before_order_table', array( $this, 'email_instructions' ), 10, 3 );
        }

        /**
         * Initialise Gateway Settings Form Fields.
         */
        public function init_form_fields() {

            $this->form_fields = array(
                'enabled' => array(
                    'title'   => __( 'Enable/Disable', $this->domain ),
                    'type'    => 'checkbox',
                    'label'   => __( 'Activer paiement au mois', $this->domain ),
                    'default' => 'yes'
                ),
                'title' => array(
                    'title'       => __( 'Title', $this->domain ),
                    'type'        => 'text',
                    'description' => __( 'This controls the title which the user sees during checkout.', $this->domain ),
                    'default'     => __( 'Paiement au mois', $this->domain ),
                    'desc_tip'    => true,
                ),
                'order_status' => array(
                    'title'       => __( 'Order Status', $this->domain ),
                    'type'        => 'select',
                    'class'       => 'wc-enhanced-select',
                    'description' => __( 'Choose whether status you wish after checkout.', $this->domain ),
                    'default'     => 'wc-pending',
                    'desc_tip'    => true,
                    'options'     => wc_get_order_statuses()
                ),
                'description' => array(
                    'title'       => __( 'Description', $this->domain ),
                    'type'        => 'textarea',
                    'description' => __( 'Payment method description that the customer will see on your checkout.', $this->domain ),
                    'default'     => __('', $this->domain),
                    'desc_tip'    => true,
                ),
                'instructions' => array(
                    'title'       => __( 'Instructions', $this->domain ),
                    'type'        => 'textarea',
                    'description' => __( 'Instructions that will be added to the thank you page and emails.', $this->domain ),
                    'default'     => '',
                    'desc_tip'    => true,
                ),
            );
        }

        /**
         * Output for the order received page.
         */
        public function thankyou_page() {
            if ( $this->instructions )
                echo wpautop( wptexturize( $this->instructions ) );
        }

        /**
         * Add content to the WC emails.
         *
         * @access public
         * @param WC_Order $order
         * @param bool $sent_to_admin
         * @param bool $plain_text
         */
        public function email_instructions( $order, $sent_to_admin, $plain_text = false ) {
            if ( $this->instructions && ! $sent_to_admin && 'custom' === $order->payment_method && $order->has_status( 'on-hold' ) ) {
                echo wpautop( wptexturize( $this->instructions ) ) . PHP_EOL;
            }
        }

        public function payment_fields(){

            if ( $description = $this->get_description() ) {
                echo wpautop( wptexturize( $description ) );
            }

            
        }

        /**
         * Process the payment and return the result.
         *
         * @param int $order_id
         * @return array
         */
        public function process_payment( $order_id ) {

            $order = wc_get_order( $order_id );

            $status = 'wc-' === substr( $this->order_status, 0, 3 ) ? substr( $this->order_status, 3 ) : $this->order_status;

            // Set order status
            $order->update_status( $status, __( 'Checkout with custom payment. ', $this->domain ) );

            // or call the Payment complete
            // $order->payment_complete();

            // Reduce stock levels
            $order->reduce_order_stock();

            // Remove cart
            WC()->cart->empty_cart();

            // Return thankyou redirect
            return array(
                'result'    => 'success',
                'redirect'  => $this->get_return_url( $order )
            );
        }
    }
}

add_filter( 'woocommerce_payment_gateways', 'add_month_gateway_class' );
function add_month_gateway_class( $methods ) {
    $methods[] = 'WC_Gateway_Month'; 
    return $methods;
}
/**
 * Update the order meta with field value
 */
add_action( 'woocommerce_checkout_update_order_meta', 'month_payment_update_order_meta' );
function month_payment_update_order_meta( $order_id ) {

    if($_POST['payment_method'] != 'month')
        return;
	
	$order = new WC_Order($order_id);
	update_post_meta( $order->get_id(), 'relance', 'manual' ); //neutralise la relance panier abandonné

}


/*add_action('admin_menu', 'ihag_user_sub_menu_month_payment');
function ihag_user_sub_menu_month_payment() {
    add_submenu_page(
        'tools.php',
        'tools_month_payment',
        'Export CSV Paiement au mois',
        'manage_options',
        'tools_month_payment',
        'tools_month_payment',
    );
    global $submenu;
}*/


if ( ! wp_next_scheduled( 'ihag_month_recap' ) ) {
	wp_schedule_event( time(), 'daily', 'ihag_month_recap' );
}

add_action( 'ihag_month_recap', 'ihag_month_recap_fun' );
function ihag_month_recap_fun(){
   
    $dtFirst = new DateTime('first day of this month');
    $dtToday = new DateTime('today');
    
    $interval = $dtToday->diff($dtFirst);
    
    // format('%a') produces total 'days' from DateTime::diff()
    if ($interval->format('%a') == '0'){
        ihag_month_recap_fun_go(true);
    }
    
       
}

function ihag_month_recap_fun_go($send_email = false){
    export_order($send_email, 'systempaystd');
    export_order($send_email, 'cheque');
    export_order($send_email, 'month');
    export_order($send_email, 'bacs');

    $month_start = strtotime('first day of this month', mktime(0, 0, 0, date("m")-1, date("d"),   date("Y")));
	$month_end = strtotime('last day of this month', mktime(23, 59, 59, date("m")-1, date("d"),   date("Y")));
	$startlastmonth = date('Y-m-d', $month_start);
	$endlastmonth = date('Y-m-d', $month_end);

    $args = array(
        'post_type'         => 'shop_order',
        'posts_per_page'    => -1,
        'post_status'    => array('wc-completed', 'wc-processing'),
        'orderby' => 'author',
        'date_query' => array( 
            array(
                'after'     => array(
                    'year'  => date('Y', $month_start),
                    'month' => date('m', $month_start),
                    'day'   => date('d', $month_start),
                ),
                'before'    => array(
                    'year'  => date('Y', $month_end),
                    'month' => date('m', $month_end),
                    'day'   => date('d', $month_end),
                ),
                'inclusive' => true,
            ),
        ),
    );
    $orders = get_posts($args);
    $customer = [];
    foreach ( $orders as $orderexport ):
        $order = new IHAG_WC_Order($orderexport->ID);
        if( !in_array($order->get_customer_id(), $customer) && get_post_meta($order->get_ID(), '_payment_method', true ) == 'month'){
            $customer[] = $order->get_customer_id();
            export_month_payment($order->get_customer_id(), $send_email);
        }
    endforeach;
   
}

class PDF extends Fpdi
{
    function cell($w, $h = 0, $txt = '', $border = 0, $ln = 0, $align = '', $fill = false, $link = ''){
        return parent::Cell($w,$h,iconv('UTF-8', 'windows-1252', $txt), $border, $ln, $align, $fill, $link);
    }
     // Pied de page
     function Footer()
     {
        $this->SetFont('Arial','',12);
        $this->SetX(10);
        $this->setY(255); 
        $this->Cell(190,5,'Société Pré état daté en ligne',0,0, 'C');
        $this->Ln(); 
        $this->Cell(190,5,'177 rue Joncours 44100 Nantes',0,0, 'C');
        $this->Ln(); 
        $this->Cell(190,5,'SIRET n° 890 582 372 00011 RCS de Nantes',0,0, 'C');
        $this->Ln(); 
        $this->Cell(190,5,'07 86 09 96 17 - contact@pre-etat-date-en-ligne.com',0,0, 'C');
     }
}
function export_month_payment($get_customer_id, $send_email){
   

    require_once(dirname(__DIR__, 1).'/vendor/setasign/fpdf/fpdf.php');
    require_once(dirname(__DIR__, 1).'/vendor/setasign/fpdi/src/autoload.php');
    global $wpdb, $post;
    $month_start = strtotime('first day of this month', mktime(0, 0, 0, date("m")-1, date("d"),   date("Y")));
	$month_end = strtotime('last day of this month', mktime(23, 59, 59, date("m")-1, date("d"),   date("Y")));
	$startlastmonth = date('Y-m-d', $month_start);
	$endlastmonth = date('Y-m-d', $month_end);




    $pdf = new PDF();
        
    $pdf->SetFont('Arial','',12);
    $pdf->addPage();
    $pdf->Image(dirname(__DIR__, 1).'/pdf/logo-pre-etat-date.png',10,10,80);
    //Adresse du bien
    $x = 120;
    $pdf->SetY(40);
    $pdf->SetX($x);
    //$pdf->Cell(10,7,"Récapitulatif des ventes",0,2);
    $customer = new WC_Customer( $get_customer_id );
    $pdf->Cell(10,5,$customer->get_username(),0,2);
    $pdf->Cell(10,5,$customer->get_billing_company(),0,2);
    $pdf->Cell(10,5,$customer->get_billing_first_name().' '.$customer->get_billing_last_name(),0,2);
    $pdf->Cell(10,5,$customer->get_billing_address_1(),0,2);
    $pdf->Cell(10,5,$customer->get_billing_address_2(),0,2);
    $pdf->Cell(10,5,$customer->get_billing_postcode().' '.$customer->get_billing_city(),0,2);
    
    $pdf->Ln(10);$pdf->SetX($x);
    $pdf->Cell(10,5,'À Nantes,',0,2);
    $pdf->Cell(10,5,'Le '.date_i18n('l j F Y', current_time('timestamp')));

    $pdf->Ln(20); 
    $pdf->SetX(10);
    $pdf->SetFont('Arial','B',16);
    $pdf->Cell(190,5,'Récapitulatif des factures du mois de '.date_i18n('F', $month_start),0,0,'C');
    $pdf->SetFont('Arial','',12);
    
    $pdf->SetFillColor(2,123,108);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetDrawColor(200,200,200);
    $pdf->Ln(20); 
    $pdf->SetX(10);
    $pdf->Cell(130,8,'Prestations',1,0,'C',true);
    $pdf->SetX(140);
    $pdf->Cell(30,8,'Date',1,0,'C',true);
    $pdf->SetX(170);
    $pdf->Cell(30,8,'Tarif TTC',1,0,'C',true);
	//$lastmonth = date('Y-m-d', mktime(0, 0, 0, 12, 31,   date("Y")-1));
	//$lastlastmonth = date('Y-m-d', mktime(0, 0, 0, 01, 01, date("Y")-1));
	
    $pdf->SetTextColor(0,0,0);

	$args = array(
		'post_type'         => 'shop_order',
		'posts_per_page'    => -1,
		'post_status'    => array('wc-completed', 'wc-processing'),
        'date_query' => array( 
            array(
                'after'     => array(
                    'year'  => date('Y', $month_start),
                    'month' => date('m', $month_start),
                    'day'   => date('d', $month_start),
                ),
                'before'    => array(
                    'year'  => date('Y', $month_end),
                    'month' => date('m', $month_end),
                    'day'   => date('d', $month_end),
                ),
                'inclusive' => true,
            ),
        ),
    );
    $orders = get_posts($args);
	$tab_data = array();
	$tab_total_export = array();
    $total = 0;
    $subtotal = 0;
    $page = 0;
    $ligne_de_commande = 0;

	foreach ( $orders as $orderexport ):
		$order = new IHAG_WC_Order($orderexport->ID);
        if($get_customer_id == $order->get_customer_id() && get_post_meta($order->get_ID(), '_payment_method', true ) == 'month'){
            $pdf->Ln(); 
            $pdf->SetX(10);
            $pdf->SetFont('Arial','',9);
            $pdf->Cell(130,8,$order->get_proprio().'('.$order->get_ID().')',1,0);
            $pdf->SetFont('Arial','',12);
            $pdf->SetX(140);
            $order_datetime  = $order->get_date_created(); // Get order created date ( WC_DateTime Object ) 
            $order_timestamp = $order_datetime->getTimestamp();
            $pdf->Cell(30,8,date_i18n('d/m/y', $order_timestamp),1,0,'C');
            $pdf->SetX(170);
            $pdf->Cell(30,8,$order->get_total(),1,0,'C');

            $total += $order->get_total();
            $subtotal += $order->get_subtotal();

            $ligne_de_commande++;
            if(($page == 0 && $ligne_de_commande > 8) ){
                $pdf->addPage();
                $page++;
                $ligne_de_commande = 0;
            }
            elseif($page > 0 && $ligne_de_commande > 20){
                $pdf->addPage();
                $page++;
                $ligne_de_commande = 0;
            }
        }
	endforeach;

    
    
    $pdf->SetDrawColor(100,100,100);

    $pdf->SetFont('Arial','B',12);
    $pdf->Ln(); 
    $pdf->SetX(140);
    $pdf->Cell(30,8,'Total HT',1,0,'C');
    $pdf->SetX(170);
    $pdf->Cell(30,8,number_format(($subtotal),2) . ' €',1,0,'C');

    $pdf->Ln(); 
    $pdf->SetX(140);
    $pdf->Cell(30,8,'Total TVA 20%',1,0,'C');
    $pdf->SetX(170);
    $pdf->Cell(30,8,number_format(($total - $subtotal),2) . ' €',1,0,'C');

    $pdf->SetTextColor(255,255,255);
    $pdf->Ln(); 
    $pdf->SetX(140);
    $pdf->Cell(30,8,'Total TTC',1,0,'C', true);
    $pdf->SetX(170);
    $pdf->Cell(30,8,number_format(($total),2) . ' €',1,0,'C', true);

    $pdf->SetFont('Arial','',12);
    $pdf->SetTextColor(0,0,0);
    $pdf->Ln(10); 
    $pdf->SetX(140);
    $pdf->Cell(60,5,'Philippe Antoine',0,0,'C',);
    $pdf->Ln(); 
    $pdf->SetX(130);
    $pdf->Image(dirname(__DIR__, 1).'/pdf/sign-pre-etat-date.png',null,null,80);

    $pdf->Ln(-30); 
    $pdf->SetX(10);
    $pdf->Cell(120,8,'Conditions de réglement : 30 jours fin de mois',0,0);
    $pdf->SetFont('Arial','',8);
    $pdf->Ln(); 
    $pdf->Cell(120,5,'En cas de retard de paiement, il sera appliqué des pénalités de 5% par mois de retard',0,0);
    $pdf->Ln(); 
    $pdf->Cell(120,5,'En outre, une indemnité forfaitaire pour les frais de recouvrement de 40€ sera due',0,0);

    $upload_dir = wp_upload_dir();
    if(!is_dir($upload_dir['basedir'].'/month')){
        mkdir($upload_dir['basedir'].'/month');
    }
    $pdf->Output('F',$upload_dir['basedir'].'/month/recap_'.(date("m")-1).'-'.date("Y").'_'.$get_customer_id.'.pdf');
        
    if($send_email){
        $subject = get_field("title_email_month_paiement", 'option');
        $body = get_field("content_email_month_paiement", 'option');
        $headers[] = 'From: '.get_bloginfo('name').' <'. get_option( 'admin_email') .'>';
        wp_mail( array($customer->get_email(),get_option( 'admin_email')), $subject, $body, $headers, array($upload_dir['basedir'].'/month/recap_'.(date("m")-1).'-'.date("Y").'_'.$get_customer_id.'.pdf'));	
    }

    return;

}

function export_order($send_email, $PaymentMode){
   

    require_once(dirname(__DIR__, 1).'/vendor/setasign/fpdf/fpdf.php');
    require_once(dirname(__DIR__, 1).'/vendor/setasign/fpdi/src/autoload.php');
    global $wpdb, $post;
    $month_start = strtotime('first day of this month', mktime(0, 0, 0, date("m")-1, date("d"),   date("Y")));
	$month_end = strtotime('last day of this month', mktime(23, 59, 59, date("m")-1, date("d"),   date("Y")));
	$startlastmonth = date('Y-m-d', $month_start);
	$endlastmonth = date('Y-m-d', $month_end);

   
    $pdf = new PDF();
        
    $pdf->SetFont('Arial','',12);
    $pdf->addPage();
    $pdf->Image(dirname(__DIR__, 1).'/pdf/logo-pre-etat-date.png',10,10,80);
    //Adresse du bien
    $x = 120;
    $pdf->SetY(40);
    $pdf->SetX($x);
    //$pdf->Cell(10,7,"Récapitulatif des ventes",0,2);
    
    $pdf->Ln(10);$pdf->SetX($x);
    $pdf->Cell(10,5,'À Nantes,',0,2);
    $pdf->Cell(10,5,'Le '.date_i18n('l j F Y', current_time('timestamp')));

    $pdf->Ln(20); 
    $pdf->SetX(10);
    $pdf->SetFont('Arial','B',16);
    $pdf->Cell(190,5,'Récapitulatif des factures du mois de '.date_i18n('F', $month_start).' ('.$PaymentMode.')',0,0,'C');
    $pdf->SetFont('Arial','',12);
    
    $pdf->SetFillColor(2,123,108);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetDrawColor(200,200,200);
    $pdf->Ln(20); 
    $pdf->SetX(10);
    $pdf->Cell(130,8,'Prestations',1,0,'C',true);
    $pdf->SetX(140);
    $pdf->Cell(30,8,'Date',1,0,'C',true);
    $pdf->SetX(170);
    $pdf->Cell(30,8,'Tarif TTC',1,0,'C',true);
	//$lastmonth = date('Y-m-d', mktime(0, 0, 0, 12, 31,   date("Y")-1));
	//$lastlastmonth = date('Y-m-d', mktime(0, 0, 0, 01, 01, date("Y")-1));
	
    $pdf->SetTextColor(0,0,0);

	$args = array(
        'post_type'         => 'shop_order',
        'posts_per_page'    => -1,
        'post_status'    => array('wc-completed', 'wc-processing'),
        'date_query' => array(
            array(
                'after'     => array(
                    'year'  => date('Y', $month_start),
                    'month' => date('m', $month_start),
                    'day'   => date('d', $month_start),
                ),
                'before'    => array(
                    'year'  => date('Y', $month_end),
                    'month' => date('m', $month_end),
                    'day'   => date('d', $month_end),
                ),
                'inclusive' => true,
            ),
        ),
    );
    $orders = get_posts($args);
	$tab_data = array();
	$tab_total_export = array();
    $total = 0;
    $subtotal = 0;
    $page = 0;
    $ligne_de_commande = 0;

	foreach ( $orders as $orderexport ):
		$order = new IHAG_WC_Order($orderexport->ID);
        if(get_post_meta($order->get_ID(), '_payment_method', true ) == $PaymentMode){
            $pdf->Ln(); 
            $pdf->SetX(10);
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(130,8, $order->get_proprio().'('.$order->get_ID().')',1,0);
            $pdf->SetFont('Arial','',12);
            $pdf->SetX(140);
            $order_datetime  = $order->get_date_created(); // Get order created date ( WC_DateTime Object ) 
            $order_timestamp = $order_datetime->getTimestamp();
            $pdf->Cell(30,8,date_i18n('d/m/y', $order_timestamp),1,0,'C');
            $pdf->SetX(170);
            $pdf->Cell(30,8,$order->get_total(),1,0,'C');

            $total += $order->get_total();
            $subtotal += $order->get_subtotal();

            $ligne_de_commande++;
            if(($page == 0 && $ligne_de_commande > 8) ){
                $pdf->addPage();
                $page++;
                $ligne_de_commande = 0;
            }
            elseif($page > 0 && $ligne_de_commande > 20){
                $pdf->addPage();
                $page++;
                $ligne_de_commande = 0;
            }
        }
	endforeach;

    
    
    $pdf->SetDrawColor(100,100,100);

    $pdf->SetFont('Arial','B',12);
    $pdf->Ln(); 
    $pdf->SetX(140);
    $pdf->Cell(30,8,'Total HT',1,0,'C');
    $pdf->SetX(170);
    $pdf->Cell(30,8,number_format(($subtotal),2) . ' €',1,0,'C');

    $pdf->Ln(); 
    $pdf->SetX(140);
    $pdf->Cell(30,8,'Total TVA 20%',1,0,'C');
    $pdf->SetX(170);
    $pdf->Cell(30,8,number_format(($total - $subtotal),2) . ' €',1,0,'C');

    $pdf->SetTextColor(255,255,255);
    $pdf->Ln(); 
    $pdf->SetX(140);
    $pdf->Cell(30,8,'Total TTC',1,0,'C', true);
    $pdf->SetX(170);
    $pdf->Cell(30,8,number_format(($total),2) . ' €',1,0,'C', true);

    $pdf->SetFont('Arial','',12);
    $pdf->SetTextColor(0,0,0);
    $pdf->Ln(10); 
    $pdf->SetX(140);
    $pdf->Cell(60,5,'Philippe Antoine',0,0,'C',);
    $pdf->Ln(); 
    $pdf->SetX(130);
    $pdf->Image(dirname(__DIR__, 1).'/pdf/sign-pre-etat-date.png',null,null,80);

    $pdf->Ln(-30); 
    $pdf->SetX(10);
    $pdf->Cell(120,8,'Conditions de réglement : 30 jours fin de mois',0,0);
    $pdf->SetFont('Arial','',8);
    $pdf->Ln(); 
    $pdf->Cell(120,5,'En cas de retard de paiement, il sera appliqué des pénalités de 5% par mois de retard',0,0);
    $pdf->Ln(); 
    $pdf->Cell(120,5,'En outre, une indemnité forfaitaire pour les frais de recouvrement de 40€ sera due',0,0);

    $upload_dir = wp_upload_dir();
    if(!is_dir($upload_dir['basedir'].'/month')){
        mkdir($upload_dir['basedir'].'/month');
    }
    $pdf->Output('F',$upload_dir['basedir'].'/month/recap_'.$PaymentMode.'_'.(date("m")-1).'-'.date("Y").'.pdf');
        
    if($send_email){
        $subject = 'récap mensuel';
        $body = 'Récap mensuel';
        $headers[] = 'From: '.get_bloginfo('name').' <'. get_option( 'admin_email') .'>';
        wp_mail( get_option( 'admin_email'), $subject, $body, $headers, array($upload_dir['basedir'].'/month/recap_'.$PaymentMode.'_'.(date("m")-1).'-'.date("Y").'.pdf'));	
    }

    return;

}


add_action('admin_menu', 'ihag_menu_recap_payment');
function ihag_menu_recap_payment() {
    add_submenu_page(
        'tools.php',
        'toolsPayment',
        'Recap paiement',
        'manage_options',
        'toolsPayment',
        'toolsPayment',
    );
    global $submenu;
}


function toolsPayment(){
    ihag_month_recap_fun_go();
    ?>
    <div class="wrap">
        <h1>Récape de paiements </h1>
        <h2>Paiement au mois</h2>
        <table>
        <?php
        $upload_dir = wp_upload_dir();
        if(!is_dir($upload_dir['basedir'].'/month')){
            mkdir($upload_dir['basedir'].'/month');
        }
        $month_start = strtotime('first day of this month', mktime(0, 0, 0, date("m")-1, date("d"),   date("Y")));
        $month_end = strtotime('last day of this month', mktime(23, 59, 59, date("m")-1, date("d"),   date("Y")));
        $startlastmonth = date('Y-m-d', $month_start);
        $endlastmonth = date('Y-m-d', $month_end);

        $args = array(
            'post_type'         => 'shop_order',
            'posts_per_page'    => -1,
            'post_status'    => array('wc-completed', 'wc-processing'),
            'orderby' => 'author',
            'date_query' => array(
                array(
                    'after'     => array(
                        'year'  => date('Y', $month_start),
                        'month' => date('m', $month_start),
                        'day'   => date('d', $month_start),
                    ),
                    'before'    => array(
                        'year'  => date('Y', $month_end),
                        'month' => date('m', $month_end),
                        'day'   => date('d', $month_end),
                    ),
                    'inclusive' => true,
                ),
            ),
        );
        $orders = get_posts($args);
        $customer = [];
        foreach ( $orders as $orderexport ):
            $order = new IHAG_WC_Order($orderexport->ID);
            if( !in_array($order->get_customer_id(), $customer) && get_post_meta($order->get_ID(), '_payment_method', true ) == 'month'){
                $customer[] = $order->get_customer_id();
                $user = new WC_Customer( $order->get_customer_id() );
                echo '<tr><td>'.$user->get_email().'</td><td><a target="_blank" href="'.$upload_dir['baseurl'].'/month/recap_'.(date("m")-1).'-'.date("Y").'_'.$order->get_customer_id().'.pdf'.'" class="button">Télécharger</a></td></tr>';
            }
        endforeach;
        ?>
        </table>
        <h2>Commandes du mois de <?php echo date_i18n('F', strtotime("last month")).'</h2>';
        
        echo '<p><a target="_blank" href="'.$upload_dir['baseurl'].'/month/recap_month_'.(date("m")-1).'-'.date("Y").'.pdf'.'" class="button">Télécharger (au mois)</a></p>';
        echo '<p><a target="_blank" href="'.$upload_dir['baseurl'].'/month/recap_cheque_'.(date("m")-1).'-'.date("Y").'.pdf'.'" class="button">Télécharger (cheque)</a></p>';
        echo '<p><a target="_blank" href="'.$upload_dir['baseurl'].'/month/recap_bacs_'.(date("m")-1).'-'.date("Y").'.pdf'.'" class="button">Télécharger (virement)</a></p>';
        echo '<p><a target="_blank" href="'.$upload_dir['baseurl'].'/month/recap_systempaystd_'.(date("m")-1).'-'.date("Y").'.pdf'.'" class="button">Télécharger (CB)</a></p>';
        
        ?>

        <table>
        <?php
        /*$args = array(
            'post_type'         => 'shop_order',
            'posts_per_page'    => -1,
            'post_status'    => array('wc-completed','wc-pending', 'wc-proccessing', 'wc-on-hold' ),
            'date_query' => array(
                array(
                    'after'     => array(
                        'year'  => date('Y', $month_start),
                        'month' => date('m', $month_start),
                        'day'   => date('d', $month_start),
                    ),
                    'before'    => array(
                        'year'  => date('Y', $month_end),
                        'month' => date('m', $month_end),
                        'day'   => date('d', $month_end),
                    ),
                    'inclusive' => true,
                ),
            ),
        );
        $orders = get_posts($args);
        $customer = [];
        foreach ( $orders as $orderexport ):
            $order = new IHAG_WC_Order($orderexport->ID);
            $user = new WC_Customer( $order->get_customer_id() );
            $order_datetime  = $order->get_date_created(); // Get order created date ( WC_DateTime Object ) 
            $order_timestamp = $order_datetime->getTimestamp();
            echo '<tr>
                <td>'.date_i18n('d/m/y', $order_timestamp).'</td>
                <td>'.$user->get_email().'</td>
                <td><a href="'.get_admin_url().'post.php?post='.$order->get_ID().'&action=edit'.'" target="_blank">'.$order->get_ID().'</a></td>
                <td>'.get_post_meta($order->get_ID(), '_payment_method', true ).'</td><td>'.$order->get_status().'</td></tr>';
        endforeach;*/
        ?>
        </table>


    <?php
}