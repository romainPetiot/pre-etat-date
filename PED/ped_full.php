<?php
add_action('rest_api_init', function() {
	
	register_rest_route( 'ped', 'createCoordonate', 
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'createCoordonate',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);

	register_rest_route( 'ped', 'createBilan', 
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'addVarPED',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);

	register_rest_route( 'ped', 'createAnnex', 
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'addVarPED',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);

});


//Etape 1 formule Full
function createCoordonate( WP_REST_Request $request ){
	if ( check_nonce() ) {
		include_once WC_ABSPATH . 'includes/wc-cart-functions.php';
		include_once WC_ABSPATH . 'includes/class-wc-cart.php';

		global $woocommerce;
		if ( is_null( $woocommerce->cart ) ) {
			wc_load_cart();
		}
		$items = $woocommerce->cart->get_cart();

		$params = $request->get_params();
		foreach($items as $item => $values) { 
			$product_id = $values['data']->get_id(); 
		} 
		$product_attributes = get_post_meta($product_id, '_product_attributes', true);
		if(!is_array($product_attributes)){
			$product_attributes = [];
		}
		foreach($params as $name => $value){
			$product_attributes[] = array (
				'name' => htmlspecialchars( stripslashes( $name ) ), // set attribute name
				'value' => $value, // set attribute value
				'position' => 1,
				'is_visible' => 1,
				'is_variation' => 1,
				'is_taxonomy' => 0
			);
		}
		
		$get_current_user_id = get_post_meta( $product_id, '_user_id',true );

		update_user_meta( $get_current_user_id, "billing_first_name", wc_clean( $params["firstnameOwner1"]) );
		update_user_meta( $get_current_user_id, "billing_last_name", wc_clean( $params["lastnameOwner1"]) );
		update_user_meta( $get_current_user_id, "billing_address_1", wc_clean( $params["addressOwner1"]) );
		update_user_meta( $get_current_user_id, "billing_address_2", wc_clean( $params["address2Owner1"]) );
		update_user_meta( $get_current_user_id, "billing_postcode", wc_clean( $params["cpOwner1"]) );
		update_user_meta( $get_current_user_id, "billing_city", wc_clean( $params["cityOwner1"]) );

		update_post_meta($product_id, '_product_attributes', $product_attributes);

		return new WP_REST_Response( true, 200 );
	}
}

