<?php

class IHAG_WC_Order extends WC_Order{

   // public $referral, $writing_status, $difficulty, $timer;


    public function __construct($id){
        parent::__construct($id);
        /*$this->$referral = $this->get_referral();
        $this->$writing_status = $this->get_writing_status();
        $this->$difficulty = $this->get_difficulty();
        $this->$timer = $this->get_timer();*/
    }

    public function get_referral(){
        return get_post_meta( $this->get_id(), '_referral_person', true );
    }

    public function set_referral($referral){
        update_post_meta( $this->get_id(), '_referral_person', $referral  );
    }

    public function set_referral_time(){
        update_post_meta( $this->get_id(), '_referral_time', time() );
    }

    public function get_referral_time(){
        if(empty(get_post_meta( $this->get_id(), '_referral_time', true )) || get_post_meta( $this->get_id(), '_referral_time', true ) == 0){
            if(!empty($this->get_referral())){
                $this->set_referral_time();
            }
            return time();
        }
        return get_post_meta( $this->get_id(), '_referral_time', true );
    }

    /*public function ped_redactor_free(){
        if(($this->get_referral_time() + (30 * 60) ) < time()){
            if($this->get_writing_status() == 'processing'){
                delete_post_meta( $this->get_id(), '_referral_person');
                delete_post_meta( $this->get_id(), '_referral_time');
            }
        }
    }*/

    public function get_writing_status(){
        $status = get_post_meta( $this->get_id(), '_writting_status', true );
        if(empty($status)){
            $status = 'processing';
            $this->set_writing_status($status);
        }
        return $status;
    }

    public function set_writing_status($status){
        update_post_meta( $this->get_id(), '_writting_status', $status  );
    }

    public function get_label_writing_status(){
        switch ($this->get_writing_status()){
            case 'processing':
                return 'À rédiger';
            case 'pending':
                return 'En attente de doc';
            case 'to_check':
                return 'À checker';
            case 'block':
                return 'Boquée';
            case 'completed':
                return 'Terminée';
        }
    }

    public function get_color_writing_status(){
        switch ($this->get_writing_status()){
            case 'processing':
                return 'green';
            case 'pending':
                return 'red';
            case 'to_check':
                return 'orange';
            case 'block':
                return 'red';
            case 'completed':
                return 'purple';
        }
    }

    public function get_difficulty(){
        return get_post_meta( $this->get_id(), '_difficulty_level', true );
    }

    public function set_difficulty($difficulty){
        update_post_meta( $this->get_id(), '_difficulty_level', $difficulty  );
    }

    public function get_message_difficulty(){
        return get_post_meta( $this->get_id(), '_difficulty_message', true );
    }

    public function set_message_difficulty($difficulty){
        update_post_meta( $this->get_id(), '_difficulty_message', $difficulty  );
    }

    
    public function get_timer(){
        return get_post_meta( $this->get_id(), '_timer', true );
    }

    public function set_timer($timer){
        update_post_meta( $this->get_id(), '_timer', $timer  );
    }

    public function autonome(){
        return (bool)($this->get_total() ==  30);
    }

    public function get_payment_link(){
        return 'TEST';
    }

    public function get_proprio(){
        $items = $this->get_items();
        foreach ( $items as $item ) {
            $product = new WC_Product($item->get_product_id());
            if($product->is_downloadable() ){
                return $product->get_name();
            }
        }
    }

}