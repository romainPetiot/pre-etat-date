<?php

if ( ! wp_next_scheduled( 'ihag_order_completed' ) ) {
	wp_schedule_event( time(), 'hourly', 'ihag_order_completed' );
}

if ( ! wp_next_scheduled( 'ihag_cart_abandonment' ) ) {
	wp_schedule_event( time(), 'hourly', 'ihag_cart_abandonment' );
}

//notif au partenaire
/*add_filter( 'woocommerce_email_recipient_new_order' , 'partner_add_recipient', 20, 2 );
function partner_add_recipient( $recipient, $order ) {
	if(!is_null($order)){
		$items = $order->get_items();
		foreach($items as $item => $values) {
			$product_id = $values->get_product_id();
			$id_ped = get_post_meta($product_id, '_id_ped', true);
			if(get_field("send_ped_partner", $id_ped)){
				$recipient = $recipient . ', ' . get_field("partner_email", $id_ped);
			}		
		}
	}
    return $recipient;
}
*/

/*add_filter( 'woocommerce_email_attachments', 'bbloomer_attach_pdf_to_emails', 10, 4 );
 
function bbloomer_attach_pdf_to_emails( $attachments, $email_id, $order, $email ) {
	if(isset($order->data['status']) && !empty($order->data['status'])){
		$status = $order->data['status'] ;
		$order = new IHAG_WC_Order($order->get_ID());
		if( ($order->get_writing_status() == 'completed' && $status == 'completed') ||  $order->get_writing_status() == 'completed' && get_post_meta($order->get_ID(), '_payment_method', true ) == 'month'){
			$items = $order->get_items();
			foreach ( $items as $item ) {
				$product = new WC_Product($item->get_product_id());
				if($product->is_downloadable() && sizeof($product->get_files()) > 0){
					$upload_dir = wp_upload_dir();
					$attachments[] = $upload_dir['basedir'] . '/pdf/pre-etat-date'.$item->get_product_id().'.pdf';		
				}
			}
		}
	}
  
    return $attachments;
}
*/

// 1 fois par heure, les orders qui sont en process avec un ped passe en status completed
add_action( 'ihag_order_completed', 'ihag_order_completed_func' );
function ihag_order_completed_func() {
    $args = array(
		'post_type'         => 'shop_order',
		'posts_per_page'    => -1,
		'post_status'    => 'wc-processing'
	);
	$orders = get_posts($args);
	foreach ( $orders as $order ):
		$order = new IHAG_WC_Order($order->ID);
		if (!empty($order) && $order->get_writing_status() == 'completed' && $order->get_status() != 'completed') {
			$order->update_status( 'completed' );
		}
		if (!empty($order)&& $order->get_writing_status() == 'processing' && $order->autonome()) { 
			$items = $order->get_items();
			foreach ( $items as $item ) {
				$product = new WC_Product($item->get_product_id());
				if($product->is_downloadable() && sizeof($product->get_files()) > 0){
					$order->update_status( 'completed' );
					break;
				}
			}
		}
	endforeach;

	
} 

// panier abandonné : toutes les heures, si plus de deux heures sans paiement
add_action( 'ihag_cart_abandonment', 'ihag_cart_abandonment_func' );
function ihag_cart_abandonment_func() {
    global $wpdb;
	$tabIDProductInOrder = array();
	$results = $wpdb->get_results('select product_id FROM '.$wpdb->prefix.'wc_order_product_lookup');
	foreach($results as $result){
		$tabIDProductInOrder[] = $result->product_id;
	}

	$args = array(
		'post_type'         => 'product',
		'posts_per_page'    => -1,
		'order' 			=> 'desc',
		'exclude'			=> $tabIDProductInOrder,
		'date_query'    => array(
			'column'  => 'post_date',
			'before'   => '- 2 hours'
		),
		'meta_query' => array(
			array(
				'key' 		=> 'relance',
				'compare' 	=> 'NOT EXISTS' ,
			),
		),
	);
	$products = get_posts($args);
	foreach ( $products as $product ){
		update_post_meta( $product->ID, 'relance', 'yes' );
		$user = get_user_by( "ID", get_post_meta( $product->ID, '_user_id', true));
		$subject = get_field("title_email_cart_abandonment", 'option');
		$body = get_field("content_email_cart_abandonment", 'option');
		$headers[] = 'From: '.get_bloginfo('name').' <'. get_option( 'admin_email') .'>';
		wp_mail( $user->user_email, $subject, $body, $headers, $attachments);		
	}
}



add_action('admin_menu', 'ihag_user_sub_menu');
function ihag_user_sub_menu() {
    add_submenu_page(
        'tools.php',
        'toolsRedactor',
        'Export CSV rédacteur',
        'manage_options',
        'toolsRedactor',
        'toolsRedactor',
    );
    global $submenu;
}


function toolsRedactor(){
    ?>
    <div class="wrap">
        <h1>Export mensuel des rédacteurs (mois précédent) </h1>
        <a href="?report=exportRedactor" class="button">Export rédacteurs</a>
		<!--<a href="?report=backup" class="button">Backup (temp)</a>-->
		<!--<a href="?report=clean" class="button">Clean</a>-->
    <?php
}

add_action('init', function() {
    if(isset($_GET['report']) && $_GET['report'] == 'exportRedactor'){
        exportRedactor();
    }

	if(isset($_GET['report']) && $_GET['report'] == 'clean'){
        //cleanFile();
    }

	if(isset($_GET['report']) && $_GET['report'] == 'backup'){
		set_time_limit(0);
        ihag_backup_func();
    }
   
});

if ( ! wp_next_scheduled( 'ihag_backup' ) ) {
	wp_schedule_event( time(), 'weekly', 'ihag_backup' );
}
add_action( 'ihag_backup', 'ihag_backup_func' );
function ihag_backup_func(){
	set_time_limit(0);
	// Mise en place d'une connexion basique
	$ftp_server = 'ftp.cluster031.hosting.ovh.net';
	$ftp_user_name = 'dttbojf';
	$ftp_user_pass = 'Jemange1frigomalin';
	$destination_file = '/ped/custom/';

	$ftp = ftp_connect($ftp_server);

	// Identification avec un nom d'utilisateur et un mot de passe
	$login_result = ftp_login($ftp, $ftp_user_name, $ftp_user_pass);

	// Vérification de la connexion
	if ((!$ftp) || (!$login_result)) {
		echo "La connexion FTP a échoué !";
		return;
	} else {
		$peremption = mktime(0, 0, 0, date("m")-3, date("d"),   date("Y"));
		$increment = 0;
		$upload_dir = wp_upload_dir();
		$dir = $upload_dir['basedir'].'/pdf';
		if (is_dir($dir)) {
			if ($dh = opendir($dir)) {
				while (($file = readdir($dh)) !== false) {
					$upload = ftp_put($ftp, $destination_file.$file, $dir.'/'.$file, FTP_BINARY);
					$increment++;
					if($increment % 10 == 0){sleep(1);} 
					if(filemtime($dir.'/'.$file) < $peremption){
						unlink($dir.'/'.$file);
					}
				}
				closedir($dh);
			}
		}


		$increment = 0;
		$upload_dir = wp_upload_dir();
		$dir = $upload_dir['basedir'].'/fileAnnexesComptables';
		if (is_dir($dir)) {
			if ($dh = opendir($dir)) {
				while (($file = readdir($dh)) !== false) {
					$upload = ftp_put($ftp, $destination_file.'fileAnnexesComptables/'.$file, $dir.'/'.$file, FTP_BINARY);
					$increment++;
					if($increment % 10 == 0){sleep(1);} 
					
					if(filemtime($dir.'/'.$file) < $peremption){
						unlink($dir.'/'.$file);
					}
				}
				closedir($dh);
			}
		}

		$increment = 0;
		$upload_dir = wp_upload_dir();
		$dir = $upload_dir['basedir'].'/fileAppelFonds';
		if (is_dir($dir)) {
			if ($dh = opendir($dir)) {
				while (($file = readdir($dh)) !== false) {
					$upload = ftp_put($ftp, $destination_file.'fileAppelFonds/'.$file, $dir.'/'.$file, FTP_BINARY);
					$increment++;
					if($increment % 10 == 0){sleep(1);} 
					if(filemtime($dir.'/'.$file) < $peremption){
						unlink($dir.'/'.$file);
					}
				}
				closedir($dh);
			}
		}

		// Fermeture de la connexion FTP
		ftp_close($ftp);
	}
}
/*
function cleanFile(){
	global $wpdb, $post;
	include_once WC_ABSPATH . 'includes/wc-cart-functions.php';
	include_once WC_ABSPATH . 'includes/class-wc-cart.php';


//suppression commandes (uniquement produit) abandonnées de plus d'un mois
	$tabIDProductInOrder = array();
	$results = $wpdb->get_results('select product_id FROM '.$wpdb->prefix.'wc_order_product_lookup');
	foreach($results as $result){
		$tabIDProductInOrder[] = $result->product_id;
	}
	$args = array(
		'post_type'         => 'product',
		'posts_per_page'    => -1,
		'order' 			=> 'desc',
		'exclude'			=> $tabIDProductInOrder,
		'date_query'    => array(
			'column'  => 'post_date',
			'before'   => '1 month ago'
		)
	);
	$products = get_posts($args);
	foreach ( $products as $item ){
		global $product;
		$product = new WC_Product($item->ID);
		$attr = array();
		foreach ( $product->get_attributes() as $attribute ):
			$attribute_data = $attribute->get_data(); // Get the data in an array
			$attr[$attribute_data['name']] = $attribute_data['options'][0];
		endforeach;

		if(isset($attr['fileAppelFonds']) && !empty($attr['fileAppelFonds'])){
			wp_delete_attachment($attr['fileAppelFonds'], true);
			$attr['fileAppelFonds'] = '';
		}
	
		if(isset($attr['fileAnnexesComptables']) && !empty($attr['fileAnnexesComptables'])){
			wp_delete_attachment($attr['fileAnnexesComptables'], true);
			$attr['fileAnnexesComptables'] = '';
		}
	
		if(isset($attr['fileDecompteChargen1']) && !empty($attr['fileDecompteChargen1'])){
			wp_delete_attachment($attr['fileDecompteChargen1'], true);
			$attr['fileDecompteChargen1'] = '';
		}
	
		if(isset($attr['fileDecompteChargen2']) && !empty($attr['fileDecompteChargen2'])){
			wp_delete_attachment($attr['fileDecompteChargen2'], true);
			$attr['fileDecompteChargen2'] = '';
		}
	
		$tab_pdf_gallery = get_post_meta($item->ID, 'pdf-gallery', true );
		if(($tab_pdf_gallery)){
			foreach($tab_pdf_gallery as $pdf_gallery){
				wp_delete_attachment($pdf_gallery, true);
				
			}
			delete_post_meta($item->ID, 'pdf-gallery');
		}
		update_post_meta($item->ID, '_product_attributes', $attr);

		$argsAttachments = array(
			'post_type' => 'attachment',
			'numberposts' => -1,
			'post_status' => null,
			'post_parent' => $item->ID,
		);
		$attachments = get_posts($argsAttachments);
		if($attachments){
			foreach($attachments as $attachment){
				wp_delete_attachment($attachment->ID, true);
			}
		}

		wp_delete_post($item->ID, true);
		error_log('Delete file and product '. $item->ID);
	}


	//suppression des fichiers des commandes finalisées de plus d'un an

	$yearPrev = date('Y-m-d', mktime(0, 0, 0, date("m")-6, date("d"),   date("Y")));
	$args = array(
		'post_type'         => 'shop_order',
		'posts_per_page'    => -1,
		'post_status'    => 'wc-completed',
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => '_completed_date',
				'value'   => $yearPrev,
				'type'    => 'DATE',
				'compare' => '<=',
			),
		),
		
		'orderby'   => 'meta_value_num' ,
		//'meta_key'  => '_referral_person',
	);
	$orders = get_posts($args);
	foreach ( $orders as $orderexport ):
		$order = new WC_Order($orderexport->ID);
		if (!empty($order)) {
			$items = $order->get_items();
			$bool_delete = false;
			foreach ( $items as $item ) {
				
				global $product;
				$product = new WC_Product($item->get_product_id());
				$attr = array();
				foreach ( $product->get_attributes() as $attribute ):
					$attribute_data = $attribute->get_data(); // Get the data in an array
					$attr[$attribute_data['name']] = $attribute_data['options'][0];
				endforeach;

				if(isset($attr['fileAppelFonds']) && !empty($attr['fileAppelFonds'])){
					wp_delete_attachment($attr['fileAppelFonds'], true);
					$bool_delete = true;
					$attr['fileAppelFonds'] = '';
				}
			
				if(isset($attr['fileAnnexesComptables']) && !empty($attr['fileAnnexesComptables'])){
					wp_delete_attachment($attr['fileAnnexesComptables'], true);
					$bool_delete = true;
					$attr['fileAnnexesComptables'] = '';
				}
			
				if(isset($attr['fileDecompteChargen1']) && !empty($attr['fileDecompteChargen1'])){
					wp_delete_attachment($attr['fileDecompteChargen1'], true);
					$bool_delete = true;
					$attr['fileDecompteChargen1'] = '';
				}
			
				if(isset($attr['fileDecompteChargen2']) && !empty($attr['fileDecompteChargen2'])){
					wp_delete_attachment($attr['fileDecompteChargen2'], true);
					$bool_delete = true;
					$attr['fileDecompteChargen2'] = '';
				}
			
				$tab_pdf_gallery = get_post_meta($item->get_product_id(), 'pdf-gallery', true );
				if(($tab_pdf_gallery)){
					foreach($tab_pdf_gallery as $pdf_gallery){
						wp_delete_attachment($pdf_gallery, true);
						$bool_delete = true;
					}
					delete_post_meta($item->get_product_id(), 'pdf-gallery');
				}
				update_post_meta($item->get_product_id(), '_product_attributes', $attr);
			}
			if($bool_delete){
				error_log('delete files order : '.$item->get_product_id());
			}
		}
	endforeach;
	

	error_log('End clean');


}
*/

function exportRedactor(){
    global $wpdb, $post;
    
    $output_filename = "redacteur_".date("Y-m-d H:i:s").'.csv';
    $output_handle = @fopen( 'php://output', 'w' );
    header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
    header( 'Content-Description: File Transfer' );
    header( 'Content-type: text/csv' );
    header( 'Content-Disposition: attachment; filename=' . $output_filename );
    header( 'Expires: 0' );
    header( 'Pragma: public' );
    // Insert header row
    


	$month_start = strtotime('first day of this month', mktime(0, 0, 0, date("m")-1, date("d"),   date("Y")));
	$month_end = strtotime('last day of this month', mktime(23, 59, 59, date("m")-1, date("d"),   date("Y")));
	
	$startlastmonth = date('Y-m-d', $month_start);
	$endlastmonth = date('Y-m-d', $month_end);
	
	//$lastmonth = date('Y-m-d', mktime(0, 0, 0, 12, 31,   date("Y")-1));
	//$lastlastmonth = date('Y-m-d', mktime(0, 0, 0, 01, 01, date("Y")-1));
	
	$args = array(
		'post_type'         => 'shop_order',
		'posts_per_page'    => -1,
		'post_status'    => 'wc-completed',
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => '_completed_date',
				'value'   => array($startlastmonth, $endlastmonth),
				'type'    => 'DATE',
				'compare' => 'BETWEEN',
			),
		),
		
		'orderby'   => 'meta_value_num' ,
		'meta_key'  => '_referral_person',
	);
	$orders = get_posts($args);
	$tab_data = array();
	$tab_total_redactor = array();
	foreach ( $orders as $orderexport ):
		$user_ID = get_post_meta($orderexport->ID, '_referral_person', true);
		$user_info = get_userdata($user_ID);
		$tab_data[] = array( 
            $user_info->display_name,
            get_post_meta($orderexport->ID, '_completed_date', true), 
            $orderexport->ID,
        );
		if(!isset($tab_total_redactor[$user_ID])){
			$tab_total_redactor[$user_ID] = 0;
		}
		$tab_total_redactor[$user_ID]++;
	endforeach;

	foreach ($tab_total_redactor as $key => $value) {
		$user_info = get_userdata($key);
		fputcsv( $output_handle, array(
			$user_info->display_name.'('.$key.')',
			$value
		),";" );
	}

	$csv_fields=array();
    $csv_fields[] = 'Rédacteur';
	$csv_fields[] = 'Date';
    $csv_fields[] = 'num PED';
	fputcsv( $output_handle, $csv_fields,";" );
    foreach ( $tab_data as $data ){
        fputcsv( $output_handle, $data,";");
	}
    


    fclose( $output_handle );
    exit();
}