<?php

function ihag_custom_role_representative() {
    add_role( 'representative', __("Mandataire", "ped"), get_role( 'customer' )->capabilities );
}
add_action( 'init', 'ihag_custom_role_representative' );

/*
add_filter( 'woocommerce_billing_fields', 'ihag_update_field_representative', 1, 1 );
function ihag_update_field_representative( $address_fields ) {
	if(is_representative()){
		$address_fields['billing_email']['label'] = 'Email de votre contact (pour envoyer la demande de paiement)';	
	}

  return $address_fields;
}*/


function is_representative(){
	
	$user_id = get_current_user_id();
	if($user_id == 0){return false;}
	$user_meta = get_userdata($user_id);
	$user_roles = $user_meta->roles;
	return in_array('representative', $user_roles);
}

add_filter( 'woocommerce_email_recipient_customer_completed_order', 'add_representative_email', 10, 2);
function add_representative_email($recipient, $order) {
	error_log($order->get_id());
	$email_representative = get_post_meta($order->get_id(), 'email_representative', true);
	if($email_representative){
		$recipient = $recipient . ', '.$email_representative;
	}
	error_log($recipient);
    return $recipient;
}

add_action('init', 'init_custom_gateway_class');
function init_custom_gateway_class(){

    class WC_Gateway_Custom extends WC_Payment_Gateway {

        public $domain;

        /**
         * Constructor for the gateway.
         */
        public function __construct() {

            $this->domain = 'custom_payment';

            $this->id                 = 'custom';
            $this->icon               = apply_filters('woocommerce_custom_gateway_icon', '');
            $this->has_fields         = false;
            $this->method_title       = __( 'Mandat', $this->domain );
            $this->method_description = __( 'Allows payments with custom gateway.', $this->domain );

            // Load the settings.
            $this->init_form_fields();
            $this->init_settings();

            // Define user set variables
            $this->title        = $this->get_option( 'title' );
            $this->description  = $this->get_option( 'description' );
            $this->instructions = $this->get_option( 'instructions', $this->description );
            $this->order_status = $this->get_option( 'order_status', 'completed' );

            // Actions
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
            add_action( 'woocommerce_thankyou_' . $this->id, array( $this, 'thankyou_page' ) );

            // Customer Emails
            add_action( 'woocommerce_email_before_order_table', array( $this, 'email_instructions' ), 10, 3 );
        }

        /**
         * Initialise Gateway Settings Form Fields.
         */
        public function init_form_fields() {

            $this->form_fields = array(
                'enabled' => array(
                    'title'   => __( 'Enable/Disable', $this->domain ),
                    'type'    => 'checkbox',
                    'label'   => __( 'Activer paiement délégué', $this->domain ),
                    'default' => 'yes'
                ),
                'title' => array(
                    'title'       => __( 'Title', $this->domain ),
                    'type'        => 'text',
                    'description' => __( 'This controls the title which the user sees during checkout.', $this->domain ),
                    'default'     => __( 'Paiement délégué (Pour les mandataires)', $this->domain ),
                    'desc_tip'    => true,
                ),
                'order_status' => array(
                    'title'       => __( 'Order Status', $this->domain ),
                    'type'        => 'select',
                    'class'       => 'wc-enhanced-select',
                    'description' => __( 'Choose whether status you wish after checkout.', $this->domain ),
                    'default'     => 'wc-pending',
                    'desc_tip'    => true,
                    'options'     => wc_get_order_statuses()
                ),
                'description' => array(
                    'title'       => __( 'Description', $this->domain ),
                    'type'        => 'textarea',
                    'description' => __( 'Payment method description that the customer will see on your checkout.', $this->domain ),
                    'default'     => __("Si vous êtes agent immobilier et que vous effectuez le pré-état daté pour le compte de votre client, vous pouvez renseigner son email afin qu'il réalise le paiement.", $this->domain),
                    'desc_tip'    => true,
                ),
                'instructions' => array(
                    'title'       => __( 'Instructions', $this->domain ),
                    'type'        => 'textarea',
                    'description' => __( 'Instructions that will be added to the thank you page and emails.', $this->domain ),
                    'default'     => 'La demande de paiement est envoyée à votre contact',
                    'desc_tip'    => true,
                ),
            );
        }

        /**
         * Output for the order received page.
         */
        public function thankyou_page() {
            if ( $this->instructions )
                echo wpautop( wptexturize( $this->instructions ) );
        }

        /**
         * Add content to the WC emails.
         *
         * @access public
         * @param WC_Order $order
         * @param bool $sent_to_admin
         * @param bool $plain_text
         */
        public function email_instructions( $order, $sent_to_admin, $plain_text = false ) {
            if ( $this->instructions && ! $sent_to_admin && 'custom' === $order->payment_method && $order->has_status( 'on-hold' ) ) {
                echo wpautop( wptexturize( $this->instructions ) ) . PHP_EOL;
            }
        }

        public function payment_fields(){

            if ( $description = $this->get_description() ) {
                echo wpautop( wptexturize( $description ) );
            }

            ?>
            <div id="custom_input">
                <p class="form-row form-row-wide">
                    <label for="email_representative" class=""><?php _e('Email du mandaté', $this->domain); ?></label>
                    <input type="email" class="" name="email_representative" id="email_representative" placeholder="" value="">
                </p>
                
            </div>
            <?php
        }

        /**
         * Process the payment and return the result.
         *
         * @param int $order_id
         * @return array
         */
        public function process_payment( $order_id ) {

            $order = wc_get_order( $order_id );

            $status = 'wc-' === substr( $this->order_status, 0, 3 ) ? substr( $this->order_status, 3 ) : $this->order_status;

			
            // or call the Payment complete
			$order->payment_complete();
			
            // Set order status
            $order->update_status( $status, __( 'Checkout with custom payment. ', $this->domain ) );

			// Reduce stock levels
            //$order->reduce_order_stock();

            // Remove cart
            WC()->cart->empty_cart();

            // Return thankyou redirect
            return array(
                'result'    => 'success',
                'redirect'  => $this->get_return_url( $order )
            );
        }
    }
}

add_filter( 'woocommerce_payment_gateways', 'add_custom_gateway_class' );
function add_custom_gateway_class( $methods ) {
    $methods[] = 'WC_Gateway_Custom'; 
    return $methods;
}

add_action('woocommerce_checkout_process', 'process_custom_payment');
function process_custom_payment(){

    if($_POST['payment_method'] != 'custom')
        return;

    if( !isset($_POST['email_representative']) || empty($_POST['email_representative']) )
        wc_add_notice( __( 'Merci de renseigner l\'adresse email du mandaté', 'custom_payment' ), 'error' );


    /*if( !isset($_POST['transaction']) || empty($_POST['transaction']) )
        wc_add_notice( __( 'Please add your transaction ID', $this->domain ), 'error' );
  */
}

/**
 * Update the order meta with field value
 */
add_action( 'woocommerce_checkout_update_order_meta', 'custom_payment_update_order_meta' );
function custom_payment_update_order_meta( $order_id ) {

    if($_POST['payment_method'] != 'custom')
        return;
	
	$order = new WC_Order($order_id);
	
	update_post_meta( $order_id, 'email_representative', $_POST['email_representative'] );
	update_post_meta( $order->get_id(), 'relance', 'manual' ); //neutralise la relance panier abandonné
	
	$mandataire = $order->get_user();
	$mandataire_name = (!empty($mandataire->display_name)) ? $mandataire->display_name : $mandataire->user_email;
	$subject = 'Votre Pré-état daté a été préparé - attente de paiement';
	$body = 'Bonjour,
	
	La commande de votre pré-état daté a été préparée par '.$mandataire_name.'.
	
	Merci d\'effectuer le paiement pour finaliser cette commande : <a href="'.$order->get_checkout_payment_url().'" target="_blank">'.$order->get_checkout_payment_url().'</a>
	
	Notre équipe reste à votre disposition en cas de besoin.
	
	Cordialement
	
	L’équipe Pré Etat Daté en ligne.
	www.pre-etat-date-en-ligne.com
	contact@pre-etat-date-en-ligne.com
	02 85 52 25 70
	';
	$headers[] = 'From: '.get_bloginfo('name').' <'. get_option( 'admin_email') .'>'."\r\n";
	$headers[] = 'Cc: Pre-etat date en ligne <contact@pre-etat-date-en-ligne.com>';
	wp_mail( $_POST['email_representative'], $subject, $body, $headers);

	$subject = 'Votre Pré-état daté a été préparé - attente de paiement';
	$body = 'Bonjour,
	
	La commande de votre pré-état daté a été envoyée à '.$_POST['email_representative'].'
	
	Notre équipe reste à votre disposition en cas de besoin.
	
	Cordialement
	
	L’équipe Pré Etat Daté en ligne.
	www.pre-etat-date-en-ligne.com
	contact@pre-etat-date-en-ligne.com
	02 85 52 25 70
	';
	$headers[] = 'From: '.get_bloginfo('name').' <'. get_option( 'admin_email') .'>'."\r\n";
	$headers[] = 'Cc: Pre-etat date en ligne <contact@pre-etat-date-en-ligne.com>';
	wp_mail( $mandataire->user_email, $subject, $body, $headers);

}

/**
 * Display field value on the order edit page
 */
add_action( 'woocommerce_admin_order_data_after_billing_address', 'custom_checkout_field_display_admin_order_meta', 10, 1 );
function custom_checkout_field_display_admin_order_meta($order){
    $method = get_post_meta( $order->id, '_payment_method', true );
    if($method != 'custom')
        return;

    $email_representative = get_post_meta( $order->id, 'email_representative', true );
    $transaction = get_post_meta( $order->id, 'transaction', true );

    echo '<p><strong>'.__( 'Email mandaté' ).':</strong> ' . $email_representative . '</p>';
    //echo '<p><strong>'.__( 'Transaction ID').':</strong> ' . $transaction . '</p>';
  }

  add_filter( 'user_has_cap', 'bbloomer_order_pay_without_login', 9999, 3 );
 
function bbloomer_order_pay_without_login( $allcaps, $caps, $args ) {
   if ( isset( $caps[0], $_GET['key'] ) ) {
      if ( $caps[0] == 'pay_for_order' ) {
         $order_id = isset( $args[2] ) ? $args[2] : null;
         $order = wc_get_order( $order_id );
         if ( $order ) {
            $allcaps['pay_for_order'] = true;
         }
      }
   }
   return $allcaps;
}



