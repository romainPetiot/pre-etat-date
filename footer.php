

	</div><!-- div #content -->

	<?php
	$display_menu_partner = false;
	if(is_cart() || is_checkout() || is_page()){
		global $woocommerce;
		$items = $woocommerce->cart->get_cart();
		
		foreach($items as $item => $values) { 
			$id_ped = get_post_meta($values['data']->get_id(), '_id_ped', true);
			if(get_field("footer", $id_ped) == "empty"){ 
				//include('footer-partner.php');
				$display_menu_partner = true;
			}
		} 	
	}
	elseif(get_post_type() == "ped"){
		if(get_field("footer", $post->ID) == "empty"){
			$id_ped = $post->ID;
			$display_menu_partner = true;
		}
	}
	?>
	<?php if ( is_active_sidebar( 'pre-footer' ) ) : ?>

	<div class="wrapper pre-footer">
		<ul>
			<?php dynamic_sidebar( 'pre-footer' ); ?>
		</ul>
	</div><!-- .widget-area -->

	<?php endif; ?>
	<footer id="footer">
		<div class="wrapper">
			<div class="bloc-has-2-parts footer-top">
				<!-- Part 1 : Custom logo-->
				<div class="part-1">
					<!-- Custom Logo -->
					<div class="custom-logo">
						<?php
						if($display_menu_partner):
						?>
						<a href="<?php echo get_home_url() ?>" title="Accueil du site">
							<a href="<?php echo get_the_permalink($id_ped); ?>" title="Accueil du partenaire">
							<?php
							$image = get_field('logo_partner', $id_ped);
							$size = 'logo'; // (thumbnail, medium, large, full or custom size)
							if( $image ) :
								echo wp_get_attachment_image( $image, $size );
							else:
							?>
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/logo-pre-etat-date.svg" alt="Pr&eacute;-&eacute;tat-dat&eacute; : Simple, rapide et en ligne !">
						
							<?php
							endif;?>
							</a>
						</a>
						<?php
						/*else:
						?>
						<a href="<?php echo get_the_permalink($id_ped); ?>" title="Accueil du partenaire">
							<?php
								$image = get_field('logo_partner',$id_ped);
								$size = 'visuel-home'; // (thumbnail, medium, large, full or custom size)
								if( $image ) {
									echo wp_get_attachment_image( $image, $size );
								}
							?>
						</a>
						<?php*/
						endif;
						?>
					</div>
				</div>
				<!-- Part 1 : Custom logo-->
				<!-- Part 2 : CTA-->
				<div class="part-2">
					<!-- Call to action - Footer -->
					<?php
					if(!$display_menu_partner):
					?>
					<!--<nav class="footer-cta">
						<a href="<?php echo get_field("intro", "option"); ?>" ><?php esc_html_e( 'Créer mon pré-état daté', 'ped' ); ?></a>
					</nav>-->
					<?php
					endif;
					?>
				</div><!-- Part 2 : CTA-->
			</div>
			<?php
			if(!$display_menu_partner):
			?>
			<nav class="footer-links">
				<?php
					echo ihag_menu('secondary');
				?>
				<div class="bottom-footer">
					<ul>
						<?php dynamic_sidebar( 'bottom-footer' ); ?>
					</ul>
				</div>
			</nav>
			<?php if ( is_active_sidebar( 'bottom-footer' ) ) : ?>
			<!-- .widget-area -->

			<?php endif; ?>
			<!--<nav class="footer-social-links">
				<?php if(get_field("linkedin", 'option')): ?>
					<a href="<?php the_field("linkedin", 'option');?>" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/linkedin-logo.svg" width="25"></a>
				<?php endif;?>
				<?php if(get_field("facebook", 'option')): ?>
					<a href="<?php the_field("facebook", 'option');?>" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/facebook-logo.svg" width="25"></a>
				<?php endif;?>
				<?php if(get_field("twitter", 'option')): ?>
					<a href="<?php the_field("twitter", 'option');?>" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/twitter-logo.svg" width="25"></a>
				<?php endif;?>
			</nav>-->
			<?php
			endif;
			?>
		</div>
	</footer>
	<div class="copyright grey-background <?php if ( is_front_page() ) : echo 'copyright-frontpage'; endif; ?>">
		©Pré-état daté en ligne 2020
	</div>
</div><!-- div #page -->

<?php wp_footer(); 

if(is_checkout()){

	?>
	<!-- Event snippet for Ajout adresse mail conversion page
	In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
	<!--<script>
		function gtag_report_conversion(url) {
			var callback = function () {
				if (typeof(url) != 'undefined') {
				window.location = url;
				}
		};
		gtag('event', 'achat', {
			'send_to': 'AW-626522450/zfDdCMvshvIBENLy36oC',
			'event_callback': callback
		});
		return false;
		}
	</script>-->
	<?php

}

?>

</body>
</html>
