<?php



$theme = wp_get_theme();
define('THEME_VERSION', $theme->Version);

function number_empty($val){
	return (empty($val))? 0 : $val;
}

include_once("inc/acf.php");
include_once("inc/acf-block.php");
include_once("inc/settings-gutenberg.php");
include_once("inc/widget.php");
include_once("inc/clean.php");
include_once("inc/no-comment.php");
include_once("inc/images.php");
include_once("inc/custom-post-type.php");
include_once("inc/breadcrumb.php");
include_once("inc/menu.php");
include_once("inc/enqueue_scripts.php");
include_once("inc/contact.php");
include_once("PED/ped.php");

include 'vendor/autoload.php';

// Adding excerpt for page
//add_post_type_support( 'page', 'excerpt' );

/**
 * Filter the excerpt length to 20 characters.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
//add_filter( 'excerpt_length', function( $length ) { return 20; } );
add_filter('https_ssl_verify', '__return_false');

add_action( 'admin_menu', 'remove_default_post_type' );
function remove_default_post_type() {
    remove_menu_page( 'edit.php' );
}

function nbTinyURL($url)  {
  $ch = curl_init();
  $timeout = 5;
  curl_setopt($ch,CURLOPT_URL,'http://tinyurl.com/api-create.php?url='.$url);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
  curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
  $data = curl_exec($ch);
  curl_close($ch);
  return $data;
}

add_action( 'template_redirect', 'ihag_single_product_redirect' );
function ihag_single_product_redirect() { 
    if ( is_product() ){
      wp_redirect(home_url(),301);
    }
    if(is_account_page()){
      wp_redirect(home_url(),301);
    }
    if(is_shop()){
      wp_redirect(home_url(),301);
    }
    
    return;
}

add_filter( 'woocommerce_billing_fields', 'ihag_remove_filter_phone', 10, 1 );
function ihag_remove_filter_phone( $address_fields ) {
  $address_fields['billing_phone']['required'] = false;

  $address_fields['billing_address_1']['type'] = 'hidden';
  $address_fields['billing_address_2']['type'] = 'hidden';
  $address_fields['billing_state']['type'] = 'hidden';
  $address_fields['billing_postcode']['type'] = 'hidden';
  $address_fields['billing_city']['type'] = 'hidden';

  $address_fields['billing_first_name']['required'] = false;
  $address_fields['billing_last_name']['required'] = false;
  $address_fields['billing_address_1']['required'] = false;
  $address_fields['billing_address_2']['required'] = false;
  
  $address_fields['billing_state']['required'] = false;
  $address_fields['billing_postcode']['required'] = false;
  $address_fields['billing_city']['required'] = false;

  return $address_fields;
}

function woocommerce_form_field_hidden( $field, $key, $args ){
  $field = '<input type="hidden" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" value="' . esc_attr( $args['default'] ) . '" />';
  return $field;
}
add_filter( 'woocommerce_form_field_hidden', 'woocommerce_form_field_hidden', 10, 3 );


function check_nonce(){


	global $wp_rest_auth_cookie;
	/*
	 * Is cookie authentication being used? (If we get an auth
	 * error, but we're still logged in, another authentication
	 * must have been used.)
	 */
	if ( true !== $wp_rest_auth_cookie && is_user_logged_in() ) {
		return false;
	}
	// Is there a nonce?
	$nonce = null;
	if ( isset( $_REQUEST['_wp_rest_nonce'] ) ) {
		$nonce = $_REQUEST['_wp_rest_nonce'];
	} elseif ( isset( $_SERVER['HTTP_X_WP_NONCE'] ) ) {
		$nonce = $_SERVER['HTTP_X_WP_NONCE'];
	}
	if ( null === $nonce ) {
		// No nonce at all, so act as if it's an unauthenticated request.
		wp_set_current_user( 0 );
		return false;
	}
	// Check the nonce.
	return wp_verify_nonce( $nonce, 'wp_rest' );
}


add_action('wp_head','hook_google_verif');
function hook_google_verif() {
  $output='<meta name="google-site-verification" content="Xe2aPsIZCAZ7dQCMIOJebpiDbaN2e9EN7zUmvoi53Ts" />';
  echo $output;
}