<?php
/*
Template Name: Tutorial Choix Formule (start)
*/
$current_user = wp_get_current_user();
?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php get_header(); ?>

<div id="tutorial-start">

  <div class="start-content wrapper">
    <h1 class="print"><?php the_title() ?></h1>
      <div><?php the_content() ?></div>
      <a href="<?php echo get_the_permalink(get_field("form-autonome", "option")); ?>" class="cta-standard">Formule Autonome</a>
      <a href="<?php echo get_the_permalink(get_field("form-assiste", "option")); ?>" class="cta-standard cta-plan-2">Formule Clé en main</a>
      <?php 
        $info = get_field("help_offer", "option");
        if ($info) { ?>
          <p class="help-text">Vous hésitez ?
            <a href="<?php echo $info; ?>" >Comparez nos offres</a>
          </p>
        <?php
        }
      ?>
      
  </div>
</div><!-- .tutorial-start -->


<?php endwhile; endif; ?>
<?php get_footer(); ?>
