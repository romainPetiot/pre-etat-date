<?php
/*
Template Name: Listing Admin
*/
$roles = array();
if( is_user_logged_in() ) {
	$user = wp_get_current_user();
	$roles = ( array ) $user->roles;
	$role = $roles[0];
} 
if(!in_array('administrator', $roles) && !in_array('shop_manager', $roles) && !in_array('redactor', $roles) && !in_array('senior_redactor', $roles)){
	error_log('admin-listing '.implode($roles));
	exit(wp_redirect( home_url(), 301 ));
}

if(isset($_GET['d'])){
	$order = new IHAG_WC_Order( $_GET['d'] );
	$order->set_referral(''); 
}

?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<style>

	table.listing-ped tr:nth-child(even){
		background-color:white;
	}
</style>
<script>
	setTimeout(function(){
   window.location.reload();
}, 60000);
</script>
<div class="template-recap grey-background">
	<div class="wrapper">
		<section style="display:flex;justify-content:space-between;">
			<h1 class="print">Listing PED</h1>
			<?php if($role != "redactor"): ?>
				<a href="<?php echo get_field('page_admin_ped_create', 'option');?>" class="button" style="height:fit-content;align-self:center;">Créer un nouveau PED</a>
			<?php endif;?>
			<!--<section class="recap-content narrow-wrapper white-background wrapper-defines-height">
				<h1 class="print">Listing PED</h1>
			</section>-->
		</section>
		<table width="100%" class="listing-ped">
			<tr>
				<th>
					Statut
				</th>
				<th>Référant</th>
				<th>
					Client
				</hd>
				<th>
					Numéro
				</hd>
				<th>
					Date
				</th>
				<?php if($role != "redactor"): ?>
				<th>
					Timer
				</th>
				<?php endif;?>
				<th>
					Actions
				</th>
			</tr>

			<?php
				/*$arg = array(
					'numberposts' => -1,
					'post_type'   => 'custom_order_status',
				);
				$tab_status_color = [];
				$tab_status_color[''] = '';
				$custom_order_statuses = get_posts( $arg );
				if ( ! empty( $custom_order_statuses ) ) {
					foreach ( $custom_order_statuses as $post ) {
						$status_slug = get_post_meta( $post->ID, 'status_slug', true );
						$tab_status_color[$status_slug] = get_post_meta( $post->ID, 'color', true );
					}
				}
				$tab_status_color = array(
					''
				);*/

				$args = array(
					'post_type'         => 'shop_order',
					'posts_per_page'    => -1,
					'post_status'    => array('wc-pending','wc-processing', 'wc-proces-to-update', 'wc-proces-to-check', 'wc-proces-wait-costu'),
				);// toutes les commandes sauf 	wc-completed
				$orders = get_posts($args);
				foreach ( $orders as $order ):
					$order = new IHAG_WC_Order($order->ID);
					$data = $order->get_data(); // order data
					$color = '';
					$referant = '';
					$timer = '';
					$number_order_to_check = '';
					$show = true;
					if (!empty($order)) {
						$status = $order->get_writing_status();
						$items = $order->get_items();
						if($role == "redactor" && $status == "to_check"){// les rédacteurs ne visualisent pas les commandes à checker
							$show = false;
						}
						else{
							$order_id = $order->get_id();
							$timer = get_post_meta( $order_id, "_timer", true );
							$number_order_to_check = get_post_meta( $order_id, "_rand_order_to_check", true );
							$referant_id = get_post_meta( $order_id, "_referral_person", true );
							$referant = get_user_by("ID", $referant_id);
							if($referant){
								$referant = $referant->display_name;
							}							
							
							if( ($order->get_writing_status() == 'completed' && $order->get_status() == 'completed') // la commande est finalisée (non afichée)
								|| (get_post_meta( $order_id, '_payment_method', true ) == 'month' && $order->get_writing_status() == 'completed') // la commande est en attente mais paiement au mois cependant la le PED est généré (non-affiché)
								//|| (get_post_meta( $order_id, '_payment_method', true ) == 'systempaystd' && $order->get_status() == 'pending') // la commande est en attente de paiement cb
								){
								$show = false;
							}
							 
							
						}
						
						if($show && !$order->autonome()){
							echo '<tr>
								<td style="color:#ccc;background-color:'.$order->get_color_writing_status().';">'.$order->get_label_writing_status().'</td>
								<td>'.$referant.'</td>
								<td>'.$data['billing']['first_name'].' '.$data['billing']['last_name'].'</td>
								<td>'.$order->ID.'</td>
								<td>'.date_i18n(get_option( 'date_format' ) . ' - ' . get_option( 'time_format' ), $order->get_date_created()->getTimestamp()).'</td>';
							if($role != "redactor"){
								echo '<td>'.$timer.' s</td>';
							}
							

							foreach ( $items as $item ) {
								$product = new WC_Product($item->get_product_id());
								echo '<td><a class="button" href="'.get_field("page_admin_ped", "option").'?product_id='.$item->get_product_id().'&order_id='.$order->ID.'">Traiter</a>';
								if($product->is_downloadable() && sizeof($product->get_files()) > 0 ){//si la commande a un PED (pdf)
									foreach ( $product->get_downloads() as $file_id => $file ) {
										echo ' <a class="button" target="_BLANK" href="'.$file['file'].'">PED</a>';	
									}
								}
								echo '</td>';
							}
							echo '</tr>';
						}
					}
					
				endforeach;
			?>



		</table>
		<p>
			<a href="<?php echo wp_logout_url();?>" class="button">Déconnexion</a>
		</p>
	</div>
	
</div>


<?php endwhile; endif; ?>
<?php get_footer(); ?>
