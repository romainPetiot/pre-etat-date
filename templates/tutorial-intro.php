<?php
/*
Template Name: Tutorial Introduction
*/
$current_user = wp_get_current_user();
?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php get_header(); ?>

<div class="template-tutorial">

  <section class="tutorial-main wrapper">
    <div class="page-title">
      <!-- Documents nécessaires -->
      <h1 class="print"><?php the_title() ?></h1>
    </div><!-- .page-title -->
    <div class="page-content">
      <div class="bloc-doc">
        <img class="doc-picture" src="<?php echo get_stylesheet_directory_uri(); ?>/image/administrable/doc-appel.png" alt="#" height="50" width="50">
        <p class="doc-text">Vous aurez besoin du dernier <b class="red">appel de fonds</b></p>
      </div>
      <!-- Block for Document -->
      <!-- Block for Document -->
      <div class="bloc-doc">
        <img class="doc-picture" src="<?php echo get_stylesheet_directory_uri(); ?>/image/administrable/doc-annexes.png" alt="#" height="50" width="50">
        <p class="doc-text">Vous aurez besoin des dernières <b class="blue">annexes comptables</b></p>
      </div>
      <div class="bloc-doc">
        <img class="doc-picture" src="<?php echo get_stylesheet_directory_uri(); ?>/image/administrable/doc-decompte.png" alt="#" height="50" width="50">
        <p class="doc-text">Vous aurez besoin des derniers <b class="green">décomptes de charges</b></p>
      </div>
      <?php the_field("aside-content"); ?>
      <?php if(!empty(get_field("label_bouton_ped")) && !empty(get_field("lien_ped")) ): ?>
        <a href="<?php echo get_field("lien_ped"); ?>" class="cta-standard"><?php echo get_field("label_bouton_ped"); ?></a>
      <?php endif;?>
      <!--<p class="help-text">Étape suivante : choix de la formule</p>-->
    </div><!-- .page-content -->
  </section><!-- .tutorial-main -->

  <section class="tutorial-aside">
    <div class="tutorial-information">
      <h2><?php the_field("title"); ?></h2>
      <?php the_field("content"); ?>
      
      <?php if(!empty(get_field("label-button")) && !empty(get_field("fichier")) ): ?>
      <a class="cta-secondary" href="<?php the_field("fichier");?>" download><?php the_field("label-button");?></a>
      <?php endif;?>
      
      
    </div>
  </section><!-- .tutorial-aside -->

  <div class="tutorial-grey yellow-background tablet-hidden mobile-hidden"></div>
  <!-- .tutorial-grey | decorative element -->

</div><!-- .template-form -->


<?php endwhile; endif; ?>
<?php get_footer(); ?>
