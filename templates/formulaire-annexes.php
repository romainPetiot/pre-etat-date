<?php
/*
Template Name: Formulaire Annexes
*/
if(current_user_can('administrator')){
	//wp_redirect( home_url(), 301 );
}

global $woocommerce;
if ( is_null( $woocommerce->cart ) ) {
	wc_load_cart();
}
$items = $woocommerce->cart->get_cart();
foreach($items as $item => $values) {
	$product_id = $values['data']->get_id();
}
global $product;
$product = wc_get_product($product_id);
$attr = array();
foreach ( $product->get_attributes() as $attribute ):
    $attribute_data = $attribute->get_data(); // Get the data in an array
	$attr[$attribute_data['name']] = $attribute_data['options'][0];
endforeach;
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="template-form">

	<section class="form-header wrapper">
		<div class="page-title">
			<p class="print h1-like"><?php echo get_field("plan-1-name", "option"); ?></p>
		</div><!-- .page-title -->
		<div class="form-anchor form-step-3">
			<div class="form-anchor-container">
				<a href="<?php get_site_url(); the_field("form-coordonate", "option"); ?>" title="&Eacute;tape 1" class="form-anchor-links form-anchor-link-1"></a>
				<span aria-hidden="true" class="form-anchor-separator form-anchor-separator-1 blue-background"></span>
				<a href="<?php get_site_url(); the_field("form-bilan", "option"); ?>" title="&Eacute;tape 2" class="form-anchor-links form-anchor-link-2"></a>
				<span aria-hidden="true" class="form-anchor-separator form-anchor-separator-2 blue-background"></span>
				<a href="<?php get_site_url(); the_field("form-annexes", "option"); ?>" title="&Eacute;tape 3" class="form-anchor-links form-anchor-link-3"></a>
			</div>
			<p class="small-text print">&Eacute;tape 3</p>
			<h1 class="form-title print">Informations annexes</h1>
		</div><!-- .form-anchor -->
	</section><!-- .form-header -->

	<section class="form-aside grey-background">
		<div class="form-doc">
			<h3>&Eacute;tape 3 : Bilan comptable</h3>
			<div class="form-doc-2-parts gutter">
				<div class="part1">
					<!-- Block for Document -->
					<div class="bloc-doc">
						<img class="doc-picture" src="<?php echo get_stylesheet_directory_uri(); ?>/image/administrable/doc-annexes.png" alt="#" height="50" width="50">
						<p class="doc-text">Vous aurez besoin des <b class="blue">annexes comptables</b></p>
					</div>
					<!-- Block for Document -->
				</div><!--.part-1 -->
				<div class="part-2">
					<p class="help-text">Vous n’avez pas ces documents ? <a href="<?php the_field("help", "option"); ?>">Pas de panique, on vous explique comment faire !</a></p>
				</div><!-- .part-2-->
			</div><!--.bloc-has-2-parts-->
		</div><!-- .wrapper -->
	</section><!-- .form-aside -->

	<section class="createAnnex bloc-form wrapper" id="form4">
		<form name="createAnnex" id="createAnnex" action="#" method="POST">

			<!-- Block for Document -->

			<!-- Block for Document -->

			<div>
				<h2>Charge propriétaire</h2>
				<div class="field-combine">
					<div class="formCont">
						<label for="montantAppelDecompteChargeN2">Montant appel Décompte de charge N-2</label>
						<small>Si vous n'avez pas l'information, saisissez le champs suivant</small>
						<input required data-required-alt="budgetN2" class="required-alt" type="number" step="0.01" name="montantAppelDecompteChargeN2" id="montantAppelDecompteChargeN2"  value="<?php echo (isset($attr['montantAppelDecompteChargeN2']))?$attr['montantAppelDecompteChargeN2']:'';?>">
					</div>
					<div class="formCont">
						<label for="budgetN2">Budget N-2</label>
						<input data-required-alt="montantAppelDecompteChargeN2" class="required-alt" type="number" step="0.01" name="budgetN2" id="budgetN2"  value="<?php echo (isset($attr['budgetN2']))?$attr['budgetN2']:'';?>">
					</div>
				</div>

				<div class="field-combine">
					<div class="formCont">
						<label for="montantDepenseDecompteChargeN2">Montant dépenses Décompte de charge N-2</label>
						<small>Si vous n'avez pas l'information, saisissez le champs suivant</small>
						<input required data-required-alt="depenseN2" class="required-alt" type="number" step="0.01" name="montantDepenseDecompteChargeN2" id="montantDepenseDecompteChargeN2"  value="<?php echo (isset($attr['montantDepenseDecompteChargeN2']))?$attr['montantDepenseDecompteChargeN2']:'';?>">
					</div>
					<div class="formCont">
						<label for="depenseN2">Dépenses N-2</label>
						<input data-required-alt="montantDepenseDecompteChargeN2" class="required-alt" type="number" step="0.01" name="depenseN2" id="depenseN2"  value="<?php echo (isset($attr['depenseN2']))?$attr['depenseN2']:'';?>">
					</div>
				</div>

				<div class="field-combine">
					<div class="formCont">
						<label for="montantAppelDecompteChargeN1">Montant appel Décompte de charge N-1</label>
						<small>Si vous n'avez pas l'information, saisissez le champs suivant</small>
						<input required data-required-alt="budgetN1" class="required-alt" type="number" step="0.01" name="montantAppelDecompteChargeN1" id="montantAppelDecompteChargeN1"  value="<?php echo (isset($attr['montantAppelDecompteChargeN1']))?$attr['montantAppelDecompteChargeN1']:'';?>">
					</div>
					<div class="formCont">
						<label for="budgetN1">Budget N-1</label>
						<input data-required-alt="montantAppelDecompteChargeN1" class="required-alt" type="number" step="0.01" name="budgetN1" id="budgetN1"  value="<?php echo (isset($attr['budgetN1']))?$attr['budgetN1']:'';?>">
					</div>
				</div>

				<div class="field-combine">
					<div class="formCont">
						<label for="montantDepenseDecompteChargeN1">Montant dépenses Décompte de charge N-1</label>
						<small>Si vous n'avez pas l'information, saisissez le champs suivant</small>
						<input required data-required-alt="depenseN1" class="required-alt" type="number" step="0.01" name="montantDepenseDecompteChargeN1" id="montantDepenseDecompteChargeN1"  value="<?php echo (isset($attr['montantDepenseDecompteChargeN1']))?$attr['montantDepenseDecompteChargeN1']:'';?>">
					</div>
					<div class="formCont">
						<label for="depenseN1">Dépenses N-1</label>
						<input data-required-alt="montantDepenseDecompteChargeN1" class="required-alt" type="number" step="0.01" name="depenseN1" id="depenseN1"  value="<?php echo (isset($attr['depenseN1']))?$attr['depenseN1']:'';?>">
					</div>
				</div>
			</div>

			<div>
				<h2>Travaux propriétaire</h2>
				<div class="field-combine">
					<div class="formCont">
						<label for="montantAppelDecompteTravauxN2">Montant appel Décompte travaux N-2</label>
						<small>Si vous n'avez pas l'information, saisissez le champs suivant</small>
						<input required data-required-alt="budgetTravauxN2" class="required-alt" type="number" step="0.01" name="montantAppelDecompteTravauxN2" id="montantAppelDecompteTravauxN2"  value="<?php echo (isset($attr['montantAppelDecompteTravauxN2']))?$attr['montantAppelDecompteTravauxN2']:'';?>">
					</div>
					<div class="formCont">
						<label for="budgetTravauxN2">Budget Travaux N-2</label>
						<input data-required-alt="montantAppelDecompteTravauxN2" class="required-alt" type="number" step="0.01" name="budgetTravauxN2" id="budgetTravauxN2"  value="<?php echo (isset($attr['budgetTravauxN2']))?$attr['budgetTravauxN2']:'';?>">
					</div>
				</div>

				<div class="field-combine">
					<div class="formCont">
						<label for="montantDepenseDecompteTravauxN2">Montant dépenses Décompte travaux N-2</label>
						<small>Si vous n'avez pas l'information, saisissez le champs suivant</small>
						<input required data-required-alt="depenseTravauxN2" class="required-alt" type="number" step="0.01" name="montantDepenseDecompteTravauxN2" id="montantDepenseDecompteTravauxN2"  value="<?php echo (isset($attr['montantDepenseDecompteTravauxN2']))?$attr['montantDepenseDecompteTravauxN2']:'';?>">
					</div>
					<div class="formCont">
						<label for="depenseTravauxN2">Dépenses travaux N-2</label>
						<input data-required-alt="montantDepenseDecompteTravauxN2" class="required-alt" type="number" step="0.01" name="depenseTravauxN2" id="depenseTravauxN2"  value="<?php echo (isset($attr['depenseTravauxN2']))?$attr['depenseTravauxN2']:'';?>">
					</div>
				</div>

				<div class="field-combine">
					<div class="formCont">
						<label for="montantAppelDecompteTravauxN1">Montant appel Décompte travaux N-1</label>
						<small>Si vous n'avez pas l'information, saisissez le champs suivant</small>
						<input required data-required-alt="budgetTravauxN1" class="required-alt" type="number" step="0.01" name="montantAppelDecompteTravauxN1" id="montantAppelDecompteTravauxN1"  value="<?php echo (isset($attr['montantAppelDecompteTravauxN1']))?$attr['montantAppelDecompteTravauxN1']:'';?>">
					</div>
					<div class="formCont">
						<label for="budgetTravauxN1">Budget Travaux N-1</label>
						<input data-required-alt="montantAppelDecompteTravauxN1" class="required-alt" type="number" step="0.01" name="budgetTravauxN1" id="budgetTravauxN1"  value="<?php echo (isset($attr['budgetTravauxN1']))?$attr['budgetTravauxN1']:'';?>">
					</div>
				</div>

				<div class="field-combine">
					<div class="formCont">
						<label for="montantDepenseDecompteTravauxN1">Montant dépenses Décompte travaux N-1</label>
						<small>Si vous n'avez pas l'information, saisissez le champs suivant</small>
						<input required data-required-alt="depenseTravauxN1" class="required-alt" type="number" step="0.01" name="montantDepenseDecompteTravauxN1" id="montantDepenseDecompteTravauxN1"  value="<?php echo (isset($attr['montantDepenseDecompteTravauxN1']))?$attr['montantDepenseDecompteTravauxN1']:'';?>">
					</div>
					<div class="formCont">
						<label for="depenseTravauxN1">Dépenses travaux N-1</label>
						<input data-required-alt="montantDepenseDecompteTravauxN1" class="required-alt" type="number" step="0.01" name="depenseTravauxN1" id="depenseTravauxN1"  value="<?php echo (isset($attr['depenseTravauxN1']))?$attr['depenseTravauxN1']:'';?>">
					</div>
				</div>
			</div>
			<div style="text-align:center;grid-column: 1 / 3;">
				<input type="submit" class="cta-standard" id="createAnnexSubmitBtn" value="Finaliser la demande">
			</div>
		</form>
		
	</section>
	<section class="form-aside grey-background">
		<div class="form-video video-container">
			<?php the_field("video");?>
		</div>
	</section><!-- .form-aside -->

</div><!-- .template-form -->

<!-- Help button + popup -->
<?php get_template_part( 'template-parts/content', 'help' ); ?>

<?php endwhile; endif; ?>
<?php get_footer(); ?>
