<?php
/*
Template Name: Formulaire Bilan
*/
if(current_user_can('administrator')){
	//wp_redirect( home_url(), 301 );
}
global $woocommerce;
global $product;
if ( is_null( $woocommerce->cart ) ) {
	wc_load_cart();
}
$items = $woocommerce->cart->get_cart();
foreach($items as $item => $values) {
	$product_id = $values['data']->get_id();
}
$product = wc_get_product($product_id);
$attr = array();
foreach ( $product->get_attributes() as $attribute ):
    $attribute_data = $attribute->get_data(); // Get the data in an array
	$attr[$attribute_data['name']] = $attribute_data['options'][0];
endforeach;

?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="template-form">

	<section class="form-header wrapper">
	  <div class="page-title">
	  	<p class="print h1-like"><?php the_field("plan-1-name", "option"); ?></p>
	  </div><!-- .page-title -->
		<div class="form-anchor form-step-2">
			<div class="form-anchor-container">
				<a href="<?php get_site_url(); the_field("form-coordonate", "option"); ?>" title="&Eacute;tape 1" class="form-anchor-links form-anchor-link-1"></a>
				<span aria-hidden="true" class="form-anchor-separator form-anchor-separator-1 blue-background"></span>
				<a href="<?php get_site_url(); the_field("form-bilan", "option"); ?>" title="&Eacute;tape 2" class="form-anchor-links form-anchor-link-2"></a>
				<span aria-hidden="true" class="form-anchor-separator form-anchor-separator-2 dark-grey-background"></span>
				<a href="<?php get_site_url(); the_field("form-annexes", "option"); ?>" title="&Eacute;tape 3" class="form-anchor-links form-anchor-link-3 disabled"></a>
			</div>
			<p class="small-text print">&Eacute;tape 2</p>
			<h1 class="print form-title">Bilan comptable</h1>
		</div><!-- .form-anchor -->
	</section><!-- .form-header -->

	<section class="form-aside grey-background">
		<div class="form-doc">
			<h3>&Eacute;tape 2 : Bilan comptable</h3>
			<div class="form-doc-2-parts gutter">
				<div class="part1">
					<!-- Block for Document -->
					<div class="bloc-doc">
						<img class="doc-picture" src="<?php echo get_stylesheet_directory_uri(); ?>/image/administrable/doc-extranet.png" alt="#" height="50" width="50">
						<p class="doc-text">Vous aurez besoin de <b class="green">votre situation comptable</b> (consultable sur l'extranet de votre copropriété)</p>
					</div>
					<!-- Block for Document -->
					<!-- Block for Document -->
					<div class="bloc-doc">
						<img class="doc-picture" src="<?php echo get_stylesheet_directory_uri(); ?>/image/administrable/doc-annexes.png" alt="#" height="50" width="50">
						<p class="doc-text">Vous aurez besoin des <b class="blue">annexes comptables</b></p>
					</div>
					<!-- Block for Document -->
				</div><!--.part-1 -->
				<div class="part-2">
					<p class="help-text">Vous n’avez pas ces documents ? <a href="<?php the_field("help", "option"); ?>">Pas de panique, on vous explique comment faire !</a></p>
				</div><!-- .part-2-->
			</div><!--.bloc-has-2-parts-->
		</div><!-- .wrapper -->
	</section><!-- .form-aside -->
			
	<section class="createBilan bloc-form wrapper" id="form3">
		<form name="createBilan" id="createBilan" action="#" method="POST">


			<div>
				<h2>E - Montant de l'avance de trésorerie</h2>
				<div class="field-combine">
					<div class="formCont">
						<label for="avanceTresoADF">Montant de l'avance de trésorerie SUR ADF</label>
						<small>Si vous n'avez pas l'information, saisissez le champs suivant</small>
						<input required data-required-alt="avanceTresoACP" class="required-alt" type="number" step="0.01" name="avanceTresoADF" id="avanceTresoADF"  value="<?php echo (isset($attr['avanceTresoADF']))?$attr['avanceTresoADF']:'';?>">
					</div>
					<div class="formCont">
						<label for="avanceTresoACP">Montant de l'avance de trésorerie SUR ACP</label>
						<input data-required-alt="avanceTresoADF" class="required-alt" type="number" step="0.01" name="avanceTresoACP" id="avanceTresoACP"  value="<?php echo (isset($attr['avanceTresoACP']))?$attr['avanceTresoACP']:'';?>">
					</div>
				</div>

				<div class="field-combine">
					<div class="formCont">
						<label for="avanceTresoADF18al6">Montant de l'avance de trésorerie (18al6) SUR ADF</label>
						<small>Si vous n'avez pas l'information, saisissez le champs suivant</small>
						<input required data-required-alt="avanceTresoACP18al6" class="required-alt" type="number" step="0.01" name="avanceTresoADF18al6" id="avanceTresoADF18al6"  value="<?php echo (isset($attr['avanceTresoADF18al6']))?$attr['avanceTresoADF18al6']:'';?>">
					</div>

					<div class="formCont">
						<label for="avanceTresoACP18al6">Montant de l'avance de trésorerie (18al6) SUR ACP</label>
						<input data-required-alt="avanceTresoADF18al6" class="required-alt" type="number" step="0.01" name="avanceTresoACP18al6" id="avanceTresoACP18al6"  value="<?php echo (isset($attr['avanceTresoACP18al6']))?$attr['avanceTresoACP18al6']:'';?>">
					</div>
				</div>

			</div>

			<div>
				<h2>F - Montant du fonds ALUR</h2>
				<div class="field-combine">
					<div class="formCont">
						<label for="montantFondsAlurADF">Montant du fonds ALUR SUR ADF</label>
						<small>Si vous n'avez pas l'information, saisissez le champs suivant</small>
						<input required data-required-alt="montantFondsAlurACP" class="required-alt"  type="number" step="0.01" name="montantFondsAlurADF" id="montantFondsAlurADF"  value="<?php echo (isset($attr['montantFondsAlurADF']))?$attr['montantFondsAlurADF']:'';?>">
					</div>

					<div class="formCont">
						<label for="montantFondsAlurACP">Montant du fonds ALUR SUR ACP</label>
						<input data-required-alt="montantFondsAlurADF" class="required-alt"  type="number" step="0.01" name="montantFondsAlurACP" id="montantFondsAlurACP"  value="<?php echo (isset($attr['montantFondsAlurACP']))?$attr['montantFondsAlurACP']:'';?>">
					</div>
				</div>
			</div>

			<div>
				<h2>G - Dettes</h2>
				<div class="field-combine">
					<div class="formCont">
						<label for="dettesCopro">Dettes copropriétaires</label>
						<small>Si vous n'avez pas l'information, saisissez le champs suivant</small>
						<input required data-required-alt="dettesFournisseurs" class="required-alt" type="number" step="0.01" name="dettesCopro" id="dettesCopro"  value="<?php echo (isset($attr['dettesCopro']))?$attr['dettesCopro']:'';?>">
					</div>

					<div class="formCont">
						<label for="dettesFournisseurs">Dettes fournisseurs</label>
						<input data-required-alt="dettesCopro" class="required-alt" type="number" step="0.01" name="dettesFournisseurs" id="dettesFournisseurs"  value="<?php echo (isset($attr['dettesFournisseurs']))?$attr['dettesFournisseurs']:'';?>">
					</div>
				</div>
			</div>
			<div></div>
			<div style="text-align:center;grid-column: 1 / 3;">
				<input type="submit" class="cta-standard" id="createBilanSubmitBtn" value="&Eacute;tape suivante">
			</div>
		</form>
		
	</section>
	<section class="form-aside grey-background">
		<div class="form-video video-container">
		<?php the_field("video");?>
		</div>
	</section><!-- .form-aside -->
</div><!-- .template-form -->

<!-- Help button + popup -->
<?php get_template_part( 'template-parts/content', 'help' ); ?>

<?php endwhile; endif; ?>
<?php get_footer(); ?>
