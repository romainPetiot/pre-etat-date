<?php
/*
Template Name: Formulaire Admin create ped
*/

	
///////////////
$roles = array();
if( is_user_logged_in() ) {
	$user = wp_get_current_user();
	$roles = ( array ) $user->roles;
	$role = $roles[0];
} 

if(!in_array('administrator', $roles) && !in_array('shop_manager', $roles) && !in_array('redactor', $roles) && !in_array('senior_redactor', $roles)){
	exit(wp_redirect( home_url(), 301 ));
}

get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="template-recap grey-background">
	<div class="wrapper" style="max-width: 800px;margin: 0 auto;">
		<form name="formAdminCreatePEDManual" id="formAdminCreatePEDManual" action="#" method="POST">

		<h2>Création manuel d'un PED</h2>

		<div class="formCont">
			<label for="admin-ped-email">E-mail</label>
			<input type="email" name="admin-ped-email" id="admin-ped-email" onkeypress="search_users_with_price(this.value)"  required>
		</div>
		<div class="formCont">
			<label for="ped-price">Tarif</label>
			<input type="number" step="0,01" min="0" placeholder="60 (saisir le tarif TTC)" name="ped-price" id="ped-price"  required>
		</div>
		

		<input class="cta-standard" type="submit" style="margin:0;" id="AdminCreatePEDManualBtn" value="Création du PED">

		</form>
	</div>
</div><!-- .template-form -->
<script>
	var users_with_price = [
<?php
$blogusers = get_users( 
    array('meta_query'=>
         array(
            array(
                'relation' => 'AND',
                array(
                    'key' => 'price_ped',
                    'value' => 60,
                    'compare' => "<",
                    'type' => 'NUMERIC'
                ),
            )
        )
    )
);

foreach ( $blogusers as $user ) {
    echo '{"user" : "' . esc_html( $user->display_name ) .'",';
	echo '"price" : "' . esc_html( get_field('price_ped', 'user_'.$user->ID ) ) .'"}';
}
?>
	];
</script>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
