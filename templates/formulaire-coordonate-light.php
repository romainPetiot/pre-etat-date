<?php
/*
Template Name: Formulaire Coordonate light
*/

global $woocommerce;
global $product;
if ( is_null( $woocommerce->cart ) ) {
	wc_load_cart();
}
$items = $woocommerce->cart->get_cart();
foreach($items as $item => $values) {
	$product_id = $values['data']->get_id();
}

$product = wc_get_product($product_id);
$attr = array();
foreach ( $product->get_attributes() as $attribute ):
    $attribute_data = $attribute->get_data(); // Get the data in an array
	$attr[$attribute_data['name']] = $attribute_data['options'][0];
endforeach;

?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="template-form">

	<section class="form-header wrapper">
		<div class="page-title">
			<p class="print h1-like">Pré-état daté</p>
		</div><!-- .page-title -->
		<div class="form-anchor form-step-1">
			<h1 class="form-title print">Coordonn&eacute;es du bien</h1>
		</div><!-- .form-anchor -->
	</section><!-- .form-header -->

	<section class="form-aside tablet-hidden mobile-hidden">
    <div class="form-picture yellow-background image-container">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/administrable/header-form-email.png" alt=#>
    </div>
  </section><!-- .form-aside -->

	<div class="form-background grey-background tablet-hidden mobile-hidden"></div>
	<!-- .form-background | decorative element -->

	<section class="bloc-form wrapper" id="form2">
	 	<form name="createCoordonateLight" id="createCoordonateLight" action="#" method="POST">

			<h2>A - Adresse du bien</h2>

			<div class="formCont">
				<label for="addressCoordonate">Adresse </label>
				<input type="text" name="addressCoordonate" id="addressCoordonate" placeholder="2 rue Diderot" required limit="40" value="<?php echo (isset($attr['addressCoordonate']))?$attr['addressCoordonate']:'';?>">
			</div>
			<div class="formCont">
				<label for="address2Coordonate">Compl&eacute;ment d'adresse </label>
				<input type="text" name="address2Coordonate" id="address2Coordonate" placeholder="R&eacute;sidence l'Atlantique" limit="40" value="<?php echo (isset($attr['address2Coordonate']))?$attr['address2Coordonate']:'';?>">
			</div>
			<div class="formCont">
				<label for="cpCoordonate">Code postal </label>
				<input type="text" name="cpCoordonate" id="cpCoordonate" placeholder="44000" required  value="<?php echo (isset($attr['cpCoordonate']))?$attr['cpCoordonate']:'';?>">
			</div>
			<div class="formCont">
				<label for="cityCoordonate">Ville </label>
				<input type="text" name="cityCoordonate" id="cityCoordonate" placeholder="Nantes" required  value="<?php echo (isset($attr['cityCoordonate']))?$attr['cityCoordonate']:'';?>">
			</div>

		 	<input type="submit" class="cta-standard" id="createCoordonateSubmitBtn" value="&Eacute;tape suivante">
		</form>
	</section>

</div><!-- .template-form -->

<!-- Help button + popup -->
<?php get_template_part( 'template-parts/content', 'help' ); ?>

<?php endwhile; endif; ?>
<?php get_footer(); ?>
