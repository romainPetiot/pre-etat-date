<?php
/*
Template Name: Formulaire Coordonate
*/

global $woocommerce;
global $product;
if ( is_null( $woocommerce->cart ) ) {
	wc_load_cart();
}
/*
$items = $woocommerce->cart->get_cart();
foreach($items as $item => $values) {
	$product_id = $values['data']->get_id();
}

$product = wc_get_product($product_id);
$attr = array();
foreach ( $product->get_attributes() as $attribute ):
    $attribute_data = $attribute->get_data(); // Get the data in an array
	$attr[$attribute_data['name']] = $attribute_data['options'][0];
endforeach;
*/
global $current_user;
wp_get_current_user();
$product_id = createProduct($current_user->ID, $current_user->user_email, get_field('form-autonome', 'option') );
$woocommerce->cart->empty_cart();
$woocommerce->cart->add_to_cart( $product_id,1 );

?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="template-form">

	<section class="form-header wrapper ">
		<div class="page-title">
			<p class="print h1-like"><?php the_field("plan-1-name", "option"); ?></p>
		</div><!-- .page-title -->
		<div class="form-anchor form-step-1">
			<div class="form-anchor-container">
				<a href="<?php get_site_url(); the_field("form-coordonate", "option"); ?>" title="&Eacute;tape 1" class="form-anchor-links form-anchor-link-1"></a>
				<span aria-hidden="true" class="form-anchor-separator form-anchor-separator-1 dark-grey-background"></span>
				<a href="<?php get_site_url(); the_field("form-bilan", "option"); ?>" title="&Eacute;tape 2" class="form-anchor-links form-anchor-link-2 disabled"></a>
				<span aria-hidden="true" class="form-anchor-separator form-anchor-separator-2 dark-grey-background"></span>
				<a href="<?php get_site_url(); the_field("form-annexes", "option"); ?>" title="&Eacute;tape 3" class="form-anchor-links form-anchor-link-3 disabled"></a>
			</div>
			<p class="small-text print">&Eacute;tape 1</p>
			<h1 class="form-title print">Coordonn&eacute;es du bien</h1>
		</div><!-- .form-anchor -->
	</section><!-- .form-header -->

	<section class="form-aside grey-background">
		<div class="form-doc">
			<h3>&Eacute;tape 1 : Coordonn&eacute;es du bien</h3>
			<div class="form-doc-2-parts gutter">
				<div class="part1">
					<!-- Block for Document -->
					<div class="bloc-doc">
						<img class="doc-picture" src="<?php echo get_stylesheet_directory_uri(); ?>/image/administrable/doc-appel.png" alt="#" height="50" width="50">
						<p class="doc-text">Vous aurez besoin de <b class="red">l’appel de fonds</b></p>
					</div>
					<!-- Block for Document -->
				</div><!--.part-1 -->
				<div class="part-2">
					<p class="help-text">Vous n’avez pas ces documents ? <a href="<?php the_field("help", "option"); ?>">Pas de panique, on vous explique comment faire !</a></p>
				</div><!-- .part-2-->
			</div><!--.bloc-has-2-parts-->
		</div><!-- .wrapper -->
	</section><!-- .form-aside -->

	<div class="form-background grey-background tablet-hidden mobile-hidden"></div>
	<!-- .form-background | decorative element -->

	<section class="coordonateForm bloc-form wrapper" id="form2">
	 	<form name="createCoordonate" id="createCoordonate" action="#" method="POST">

			<div>
				<h2>A - Adresse du bien</h2>
				<div class="formCont">
					<label for="addressCoordonate">Adresse </label>
					<input type="text" name="addressCoordonate" id="addressCoordonate" placeholder="2 rue Diderot" required limit="40" value="<?php echo (isset($attr['addressCoordonate']))?$attr['addressCoordonate']:'';?>">
				</div>
				<div class="formCont">
					<label for="address2Coordonate">Compl&eacute;ment d'adresse </label>
					<input type="text" name="address2Coordonate" id="address2Coordonate" placeholder="R&eacute;sidence l'Atlantique" limit="40" value="<?php echo (isset($attr['address2Coordonate']))?$attr['address2Coordonate']:'';?>">
				</div>
				<div class="formCont">
					<label for="cpCoordonate">Code postal </label>
					<input type="text" name="cpCoordonate" id="cpCoordonate" placeholder="44000" required  value="<?php echo (isset($attr['cpCoordonate']))?$attr['cpCoordonate']:'';?>">
				</div>
				<div class="formCont">
					<label for="cityCoordonate">Ville </label>
					<input type="text" name="cityCoordonate" id="cityCoordonate" placeholder="Nantes" required  value="<?php echo (isset($attr['cityCoordonate']))?$attr['cityCoordonate']:'';?>">
				</div>
			</div>
			<div>
				<h2>B - Propriétaire</h2>

				<div class="formCont">
					<label for="firstnameOwner1">Pr&eacute;nom </label>
					<input type="text" name="firstnameOwner1" id="firstnameOwner1" placeholder="Jean-Pierre" required limit="40" value="<?php echo (isset($attr['firstnameOwner1']))?$attr['firstnameOwner1']:'';?>">
				</div>
				<div class="formCont">
					<label for="lastnameOwner1">Nom </label>
					<input type="text" name="lastnameOwner1" id="lastnameOwner1" placeholder="Dupont" required limit="40" value="<?php echo (isset($attr['lastnameOwner1']))?$attr['lastnameOwner1']:'';?>">
				</div>
				<div class="formCont">
					<label for="addressOwner1">Adresse </label>
					<input type="text" name="addressOwner1" id="addressOwner1" placeholder="10 place de la République" required limit="40" value="<?php echo (isset($attr['addressOwner1']))?$attr['addressOwner1']:'';?>">
				</div>
				<div class="formCont">
					<label for="address2Owner1">Compl&eacute;ment d'adresse </label>
					<input type="text" name="address2Owner1" id="address2Owner1" placeholder="Appartement 108" limit="40" value="<?php echo (isset($attr['address2Owner1']))?$attr['address2Owner1']:'';?>">
				</div>
				<div class="formCont">
					<label for="cpOwner1">Code postal </label>
					<input type="text" name="cpOwner1" id="cpOwner1" placeholder="33000" required  value="<?php echo (isset($attr['cpOwner1']))?$attr['cpOwner1']:'';?>">
				</div>
				<div class="formCont">
					<label for="cityOwner1">Ville </label>
					<input type="text" name="cityOwner1" id="cityOwner1" placeholder="Bordeaux" required  value="<?php echo (isset($attr['cityOwner1']))?$attr['cityOwner1']:'';?>">
				</div>
	
				<div id="ownerContainer">
					<div id="owner2" class="hidden">
						<h3>Propriétaire 2</h3>
						<div class="formCont">
							<label for="firstnameOwner2">Pr&eacute;nom </label>
							<input type="text" name="firstnameOwner2" id="firstnameOwner2" placeholder="Jean-Pierre"  limit="40" value="<?php echo (isset($attr['firstnameOwner2']))?$attr['firstnameOwner2']:'';?>">
						</div>
						<div class="formCont">
							<label for="lastnameOwner2">Nom</label>
							<input type="text" name="lastnameOwner2" id="lastnameOwner2" placeholder="Dupont"  limit="40" value="<?php echo (isset($attr['firstnameOwner1']))?$attr['firstnameOwner1']:'';?>">
						</div>
						
					</div>
					<div id="owner3" class="hidden">
						<h3>Propriétaire 3</h3>
						<div class="formCont">
							<label for="firstnameOwner3">Pr&eacute;nom </label>
							<input type="text" name="firstnameOwner3" id="firstnameOwner3" placeholder="Jean-Pierre"  limit="40" value="<?php echo (isset($attr['firstnameOwner3']))?$attr['firstnameOwner3']:'';?>">
						</div>
						<div class="formCont">
							<label for="lastnameOwner3">Nom</label>
							<input type="text" name="lastnameOwner3" id="lastnameOwner3" placeholder="Dupont"  limit="40" value="<?php echo (isset($attr['firstnameOwner3']))?$attr['firstnameOwner3']:'';?>">
						</div>
					</div>
					<div id="owner4" class="hidden">
						<h3>Propriétaire 4</h3>
						<div class="formCont">
							<label for="firstnameOwner4">Pr&eacute;nom </label>
							<input type="text" name="firstnameOwner4" id="firstnameOwner4" placeholder="Jean-Pierre"  limit="40" value="<?php echo (isset($attr['firstnameOwner4']))?$attr['firstnameOwner4']:'';?>">
						</div>
						<div class="formCont">
							<label for="lastnameOwner4">Nom</label>
							<input type="text" name="lastnameOwner4" id="lastnameOwne4" placeholder="Dupont"  limit="40" value="<?php echo (isset($attr['firstnameOwner4']))?$attr['firstnameOwner4']:'';?>">
						</div>
					</div>
					<div id="owner5" class="hidden">
						<h3>Propriétaire 5</h3>
						<div class="formCont">
							<label for="firstnameOwner5">Pr&eacute;nom</label>
							<input type="text" name="firstnameOwner5" id="firstnameOwner5" placeholder="Jean-Pierre"  limit="40" value="<?php echo (isset($attr['firstnameOwner5']))?$attr['firstnameOwner5']:'';?>">
						</div>
						<div class="formCont">
							<label for="lastnameOwner5">Nom</label>
							<input type="text" name="lastnameOwner5" id="lastnameOwne5" placeholder="Dupont"  limit="40" value="<?php echo (isset($attr['firstnameOwner5']))?$attr['firstnameOwner5']:'';?>">
						</div>
					</div>
					<div id="owner6" class="hidden">
						<h3>Propriétaire 6</h3>
						<div class="formCont">
							<label for="firstnameOwner6">Pr&eacute;nom</label>
							<input type="text" name="firstnameOwner6" id="firstnameOwner6" placeholder="Jean-Pierre"  limit="40" value="<?php echo (isset($attr['firstnameOwner6']))?$attr['firstnameOwner6']:'';?>">
						</div>
						<div class="formCont">
							<label for="lastnameOwner6">Nom</label>
							<input type="text" name="lastnameOwner6" id="lastnameOwne6" placeholder="Dupont"  limit="40" value="<?php echo (isset($attr['firstnameOwner6']))?$attr['firstnameOwner6']:'';?>">
						</div>
					</div>
					<div id="owner7" class="hidden">
						<h3>Propriétaire 7</h3>
						<div class="formCont">
							<label for="firstnameOwner7">Pr&eacute;nom</label>
							<input type="text" name="firstnameOwner7" id="firstnameOwner7" placeholder="Jean-Pierre"  limit="40" value="<?php echo (isset($attr['firstnameOwner7']))?$attr['firstnameOwner7']:'';?>">
						</div>
						<div class="formCont">
							<label for="lastnameOwner7">Nom</label>
							<input type="text" name="lastnameOwner7" id="lastnameOwne7" placeholder="Dupont"  limit="40" value="<?php echo (isset($attr['firstnameOwner7']))?$attr['firstnameOwner7']:'';?>">
						</div>
					</div>
				</div>
	
				<button class="cta-secondary cta-plus" id="moreOwner">Ajouter un propri&eacute;taire</button>
				<button class="cta-secondary cta-minus hidden" id="lessOwner">Retirer un propri&eacute;taire</button>
	
				<!-- Block for Document -->
				<div class="bloc-doc">
					<img class="doc-picture" src="<?php echo get_stylesheet_directory_uri(); ?>/image/administrable/doc-appel.png" alt="#" height="50" width="50">
					<p class="doc-text">Les informations suivantes sont sur <b class="red">l’appel de fonds</b></p>
				</div>
			</div>

			<div>
				<h2>C - Lots vendus</h2>
				<div class="formCont">
					<label for="Tantiemesgeneraux">Tantièmes généraux </label>
					<input type="number" step="1" min="1" name="Tantiemesgeneraux" id="Tantiemesgeneraux" placeholder="1000" required value="<?php echo (isset($attr['Tantiemesgeneraux']))?$attr['Tantiemesgeneraux']:'0';?>">
				</div>
				<div class="formCont2">
					<label for="lotCoordonate">Num&eacute;ro du lot 1</label>
					<input type="text" name="lotCoordonate" id="lotCoordonate" placeholder="112 Appartement T4" required  value="<?php echo (isset($attr['lotCoordonate']))?$attr['lotCoordonate']:'';?>">
				</div>
				<div class="formCont2">
					<label for="tantiemeCoordonate">Tanti&egrave;me lot 1</label>
					<input type="number" step="1" min="0" name="tantiemeCoordonate" id="tantiemeCoordonate" placeholder="161" required value="<?php echo (isset($attr['tantiemeCoordonate']))?$attr['tantiemeCoordonate']:'0';?>">
				</div>

				<div class="formCont2">
					<label for="lotCoordonate2">Num&eacute;ro du lot 2</label>
					<input type="text" name="lotCoordonate2" id="lotCoordonate2" placeholder="112 Appartement T4"   value="<?php echo (isset($attr['lotCoordonate2']))?$attr['lotCoordonate2']:'';?>">
				</div>
				<div class="formCont2">
					<label for="tantiemeCoordonate2">Tanti&egrave;me lot 2</label>
					<input type="number" step="1" min="0" name="tantiemeCoordonate2" id="tantiemeCoordonate2" placeholder="161"  value="<?php echo (isset($attr['tantiemeCoordonate2']))?$attr['tantiemeCoordonate2']:'0';?>">
				</div>

				<div class="formCont2">
					<label for="lotCoordonate3">Num&eacute;ro du lot 3</label>
					<input type="text" name="lotCoordonate3" id="lotCoordonate3" placeholder="112 Appartement T4"   value="<?php echo (isset($attr['lotCoordonate3']))?$attr['lotCoordonate3']:'';?>">
				</div>
				<div class="formCont2">
					<label for="tantiemeCoordonate3">Tanti&egrave;me lot 3</label>
					<input type="number" step="1" min="0" name="tantiemeCoordonate3" id="tantiemeCoordonate3" placeholder="161"  value="<?php echo (isset($attr['tantiemeCoordonate3']))?$attr['tantiemeCoordonate3']:'0';?>">
				</div>
				<div id="moreLot" style="display:none">
					<div class="formCont2">
						<label for="lotCoordonate">Num&eacute;ro du lot 4</label>
						<input type="text" name="lotCoordonate4" id="lotCoordonate4" placeholder="112 Appartement T4"   value="<?php echo (isset($attr['lotCoordonate4']))?$attr['lotCoordonate4']:'';?>">
					</div>
					<div class="formCont2">
						<label for="tantiemeCoordonate4">Tanti&egrave;me lot 4</label>
						<input type="number" step="1" min="0" name="tantiemeCoordonate4" id="tantiemeCoordonate4" placeholder="161"  value="<?php echo (isset($attr['tantiemeCoordonate4']))?$attr['tantiemeCoordonate4']:'0';?>">
					</div>

					<div class="formCont2">
						<label for="lotCoordonate">Num&eacute;ro du lot 5</label>
						<input type="text" name="lotCoordonate5" id="lotCoordonate5" placeholder="112 Appartement T4"   value="<?php echo (isset($attr['lotCoordonate5']))?$attr['lotCoordonate5']:'';?>">
					</div>
					<div class="formCont2">
						<label for="tantiemeCoordonate5">Tanti&egrave;me lot 5</label>
						<input type="number" step="1" min="0" name="tantiemeCoordonate5" id="tantiemeCoordonate5" placeholder="161"  value="<?php echo (isset($attr['tantiemeCoordonate5']))?$attr['tantiemeCoordonate5']:'0';?>">
					</div>

					<div class="formCont2">
						<label for="lotCoordonate6">Num&eacute;ro du lot 6</label>
						<input type="text" name="lotCoordonate6" id="lotCoordonate6" placeholder="112 Appartement T4"   value="<?php echo (isset($attr['lotCoordonate6']))?$attr['lotCoordonate6']:'';?>">
					</div>
					<div class="formCont2">
						<label for="tantiemeCoordonate6">Tanti&egrave;me lot 6</label>
						<input type="number" step="1" min="0" name="tantiemeCoordonate6" id="tantiemeCoordonate6" placeholder="161"  value="<?php echo (isset($attr['tantiemeCoordonate6']))?$attr['tantiemeCoordonate6']:'0';?>">
					</div>

					<div class="formCont2">
						<label for="lotCoordonate7">Num&eacute;ro du lot 7</label>
						<input type="text" name="lotCoordonate7" id="lotCoordonate7" placeholder="112 Appartement T4"   value="<?php echo (isset($attr['lotCoordonate7']))?$attr['lotCoordonate7']:'';?>">
					</div>
					<div class="formCont2">
						<label for="tantiemeCoordonate7">Tanti&egrave;me lot 7</label>
						<input type="number" step="1" min="0" name="tantiemeCoordonate7" id="tantiemeCoordonate7" placeholder="161"  value="<?php echo (isset($attr['tantiemeCoordonate7']))?$attr['tantiemeCoordonate7']:'0';?>">
					</div>
				</div>
				<span class="button cta-secondary" id="moreLotBtn" onclick="document.getElementById('moreLot').style.display = 'inline-block';this.style.display='none'">Ajouter des lots suplémentaires</span>
			</div>

			<div>
				<h2>D - Appel de fonds</h2>
				<div class="formCont">
					<label for="situationDateDuJour">Situation à la date du jour</label>
					<small><em>Mettre un moins si le solde est débiteur</em></small>
					<input type="number" step="0.01" name="situationDateDuJour" id="situationDateDuJour" placeholder="-1500" required value="<?php echo (isset($attr['situationDateDuJour']))?$attr['situationDateDuJour']:'';?>">
					
				</div>
				<div class="formCont">
					<label for="appelFondsOrdinaire">Montant dernier appel de fonds ORDINAIRE</label>
					<input required type="number" step="0.01" min="1" name="appelFondsOrdinaire" id="appelFondsOrdinaire" value="<?php echo (isset($attr['appelFondsOrdinaire']))?$attr['appelFondsOrdinaire']:'';?>">
				</div>
				<div class="formCont">
					<label for="appelFondsAlur">Montant dernier appel de fonds ALUR</label>
					<input ata-required-alt="appelFondsOrdinaire" type="number" step="0.01" min="1" name="appelFondsAlur" id="appelFondsAlur"  value="<?php echo (isset($attr['appelFondsAlur']))?$attr['appelFondsAlur']:'';?>">
				</div>
				
			</div>
			<div style="text-align:center;grid-column: 1 / 3;"><input type="submit" class="cta-standard" id="createCoordonateSubmitBtn" value="&Eacute;tape suivante"></div>
		</form>
		
	</section>
	<section class="form-aside grey-background">
		<div class="form-video video-container">
			<?php the_field("video");?>
		</div>
	</section><!-- .form-aside -->
</div><!-- .template-form -->

<!-- Help button + popup -->
<?php get_template_part( 'template-parts/content', 'help' ); ?>

<?php endwhile; endif; ?>
<?php get_footer(); ?>
