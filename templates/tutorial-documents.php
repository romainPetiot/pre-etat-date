<?php
/*
Template Name: Tutorial Documents
*/
$current_user = wp_get_current_user();
?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php get_header(); ?>

<div class="template-tutorial">

  <section class="tutorial-main wrapper">
    <div class="page-title">
      <!-- Comment obtenir les documents : -->
      <h1 class="print"><?php the_title() ?></h1>
    </div><!-- .page-title -->
    <div class="page-content">
      <?php the_field("aside-content"); ?>
      <a href="<?php the_field("form-email", "option"); ?>" class="cta-standard">J'ai compris, c'est parti !</a>
    </div><!-- .page-content -->
  </section><!-- .tutorial-main -->

  <section class="tutorial-aside">
    <div class="tutorial-information">
      <h2><?php the_field("title"); ?></h2>
      <?php the_field("content"); ?>
      <?php if(!empty(get_field("label-button")) && !empty(get_field("fichier")) ): ?>
      <a class="cta-secondary" href="<?php the_field("fichier");?>" download><?php the_field("label-button");?></a>
      <?php endif;?>
    </div>
  </section><!-- .tutorial-aside -->

  <div class="tutorial-grey grey-background  tablet-hidden mobile-hidden"></div>
  <!-- .tutorial-grey | decorative element -->

</div><!-- .template-form -->

<!-- Testimonials -->
<?php get_template_part('template-parts/bloc', 'testimonial'); ?>
<!-- Testimonials -->

<?php endwhile; endif; ?>
<?php get_footer(); ?>
