<?php
/*
Template Name: Formulaire contact
*/
$current_user = wp_get_current_user();
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="contactFrom template-form">

  <section class="form-header wrapper">
    <div class="page-title">
      <h1 class="print"><?php the_title() ?></h1>
    </div><!-- .page-title -->
  </section><!-- .form-header -->

 
  <section class="bloc-form wrapper" id="contact">
   	<form name="contactForm" id="contactForm" action="#" method="POST">
      <input type="hidden" name="nonceformContact" value="">
      <div class="formCont">
        <label for="firstnameContact">Pr&eacute;nom</label>
        <input type="text" name="firstnameContact" id="firstnameContact" placeholder="Jean-Pierre" required value="">
      </div>
  	 	<div class="formCont">
        <label for="nameContact">Nom</label>
  			<input type="text" name="nameContact" id="nameContact" placeholder="Dupont" required value="">
      </div>
      <div class="formCont">
        <label for="emailContact">Adresse mail</label>
        <input type="email" name="emailContact" id="emailContact" placeholder="nom.prenom@exemple.com" required value="">
      </div>
      <div class="formCont">
        <label for="emailContact">Message</label>
        <textarea type="text" name="messageContact" id="messageContact" placeholder="Rédigez votre message" required value=""></textarea>
      </div>
      <div class="formCont checkbox">
        <input type="checkbox" required name="rgpdCheckbox">
        <label for="rgpdCheckbox">J’accepte que les informations saisies dans ce formulaire soient utilisées, pour permettre de me recontacter <a href=<?php echo get_privacy_policy_url();?>>voir la politique de confidentialité</a>.</label>
      </div>
      <div id="ResponseAnchor">
        <input class="cta-standard" type="submit" id="sendMessage" value="Envoyer le message">
        <div id="ResponseMessage">
          Votre message a été envoyé !
        </div>
      </div>
  	</form>
    <div class="contactInf ">
      <p>Par téléphone au : <strong>02 85 52 25 70 – 07 66 82 90 93</strong> <br>Par mail : <strong>contact@pre-etat-date-en-ligne.com</strong> </p>
    </div>
  </section><!-- .bloc-form -->

</div><!-- .template-form -->

<!-- Testimonials -->
<?php get_template_part('template-parts/bloc', 'testimonial'); ?>
<!-- Testimonials -->

<?php endwhile; endif; ?>
<?php get_footer(); ?>
