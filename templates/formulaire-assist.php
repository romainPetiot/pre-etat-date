<?php
/*
Template Name: Formulaire Assisté
*/


global $woocommerce;
global $product;
if ( is_null( $woocommerce->cart ) ) {
	wc_load_cart(); 
}
/*$items = $woocommerce->cart->get_cart();
foreach($items as $item => $values) {
	$product_id = $values['data']->get_id();
}

$product = new WC_Product($product_id);
$attr = array();
foreach ( $product->get_attributes() as $attribute ):
    $attribute_data = $attribute->get_data(); // Get the data in an array
	$attr[$attribute_data['name']] = $attribute_data['options'][0];
endforeach;*/
global $current_user;
wp_get_current_user();
$product_id = createProduct($current_user->ID, $current_user->user_email, get_field('form-assiste', 'option') );
$woocommerce->cart->empty_cart();
$woocommerce->cart->add_to_cart( $product_id,1 );

?>

<?php get_header(); ?> 
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="template-form assist-form">

<section class="form-header wrapper">
    <div class="page-title">
      <h1 class="print"><?php the_title();?></h1>
    </div><!-- .page-title -->
    <div class="page-content">
		<?php
			/*if( is_representative() ){
				echo "<p>Vous êtes identifié comme mandataire. Vous allez réaliser un pré-état daté pour le compte d'une autre personne.
				<br>À la fin de votre commande, un lien de paiement sera envoyé à cette personne (vous n'avez rien à payer).
				</p>";
			}*/
		?>
      <?php the_content() ?>
    </div><!-- .page-content -->
  </section><!-- .form-header -->

<!--   <section class="form-aside tablet-hidden mobile-hidden">
    <div class="form-picture yellow-background image-container">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/administrable/header-form-email.png" alt=#>
    </div>
  </section>-->


	<div class="form-background grey-background tablet-hidden mobile-hidden"></div>
	<!-- .form-background | decorative element -->

	
		<div class="wrapper">
			Si c'est plus simple pour vous, laissez nous vos identifiants extranet de votre syndic.
			<p><a class="button" id="switchForm" href="#">Identifiants extranet</a></p>

			Sinon, renseignez les éléments ci-dessous :
		</div>
	<section class="formAssist bloc-form wrapper" id="form2">

	 	<form name="formAssist" id="formAssist" action="#" method="POST">

			<div>
				<h2>A - Le dernier appel de fonds</h2>
				<div class="formCont">
					<label for="fileAppelFonds">Fichier appel de fonds*</label>
					<input type="file" name="fileAppelFonds" id="fileAppelFonds" accept="image/*,.pdf" required >
				</div>
			</div>

		 	<div>
				 <h2>B - La dernière convocation</h2>
				 
				<div class="formCont">
					<label for="fileAnnexesComptables">Fichier dernière convocation*</label>
					<input type="file" name="fileAnnexesComptables" id="fileAnnexesComptables" accept="image/*,.pdf" required >
				</div>
			 </div>

			<div>
				<h2>C - Le décompte de charge N-1</h2>
				 <div class="formCont">
					<label for="fileDecompteChargen1">Décompte de charge N-1</label>
					<input type="file" name="fileDecompteChargen1" id="fileDecompteChargen1" accept="image/*,.pdf" >
				</div>
			</div>

			<div>
				<h2>D - Le décompte de charge N-2</h2>
				 <div class="formCont">
					<label for="fileDecompteChargen2">Décompte de charge N-2</label>
					<input type="file" name="fileDecompteChargen2" id="fileDecompteChargen2" accept="image/*,.pdf" >
				</div>
			</div>

			<div>
				<h2>Solde de compte</h2>
				<div class="formCont">
					<label for="soldeJour">Solde de compte à la date du jour<br><em><small>Mettre un moins (signe négatif) si le solde est débiteur</small></em></label>
					<input type="number" step="0.01" name="soldeJour" id="soldeJour" required value="<?php echo (isset($attr['soldeJour']))?$attr['soldeJour']:'';?>">
				</div>
			</div>
			<input type="submit" class="cta-standard" id="formAssistSubmitBtn" value="&Eacute;tape suivante">
		</form>

		<form name="formAssistSyndic" class="hide" id="formAssistSyndic" action="#" method="POST">
			<em><small>Pour ré-afficher le formulaire de dépot de fichier cliquez <a href="#" id="switchForm2">ici</a></small></em>
			<h2>Extranet syndic</h2>
			<div class="formCont">
				<label for="nameSyndic">Nom du syndic</label>
				<input type="text" name="nameSyndic" id="nameSyndic" required value="<?php echo (isset($attr['nameSyndic']))?$attr['nameSyndic']:'';?>">
			</div>

			<div class="formCont">
				<label for="identifiantSyndic">Identifiants</label>
				<input type="text" name="identifiantSyndic" id="identifiantSyndic" required value="<?php echo (isset($attr['identifiantSyndic']))?$attr['identifiantSyndic']:'';?>">
			</div>

			<div class="formCont">
				<label for="passwordSyndic">Mot de passe</label>
				<input type="text" name="passwordSyndic" id="passwordSyndic" required value="<?php echo (isset($attr['passwordSyndic']))?$attr['passwordSyndic']:'';?>">
			</div>

			<input type="submit" class="cta-standard" id="formAssistSyndicSubmitBtn" value="&Eacute;tape suivante">
		</form>
	</section>
	

</div><!-- .template-form -->

<?php endwhile; endif; ?>
<?php get_footer(); ?>
