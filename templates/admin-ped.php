<?php
header('Access-Control-Allow-Origin: *');
/*
Template Name: Formulaire Admin
*/


///////////////
$roles = array();
if( is_user_logged_in() ) {
	$user = wp_get_current_user();
	$roles = ( array ) $user->roles;
	$role = $roles[0];
} 

if(!in_array('administrator', $roles) && !in_array('shop_manager', $roles) && !in_array('redactor', $roles) && !in_array('senior_redactor', $roles)){
	
	exit(wp_redirect( home_url(), 301 ));
}
$order = new IHAG_WC_Order( $_GET['order_id'] );
if(!in_array('administrator', $roles) && $order->get_referral() != get_current_user_id()){
	$args = array(
		'post_type'         => 'shop_order',
		'posts_per_page'    => -1,
		'post_status'    => array('wc-pending','wc-processing'),
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' 		=> '_referral_person',
				'value' 	=> get_current_user_id(),
			),
			array(
				'key' 		=> '_writting_status',
				'value' 	=> 'processing',
			),
		),
	);
	$queryCountOrderReferant = new WP_Query( $args );
	if($queryCountOrderReferant->found_posts > 1){
		wp_die('Vous avez 1 PED en cours de rédaction, vous ne pouvez pas en ouvrir 2 simultanement <a href="'.get_field('page_admin_ped_listing', 'option').'">Retour au listing</a>');
	}
	else{
		if( empty( $order->get_referral() ) ){
			$order->set_referral( get_current_user_id() ); 
			$order->set_referral_time(); 
		}
	}
}

global $product;
$product = wc_get_product($_GET['product_id']);
$attr = array();
foreach ( $product->get_attributes() as $attribute ):
    $attribute_data = $attribute->get_data(); // Get the data in an array
	//$attr[$attribute_data['name']] = $attribute_data['options'][0];
	$attr[$attribute_data['name']] = $attribute_data['value'];
endforeach;
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="template-recap grey-background">
	<div class="wrapper" style="max-width: 800px;margin: 0 auto;">
		<ul id="menu-admin-ped">
			<li><a href="#info-generales" class="menu-admin-ped" data-show-id="info-generales">Info générales</a></li> <span>·</span> 
			<li><a href="#form-ped" class="menu-admin-ped" data-show-id="form-ped">Formulaire PED</a></li><span>·</span> 
			<li><a href="#ped-manual" class="menu-admin-ped" data-show-id="ped-manual">PED manuel (xls)</a></li><span>·</span> 
			<li><a href="#email-client" class="menu-admin-ped" data-show-id="email-client">Message client</a></li><span>·</span> 
			<?php if(in_array('administrator', $roles) || in_array('senior_redactor', $roles)):?>
			<li><a href="#action-admin" class="menu-admin-ped" data-show-id="action-admin">Actions admin</a></li><span>·</span>
			<?php endif;?>
			<a href="<?php echo get_field('page_admin_ped_listing', 'option');?>" class="backToList">Retour au listing</a>
		</ul>
		<div class="global-wrapper">
			<?php
			set_query_var( 'attr', $attr );
			?>
			<div id="info-generales" class="elm-admin-ped">
				<?php get_template_part( 'template-parts/admin/info-generales' );?>
			</div>
			<div id="form-ped" class="elm-admin-ped">
				<?php get_template_part( 'template-parts/admin/form-ped' );?>
			</div>
			<div id="ped-manual" class="elm-admin-ped">
				<?php get_template_part( 'template-parts/admin/ped-manual' );?>
			</div>
			<div id="email-client" class="elm-admin-ped">
				<?php get_template_part( 'template-parts/admin/email-client' );?>
			</div>
			<?php if(in_array('administrator', $roles) || in_array('senior_redactor', $roles)):?>
			<div id="action-admin" class="elm-admin-ped">
				<?php get_template_part( 'template-parts/admin/action-admin' ); ?>
			</div>
			<?php endif;?>
		</div>
		
	</div>
</div><!-- .template-form -->
<?php if(in_array('administrator', $roles) || in_array('senior_redactor', $roles)):?>
	<dialog id="dialog-redacor-actif">
		<h3>Etes-vous toujours présent(e) ?</h3>
		<form method="dialog">
			<menu>
				<button id="btn-clearTimout" value="default">Oui</button>
			</menu>
		</form>
	</dialog>
	<script>
		let Dialog = document.getElementById('dialog-redacor-actif');
		var interval = setInterval(userActifTest, 300000);// test si user actif
		
		function userActifTest(){
			console.log('Start_decoredactor');
			Dialog.showModal();
			myTimeout = setTimeout(decoRedactor, 30000);//30 secondes avant déco
			
			clearInterval(interval);
			interval = setInterval(userActifTest, 300000);//relance pour 5minutes après le click "user actif"
		}

		function decoRedactor(){
			console.log('decoredactor');
			location.href = ped_js.listAdmin+'?d=<?php echo $_GET['order_id'] ;?>';
		}

		Dialog.addEventListener('close', function onClose() {
			clearTimeout(myTimeout);
			console.log('clearTimout');
		} );

		document.querySelectorAll('input').forEach( input => {
			console.log(input.name);
			input.addEventListener('click', function(){
				clearInterval(interval);
				interval = setInterval(userActifTest, 300000);//relance pour 5minutes après action user
			});
		});

	</script>
<?php  endif;?>

<?php endwhile; endif; ?>
<?php get_footer(); ?>
