<?php
/*
Template Name: Formulaire Recap
*/
if(current_user_can('administrator')){
	wp_redirect( home_url(), 301 );
}


global $woocommerce;
global $product;
if ( is_null( $woocommerce->cart ) ) {
	wc_load_cart();
}
$items = $woocommerce->cart->get_cart();
foreach($items as $item => $values) {
	$product_id = $values['data']->get_id();
}
$product = wc_get_product($product_id);
creaPreEtatDate($product_id);
$attr = array();
foreach ( $product->get_attributes() as $attribute ):
    $attribute_data = $attribute->get_data(); // Get the data in an array
	$attr[$attribute_data['name']] = $attribute_data['options'][0];
endforeach;

?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="template-recap grey-background">
	<div class="wrapper">

		<section class="recap-content narrow-wrapper white-background wrapper-defines-height">
			<h1 class="print">R&eacute;capitulatif des informations</h1>
			<div class='page-content'>
				<p class="print">Validez les informations avant de g&eacute;n&eacute;rer le pr&eacute;-&eacute;tat dat&eacute;</p>
				<p class="help-text">Attention, vous ne pourrez plus modifier les informations après la validation.</p>
				<?php the_content() ?>
			</div>
		</section>

		<form class="recap-form narrow-wrapper white-background">

			<h2>Adresse e-mail</h2>
			<div class="recap-text">
				<p><?php echo $current_user->user_email;?></p>
				<a href="<?php get_site_url(); the_field("form-email", "option"); ?>" class="recap-links">Modifier</a>
			</div>

			<h2>Adresse du bien</h2>
			<div class="recap-text">
				<p>
				<?php echo $attr['addressCoordonate'];?><br>
				<?php if(!empty($attr['address2Coordonate'])){
					echo $attr['address2Coordonate'].'<br>';
				}
				?>
				<?php echo $attr['cpCoordonate'];?> <?php echo $attr['cityCoordonate'];?>
				</p>
				<a href="<?php the_field("form-coordonate", "option"); ?>" class="recap-links">Modifier</a>
			</div>

			<h2>Propriétaire</h2>
			<div class="recap-text">
				<p>
				<?php echo $attr['firstnameOwner1'].' '.$attr['lastnameOwner1'];?>
				<?php
				if(!empty($attr['firstnameOwner2']) || !empty($attr['lastnameOwner2'])){
					echo ' Et '.$attr['firstnameOwner2'].' '.$attr['lastnameOwner2'];
				}
				?>
				<br>
				<?php echo $attr['addressOwner1'];?><br>
				<?php if(!empty($attr['address2Owner1'])){
					echo $attr['address2Owner1'].'<br>';
				}
				?>
				<?php echo $attr['cpOwner1'];?>
				<?php echo $attr['cityOwner1'];?>
				</p>
				<a href="<?php the_field("form-coordonate", "option"); ?>" class="recap-links">Modifier</a>
			</div>

			<h2>Lot vendu</h2>
			<div class="recap-text">
				<p>
				Nom et numéro de lot : <strong><?php echo $attr['lotCoordonate'];?> <?php echo $attr['lotCoordonate2'];?> <?php echo $attr['lotCoordonate3'];?> <?php echo $attr['lotCoordonate4'];?> <?php echo $attr['lotCoordonate5'];?> <?php echo $attr['lotCoordonate6'];?> <?php echo $attr['lotCoordonate7'];?></strong><br>
				Base : <strong><?php echo $attr['Tantiemesgeneraux'];?></strong><br>
				Tantième : <strong><?php echo $attr['tantiemeCoordonate']+$attr['tantiemeCoordonate2']+$attr['tantiemeCoordonate3']+$attr['tantiemeCoordonate4']+$attr['tantiemeCoordonate5']+$attr['tantiemeCoordonate6']+$attr['tantiemeCoordonate7'];?></strong>
				</p>
				<a href="<?php the_field("form-coordonate", "option"); ?>" class="recap-links">Modifier</a>
			</div>

			<h2>Appel de fonds</h2>
			<div class="recap-text">
				<p>
					Situation à la date du jour : <strong><?php echo $attr['situationDateDuJour'];?> €</strong><br>
					Montant dernier appel de fonds ORDINAIRE : <strong><?php echo $attr['appelFondsOrdinaire'];?> €</strong><br>
					Montant dernier appel de fonds ALUR : <strong><?php echo number_empty($attr['appelFondsAlur']);?> €</strong><br>
				</p>
				<a href="<?php the_field("form-coordonate", "option"); ?>" class="recap-links">Modifier</a>
			</div>

			<h2>Montant de l'avance de trésorerie</h2>
			<div class="recap-text">
				<p>
					Montant de l'avance de trésorerie SUR ADF : <strong><?php echo number_empty($attr['avanceTresoADF']);?> €</strong><br>
					Montant de l'avance de trésorerie SUR ACP : <strong><?php echo number_empty($attr['avanceTresoACP']);?> €</strong><br>
					Montant de l'avance de trésorerie (18al6) SUR ADF : <strong><?php echo number_empty($attr['avanceTresoADF18al6']);?> €</strong><br>
					Montant de l'avance de trésorerie (18al6) SUR ACP : <strong><?php echo number_empty($attr['avanceTresoACP18al6']);?> €</strong><br>
				</p>
				<a href="<?php the_field("form-bilan", "option"); ?>" class="recap-links">Modifier</a>
			</div>

			<h2>Montant du fonds ALUR</h2>
			<div class="recap-text">
				<p>
					Montant du fonds ALUR SUR ADF : <strong><?php echo number_empty($attr['montantFondsAlurADF']);?> €</strong><br>
					Montant du fonds ALUR SUR ACP : <strong><?php echo number_empty($attr['montantFondsAlurACP']);?> €</strong><br>
				</p>
				<a href="<?php the_field("form-bilan", "option"); ?>" class="recap-links">Modifier</a>
			</div>

			<h2>Dettes</h2>
			<div class="recap-text">
				<p>
					Dettes copropriétaires : <strong><?php echo number_empty($attr['dettesCopro']);?> €</strong><br>
					Dettes fournisseurs : <strong><?php echo number_empty($attr['dettesFournisseurs']);?> €</strong><br>
				</p>
				<a href="<?php the_field("form-bilan", "option"); ?>" class="recap-links">Modifier</a>
			</div>

			<h2>Charge propriétaire</h2>
			<div class="recap-text">
				<p>
				Montant appel Décompte de charge N-2 : <strong><?php echo number_empty($attr['montantAppelDecompteChargeN2']);?> €</strong><br>
				Budget N-2 : <strong><?php echo number_empty($attr['budgetN2']);?> €</strong><br>
				Montant dépenses Décompte de charge N-2 : <strong><?php echo number_empty($attr['montantDepenseDecompteChargeN2']);?> €</strong><br>
				Dépenses N-2 : <strong><?php echo number_empty($attr['depenseN2']);?> €</strong><br>
				Montant appel Décompte de charge N-1 : <strong><?php echo number_empty($attr['montantAppelDecompteChargeN1']);?> €</strong><br>
				Budget N-1 : <strong><?php echo number_empty($attr['budgetN1']);?> €</strong><br>
				Montant dépenses Décompte de charge N-1 : <strong><?php echo number_empty($attr['montantDepenseDecompteChargeN1']);?> €</strong><br>
				Dépenses N-1 : <strong><?php echo number_empty($attr['depenseN1']);?> €</strong><br>
				</p>
				<a href="<?php the_field("form-annexes", "option"); ?>" class="recap-links">Modifier</a>
			</div>

			<h2>Travaux propriétaire</h2>
			<div class="recap-text">
				<p>
				Montant appel Décompte travaux N-2 : <strong><?php echo number_empty($attr['montantAppelDecompteTravauxN2']);?> €</strong><br>
				Budget Travaux N-2 : <strong><?php echo number_empty($attr['budgetTravauxN2']);?> €</strong><br>
				Montant dépenses Décompte de travaux N-2 : <strong><?php echo number_empty($attr['montantDepenseDecompteTravauxN2']);?> €</strong><br>
				Dépenses Travaux N-2 : <strong><?php echo number_empty($attr['depenseTravauxN2']);?> €</strong><br>
				Montant appel Décompte de travaux N-1 : <strong><?php echo number_empty($attr['montantAppelDecompteTravauxN1']);?> €</strong><br>
				Budget Travaux N-1 : <strong><?php echo number_empty($attr['budgetTravauxN1']);?> €</strong><br>
				Montant dépenses Décompte de travaux N-1 : <strong><?php echo number_empty($attr['montantDepenseDecompteTravauxN1']);?> €</strong><br>
				Dépenses Travaux N-1 : <strong><?php echo number_empty($attr["depenseTravauxN1"]);?> €</strong><br>
				</p>
				<a href="<?php the_field("form-annexes", "option"); ?>" class="recap-links">Modifier</a>
			</div>

			

			<a href="<?php echo wc_get_cart_url();?>" class="cta-standard">Valider et payer<a>
		</form>

	</div class="wrapper">
</div><!-- .template-form -->

<?php endwhile; endif; ?>
<?php get_footer(); ?>
