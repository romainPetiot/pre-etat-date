<?php

get_header();
?>

<?php
if ( is_cart() || is_checkout() ) {
// Is Woocommerce cart or Woocommerce checkout
?>

	<div class="template-woocommerce">
		<div id="primary" class="wrapper">
			<?php
			while ( have_posts() ) :
				the_post();
				get_template_part( 'template-parts/content', get_post_type() );
			endwhile; // End of the loop.
			?>
		</div><!-- #primary -->
	</div><!-- .template-woocommerce -->

<?php
} else {
// Is a standard page
?>

	<div class="standard-page">

		<div class="page-title narrow-wrapper">
			<?php if(is_singular()):?>
				<?php the_title( '<h1 class="print">', '</h1>' ); ?>
			<?php elseif(is_archive()):?>
				<h1 class="print"><?php echo single_cat_title( '', false );?></h1>
			<?php endif;?>
		</div>

		<div id="primary" class="narrow-wrapper">
			<?php
			while ( have_posts() ) :
				the_post();
				get_template_part( 'template-parts/content', get_post_type() );
			endwhile; // End of the loop.
			?>
		</div><!-- #primary -->

	</div><!-- .standard-page -->

	<!-- Testimonials -->
	<?php get_template_part('template-parts/bloc', 'testimonial'); ?>
	<!-- Testimonials -->

<?php
} // End of the loop
?>

<?php
get_footer();
