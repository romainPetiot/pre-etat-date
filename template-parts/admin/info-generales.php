
<?php
$order = new IHAG_WC_Order($_GET['order_id']);
?>

<div>
    <div class="extern_inf">
        <h2>Information de commande</h2>
        <p>
        <?php
            echo '<b>Numéro </b>: '.$order->get_ID().'<br>';
            echo '<b>Statut de rédaction </b>: '.$order->get_label_writing_status().'<br>';
            echo '<b>Statut de commande </b>: ';
            if($order->get_status() == 'pending'){echo 'en attente de paiement';}
            else if($order->get_status() == 'processing'){echo 'en cours';}
            else if($order->get_status() == 'completed'){echo 'terminée';}
            else {echo $order->get_status();}
            echo '<br>';

            if($order->get_status() == 'pending'){ 
                echo '<b>Lien de paiement </b>: ';
                echo '<a href="'.$order->get_checkout_payment_url().'" target="_blank">'.$order->get_checkout_payment_url().'</a>';
                echo '<br>';
                if(get_post_meta( $_GET['order_id'], '_payment_method', true ) == 'month'){
                    echo '<b>Paiement au mois </b>: ';
                    echo 'Oui';
                    echo '<br>';
                }
                elseif(get_post_meta( $_GET['order_id'], '_payment_method', true ) == 'custom'){
                    echo '<b>Paiement délégué (en attente) </b>: ';
                    echo 'Oui';
                    echo '<br>';
                }
            }

            

        ?>
        </p>
    </div>
    
    <div class="intern_inf">
        
        <?php
        if(!empty($order->get_message_difficulty() || !empty($order->get_message_difficulty())) ){
            echo '<h2>Problème de rédaction</h2><b>Problématique de rédaction du PED </b> :<br> '.str_replace(array(chr(13)),'<br>',$order->get_message_difficulty()).'<br>';
        }
        ?>
    </div>
</div>
<div>
    <div class="extern_inf">
        <h2>Informations renseignées par le client</h2>
        <?php 
        
        if(!empty($order->get_billing_first_name() || !empty($order->get_billing_last_name())) ){
            echo '<b>Nom </b>: '.$order->get_billing_first_name().' '.$order->get_billing_last_name().'<br>';
        }
    
        if(!empty($order->get_billing_company() || !empty($order->get_billing_company())) ){
            echo '<b>Entreprise </b>: '.$order->get_billing_company().'<br>';
        }
    
        if(!empty($order->get_billing_address_1()) ){
            echo '<b>Adresse </b>: '.$order->get_billing_address_1().'<br>';
        }
        if(!empty($order->get_billing_address_2()) ){
            echo '<b>Adresse 2 </b>: '.$order->get_billing_address_2().'<br>';
        }
        if(!empty($order->get_billing_postcode()) ){
            echo '<b>CP </b>: '.$order->get_billing_postcode().'<br>';
        }
        if(!empty($order->get_billing_postcode()) ){
            echo '<b>Ville </b>: '.$order->get_billing_city().'<br>';
        }
        if(!empty($order->get_billing_email()) ){
            echo '<b>E-mail </b>: '.$order->get_billing_email().'<br>';
        }
        if(!empty($order->get_billing_phone()) ){
            echo '<b>Téléphone </b>: '.$order->get_billing_phone().'<br>';
        }
    
        if(isset($attr['fileAppelFonds']) && !empty($attr['fileAppelFonds'])){
            echo '<p><a href="'.wp_get_attachment_url( $attr['fileAppelFonds']).'" class="button" target="_blank">Fichier appel de fonds</a></p>';
        }
    
        if(isset($attr['fileAnnexesComptables']) && !empty($attr['fileAnnexesComptables'])){
            echo '<p><a href="'.wp_get_attachment_url( $attr['fileAnnexesComptables']).'" class="button" target="_blank">Fichier annexes comptables</a></p>';
        }
    
        if(isset($attr['fileDecompteChargen1']) && !empty($attr['fileDecompteChargen1'])){
            echo '<p><a href="'.wp_get_attachment_url( $attr['fileDecompteChargen1']).'" class="button" target="_blank">Décompte de charge N-1</a></p>';
        }
    
        if(isset($attr['fileDecompteChargen2']) && !empty($attr['fileDecompteChargen2'])){
            echo '<p><a href="'.wp_get_attachment_url( $attr['fileDecompteChargen2']).'" class="button" target="_blank">Décompte de charge N+2</a></p>';
        }
    
        $tab_pdf_gallery = get_post_meta($_GET['product_id'], 'pdf-gallery', true );
        if(($tab_pdf_gallery)){
            foreach($tab_pdf_gallery as $pdf_gallery){
                echo '<p><a href="'.wp_get_attachment_url( $pdf_gallery ).'" class="button" download>'.basename ( get_attached_file( $pdf_gallery ) ).'</a></p>';
            }
        }
    
        if(isset($attr['soldeJour']) && !empty($attr['soldeJour'])){
            echo '<p><b>Solde de compte à la date du jour </b>: '.$attr['soldeJour'].' €</p>';
        }
    
        if(isset($attr['nameSyndic']) && !empty($attr['nameSyndic'])){
            echo '<p><b>Nom du Syndic </b>: '.$attr['nameSyndic'].'</p>';
        }
    
        if(isset($attr['identifiantSyndic']) && !empty($attr['identifiantSyndic'])){
            echo '<p><b>Identifiant syndic </b>: '.$attr['identifiantSyndic'].'</p>';
        }
    
        if(isset($attr['passwordSyndic']) && !empty($attr['passwordSyndic'])){
            echo '<p><b>password syndic </b>: '.$attr['passwordSyndic'].'</p>';
        }
    
    
        if(isset($order->customer_message) && !empty($order->customer_message)){
            echo '<p><b>Commentaire client </b>: '.$order->customer_message.'</p>';
        }
        ?>
    </div>
    
    <div class="intern_inf">
        <h2>Instructions internes</h2>
        <?php echo get_post_meta( $_GET['order_id'], 'adminMessageInstruction', true );?>
    </div>
</div>