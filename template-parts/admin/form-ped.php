<?php
$roles = array();
if( is_user_logged_in() ) {
	$user = wp_get_current_user();
	$roles = ( array ) $user->roles;
	$role = $roles[0];
}
$value_pdf2str = get_values_files($_GET['product_id']);
?>
<form name="formAdmin" id="formAdmin" action="#" method="POST">
    <input type="hidden" name="product_id" id="product_id" required value="<?php echo $_GET['product_id'];?>">
    <input type="hidden" name="order_id" id="order_id" required value="<?php echo $_GET['order_id'];?>">
    <input type="hidden" name="user_id" id="user_id" required value="<?php echo get_current_user_id();?>">
    <?php if(!in_array('administrator', $roles) ):?>
        <a href="<?php echo get_field('page_admin_ped_listing', 'option').'?d='.$_GET['order_id'];?>">Abandon de la rédaction de ce PED</a>
    <?php endif;?>
    
    <div >
        <h2>Situation</h2>
        <div class="formCont">
            <label for="situationDateDuJour">Situation à la date du jour</label>
            <input type="number" step="0.01" name="situationDateDuJour" id="situationDateDuJour" placeholder="-1500" required value="<?php echo (isset($attr['situationDateDuJour']))?$attr['situationDateDuJour']:@$attr['soldeJour'];?>">
            <small>Mettre un moins si le solde est débiteur</small>
        </div>
        <h2>Adresse du bien</h2>
        
        <div class="formCont">
            <label for="addressCoordonate">Adresse du bien</label>
            <input type="text" name="addressCoordonate" onblur="copyValue(this.value, 'addressOwner1');" id="addressCoordonate" required value="<?php echo (isset($attr['addressCoordonate']))?$attr['addressCoordonate']:@$value_pdf2str['copro']['address'];?>">
        </div>

        <div class="formCont">
            <label for="address2Coordonate">Adresse du bien 2</label>
            <input type="text" name="address2Coordonate" onblur="copyValue(this.value, 'address2Owner1');" id="address2Coordonate" value="<?php echo (isset($attr['address2Coordonate']))?$attr['address2Coordonate']:@$value_pdf2str['copro']['address2'];?>">
        </div>

        <div class="formCont">
            <label for="cpCoordonate">Code postal</label>
            <input type="text" name="cpCoordonate" onblur="copyValue(this.value, 'cpOwner1');" id="cpCoordonate" required value="<?php echo (isset($attr['cpCoordonate']))?$attr['cpCoordonate']:@$value_pdf2str['copro']['zip'];?>">
        </div>

        <div class="formCont">
            <label for="cityCoordonate">Ville du bien </label>
            <input type="text" name="cityCoordonate" onblur="copyValue(this.value, 'cityOwner1');" id="cityCoordonate" required value="<?php echo (isset($attr['cityCoordonate']))?$attr['cityCoordonate']:@$value_pdf2str['copro']['city'];?>">
        </div>

        <h2>Propriétaire</h2>

        <div class="formCont">
            <label for="firstnameOwner1">Prénom</label>
            <input type="text" name="firstnameOwner1" id="firstnameOwner1"  value="<?php echo (isset($attr['firstnameOwner1']))?$attr['firstnameOwner1']:'';?>">
        </div>

        <div class="formCont">
            <label for="lastnameOwner1">Nom ou société</label>
            <input type="text" name="lastnameOwner1" id="lastnameOwner1" required value="<?php echo (isset($attr['lastnameOwner1']))?$attr['lastnameOwner1']:@$value_pdf2str['owner']['name'];?>">
        </div>

        <div class="formCont">
            <label for="firstnameOwner2">Prénom 2</label>
            <input type="text" name="firstnameOwner2" id="firstnameOwner2" value="<?php echo (isset($attr['firstnameOwner2']))?$attr['firstnameOwner2']:'';?>">
        </div>

        <div class="formCont">
            <label for="lastnameOwner2">Nom 2</label>
            <input type="text" name="lastnameOwner2" id="lastnameOwner2" value="<?php echo (isset($attr['lastnameOwner2']))?$attr['lastnameOwner2']:'';?>">
        </div>
        <p>
            <span class="button" onclick="document.getElementById('proprios').style.display = 'block'">Renseigner plus de propriétaires</span>
        </p>
        <div id="proprios" style="display:none;">
            <div class="formCont"> 
                <label for="firstnameOwner3">Prénom 3</label>
                <input type="text" name="firstnameOwner3" id="firstnameOwner3" value="<?php echo (isset($attr['firstnameOwner3']))?$attr['firstnameOwner3']:'';?>">
            </div>

            <div class="formCont">
                <label for="lastnameOwner3">Nom 3</label>
                <input type="text" name="lastnameOwner3" id="lastnameOwner3" value="<?php echo (isset($attr['lastnameOwner3']))?$attr['lastnameOwner3']:'';?>">
            </div>

            <div class="formCont">
                <label for="firstnameOwner4">Prénom 4</label>
                <input type="text" name="firstnameOwner4" id="firstnameOwner4" value="<?php echo (isset($attr['firstnameOwner4']))?$attr['firstnameOwner4']:'';?>">
            </div>

            <div class="formCont">
                <label for="lastnameOwner4">Nom 4</label>
                <input type="text" name="lastnameOwner4" id="lastnameOwner4" value="<?php echo (isset($attr['lastnameOwner4']))?$attr['lastnameOwner4']:'';?>">
            </div>

            <div class="formCont">
                <label for="firstnameOwner5">Prénom 5</label>
                <input type="text" name="firstnameOwner5" id="firstnameOwner5" value="<?php echo (isset($attr['firstnameOwner5']))?$attr['firstnameOwner5']:'';?>">
            </div>

            <div class="formCont">
                <label for="lastnameOwner5">Nom 5</label>
                <input type="text" name="lastnameOwner5" id="lastnameOwner5" value="<?php echo (isset($attr['lastnameOwner5']))?$attr['lastnameOwner5']:'';?>">
            </div>

            <div class="formCont">
                <label for="firstnameOwner6">Prénom 6</label>
                <input type="text" name="firstnameOwner6" id="firstnameOwner6" value="<?php echo (isset($attr['firstnameOwner6']))?$attr['firstnameOwner6']:'';?>">
            </div>

            <div class="formCont">
                <label for="lastnameOwner6">Nom 6</label>
                <input type="text" name="lastnameOwner6" id="lastnameOwner6" value="<?php echo (isset($attr['lastnameOwner6']))?$attr['lastnameOwner6']:'';?>">
            </div>

            <div class="formCont">
                <label for="firstnameOwner7">Prénom 7</label>
                <input type="text" name="firstnameOwner7" id="firstnameOwner7" value="<?php echo (isset($attr['firstnameOwner7']))?$attr['firstnameOwner7']:'';?>">
            </div>

            <div class="formCont">
                <label for="lastnameOwner7">Nom 7</label>
                <input type="text" name="lastnameOwner7" id="lastnameOwner7" value="<?php echo (isset($attr['lastnameOwner7']))?$attr['lastnameOwner7']:'';?>">
            </div>

        </div>
        <div class="formCont">
            <label for="addressOwner1">Adresse</label>
            <input type="text" name="addressOwner1" id="addressOwner1" required value="<?php echo (isset($attr['addressOwner1']))?$attr['addressOwner1']:@$value_pdf2str['owner']['address'];?>">
        </div>

        <div class="formCont">
            <label for="address2Owner1">Adresse 2</label>
            <input type="text" name="address2Owner1" id="address2Owner1" value="<?php echo (isset($attr['address2Owner1']))?$attr['address2Owner1']:@$value_pdf2str['owner']['address2'];?>">
        </div>

        <div class="formCont">
            <label for="cpOwner1">Code postal</label>
            <input type="text" name="cpOwner1" id="cpOwner1" required value="<?php echo (isset($attr['cpOwner1']))?$attr['cpOwner1']:@$value_pdf2str['owner']['zip'];?>">
        </div>

        <div class="formCont">
            <label for="cityOwner1">Ville</label>
            <input type="text" name="cityOwner1" id="cityOwner1" required value="<?php echo (isset($attr['cityOwner1']))?$attr['cityOwner1']:@$value_pdf2str['owner']['city'];?>">
        </div>

        <h2>Lot(s) vendu(s)</h2>
        <!--<div class="formCont">
            <label for="baseCoordonate">Base</label>
            <input type="number" step="0.01" min="1" name="baseCoordonate" id="baseCoordonate" required value="<?php echo (isset($attr['baseCoordonate']))?$attr['baseCoordonate']:'';?>">
        </div>-->
        <div class="formCont">
            <label for="Tantiemesgeneraux">Tantièmes généraux</label>
            <input type="number" step="0.01" min="1" name="Tantiemesgeneraux" id="Tantiemesgeneraux" required value="<?php echo (isset($attr['Tantiemesgeneraux']))?$attr['Tantiemesgeneraux']:(int)@$value_pdf2str['tentieme-general'];?>">
        </div>
        <div class="formCont formCont2">
            <label for="lotCoordonate">Lot n°1</label>
            <input type="text" name="lotCoordonate" id="lotCoordonate" required value="<?php echo (isset($attr['lotCoordonate']))?$attr['lotCoordonate']:'';?>">
        </div>
        <div class="formCont formCont2">
            <label for="tantiemeCoordonate">Tantième Lot n°1</label>
            <input type="number" step="0.01" min="0" name="tantiemeCoordonate" id="tantiemeCoordonate" required value="<?php echo (isset($attr['tantiemeCoordonate']))?$attr['tantiemeCoordonate']:'0';?>">
        </div>
        
            <div class="formCont formCont2">
                <label for="lotCoordonate2">Lot n°2</label>
                <input type="text" name="lotCoordonate2" id="lotCoordonate2"  value="<?php echo (isset($attr['lotCoordonate2']))?$attr['lotCoordonate2']:'';?>">
            </div>
            <div class="formCont formCont2">
                <label for="tantiemeCoordonate2">Tantième Lot n°2</label>
                <input type="number" step="0.01" min="0" name="tantiemeCoordonate2" id="tantiemeCoordonate2" required value="<?php echo (isset($attr['tantiemeCoordonate2']))?$attr['tantiemeCoordonate2']:'0';?>">
            </div>

            <div class="formCont formCont2">
                <label for="lotCoordonate3">Lot n°3</label>
                <input type="text" name="lotCoordonate3" id="lotCoordonate3"  value="<?php echo (isset($attr['lotCoordonate3']))?$attr['lotCoordonate3']:'';?>">
            </div>
            <div class="formCont formCont2">
                <label for="tantiemeCoordonate3">Tantième Lot n°3</label>
                <input type="number" step="0.01" min="0" name="tantiemeCoordonate3" id="tantiemeCoordonate3" required value="<?php echo (isset($attr['tantiemeCoordonate3']))?$attr['tantiemeCoordonate3']:'0';?>">
            </div>
            <?php if(isset($attr['tantiemeCoordonate2']) && !empty($attr['tantiemeCoordonate2'])):?>
            <div id="lots">
        <?php else:?>
            <p>
                <span class="button" onclick="document.getElementById('lots').style.display = 'block'">Renseigner plus de lots</span>
            </p>
            <div id="lots" style="display:none;">
        <?php endif;?>
            <div class="formCont formCont2">
                <label for="lotCoordonate4">Lot n°4</label>
                <input type="text" name="lotCoordonate4" id="lotCoordonate4"  value="<?php echo (isset($attr['lotCoordonate4']))?$attr['lotCoordonate4']:'';?>">
            </div>
            <div class="formCont formCont2">
                <label for="tantiemeCoordonate4">Tantième Lot n°4</label>
                <input type="number" step="0.01" min="0" name="tantiemeCoordonate4" id="tantiemeCoordonate4"  value="<?php echo (isset($attr['tantiemeCoordonate4']))?$attr['tantiemeCoordonate4']:'0';?>">
            </div>

            <div class="formCont formCont2">
                <label for="lotCoordonate5">Lot n°5</label>
                <input type="text" name="lotCoordonate5" id="lotCoordonate5"  value="<?php echo (isset($attr['lotCoordonate5']))?$attr['lotCoordonate5']:'';?>">
            </div>
            <div class="formCont formCont2">
                <label for="tantiemeCoordonate5">Tantième Lot n°5</label>
                <input type="number" step="0.01" min="0" name="tantiemeCoordonate5" id="tantiemeCoordonate5"  value="<?php echo (isset($attr['tantiemeCoordonate5']))?$attr['tantiemeCoordonate5']:'0';?>">
            </div>

            <div class="formCont formCont2">
                <label for="lotCoordonate6">Lot n°6</label>
                <input type="text" name="lotCoordonate6" id="lotCoordonate6"  value="<?php echo (isset($attr['lotCoordonate6']))?$attr['lotCoordonate6']:'';?>">
            </div>
            <div class="formCont formCont2">
                <label for="tantiemeCoordonate6">Tantième Lot n°6</label>
                <input type="number" step="0.01" min="0" name="tantiemeCoordonate6" id="tantiemeCoordonate6"  value="<?php echo (isset($attr['tantiemeCoordonate6']))?$attr['tantiemeCoordonate6']:'0';?>">
            </div>

            <div class="formCont formCont2">
                <label for="lotCoordonate7">Lot n°7</label>
                <input type="text" name="lotCoordonate7" id="lotCoordonate7"  value="<?php echo (isset($attr['lotCoordonate7']))?$attr['lotCoordonate7']:'';?>">
            </div>
            <div class="formCont formCont2">
                <label for="tantiemeCoordonate7">Tantième Lot n°7</label>
                <input type="number" step="0.01" min="0" name="tantiemeCoordonate7" id="tantiemeCoordonate7"  value="<?php echo (isset($attr['tantiemeCoordonate7']))?$attr['tantiemeCoordonate7']:'0';?>">
            </div>
        </div>

        <h2>Appel de fonds</h2>
        <div class="formCont">
            <label for="appelFondsOrdinaire">Montant dernier appel de fonds ORDINAIRE</label>
            <input type="number" step="0.01" min="0" name="appelFondsOrdinaire" id="appelFondsOrdinaire"  value="<?php echo (isset($attr['appelFondsOrdinaire']))?$attr['appelFondsOrdinaire']:(float)@$value_pdf2str['appel_copro'][0];?>">
        </div>

        <div class="formCont">
            <label for="appelFondsAlur">Montant dernier appel de fonds ALUR</label>
            <input type="number" step="0.01" onchange="coherence()" min="0" name="appelFondsAlur" id="appelFondsAlur" required value="<?php echo (isset($attr['appelFondsAlur']))?$attr['appelFondsAlur']:(float)@$value_pdf2str['appel_copro'][1];?>">
        </div>
        
        <h2>Comptabilité copropriété</h2>
        <div class="formCont">
            <label for="avanceTresoADF">Montant de l'avance de trésorerie SUR ADF</label>
            <input type="number" step="0.01" name="avanceTresoADF" id="avanceTresoADF"  value="<?php echo (isset($attr['avanceTresoADF']))?$attr['avanceTresoADF']:(float)@$value_pdf2str['avances'];?>">
        </div>
        <div class="formCont">
            <label for="avanceTresoACP">Montant de l'avance de trésorerie SUR ACP</label>
            <input type="number" step="0.01" name="avanceTresoACP" id="avanceTresoACP"  value="<?php echo (isset($attr['avanceTresoACP']))?$attr['avanceTresoACP']:0;?>">
        </div>

        <div class="formCont">
            <label for="avanceTresoADF18al6">Montant de l'avance de trésorerie (18al6) SUR ADF</label>
            <input type="number" step="0.01" name="avanceTresoADF18al6" id="avanceTresoADF18al6"  value="<?php echo (isset($attr['avanceTresoADF18al6']))?$attr['avanceTresoADF18al6']:(float)@$value_pdf2str['avancesautres'];?>">
        </div>

        <div class="formCont">
            <label for="avanceTresoACP18al6">Montant de l'avance de trésorerie (18al6) SUR ACP</label>
            <input type="number" step="0.01" name="avanceTresoACP18al6" id="avanceTresoACP18al6"  value="<?php echo (isset($attr['avanceTresoACP18al6']))?$attr['avanceTresoACP18al6']:0;?>">
        </div>

        <div class="formCont">
            <label for="montantFondsAlurADF">Montant du fonds ALUR SUR ADF</label>
            <input type="number" step="0.01" onchange="coherence()" name="montantFondsAlurADF" id="montantFondsAlurADF"  value="<?php echo (isset($attr['montantFondsAlurADF']))?$attr['montantFondsAlurADF']:0;?>">
        </div>

        <div class="formCont">
            <label for="montantFondsAlurACP">Montant du fonds ALUR SUR ACP</label>
            <input type="number" step="0.01" onchange="coherence()" name="montantFondsAlurACP" id="montantFondsAlurACP"  value="<?php echo (isset($attr['montantFondsAlurACP']))?$attr['montantFondsAlurACP']:0;?>">
        </div>

        <div class="formCont">
            <label for="dettesCopro">Dettes copropriétaires</label>
            <input type="number" step="0.01" onchange="coherence()" name="dettesCopro" id="dettesCopro"  value="<?php echo (isset($attr['dettesCopro']))?$attr['dettesCopro']:0;?>">
        </div>

        <div class="formCont">
            <label for="dettesFournisseurs">Dettes fournisseurs</label>
            <input type="number" step="0.01" onchange="coherence()" name="dettesFournisseurs" id="dettesFournisseurs"  value="<?php echo (isset($attr['dettesFournisseurs']))?$attr['dettesFournisseurs']:0;?>">
        </div>

        
        
        <h2>Comptabilité propriétaire</h2>
        <div class="formCont">
            <label for="montantAppelDecompteChargeN2">Montant appel Décompte de charge N-2</label>
            <input type="number" onchange="coherence()" step="0.01" name="montantAppelDecompteChargeN2" id="montantAppelDecompteChargeN2"  value="<?php echo (isset($attr['montantAppelDecompteChargeN2']))?$attr['montantAppelDecompteChargeN2']:(float)@$value_pdf2str['deduction'];?>">
        </div>
        <div class="formCont">
            <label for="budgetN2">Budget N-2</label>
            <input type="number" onchange="coherence()" step="0.01" name="budgetN2" id="budgetN2"  value="<?php echo (isset($attr['budgetN2']))?$attr['budgetN2']:0;?>">
        </div>

        <div class="formCont">
            <label for="montantDepenseDecompteChargeN2">Montant dépenses Décompte de charge N-2</label>
            <input type="number" onchange="coherence()" step="0.01" name="montantDepenseDecompteChargeN2" id="montantDepenseDecompteChargeN2"  value="<?php echo (isset($attr['montantDepenseDecompteChargeN2']))?$attr['montantDepenseDecompteChargeN2']:(float)@$value_pdf2str['totalcopro'];?>">
        </div>
        <div class="formCont">
            <label for="depenseN2">Dépenses N-2</label>
            <input type="number" onchange="coherence()" step="0.01" name="depenseN2" id="depenseN2"  value="<?php echo (isset($attr['depenseN2']))?$attr['depenseN2']:0;?>">
        </div>

        <div class="formCont">
            <label for="montantAppelDecompteChargeN1">Montant appel Décompte de charge N-1</label>
            <input type="number" onchange="coherence()" step="0.01" name="montantAppelDecompteChargeN1" id="montantAppelDecompteChargeN1"  value="<?php echo (isset($attr['montantAppelDecompteChargeN1']))?$attr['montantAppelDecompteChargeN1']:0;?>">
        </div>
        <div class="formCont">
            <label for="budgetN1">Budget N-1</label>
            <input type="number" onchange="coherence()" step="0.01" name="budgetN1" id="budgetN1"  value="<?php echo (isset($attr['budgetN1']))?$attr['budgetN1']:0;?>">
        </div>

        <div class="formCont">
            <label for="montantDepenseDecompteChargeN1">Montant dépenses Décompte de charge N-1</label>
            <input type="number" onchange="coherence()" step="0.01" name="montantDepenseDecompteChargeN1" id="montantDepenseDecompteChargeN1"  value="<?php echo (isset($attr['montantDepenseDecompteChargeN1']))?$attr['montantDepenseDecompteChargeN1']:0;?>">
        </div>
        <div class="formCont">
            <label for="depenseN1">Dépenses N-1</label>
            <input type="number" onchange="coherence()" step="0.01" name="depenseN1" id="depenseN1"  value="<?php echo (isset($attr['depenseN1']))?$attr['depenseN1']:0;?>">
        </div>
        
        <div class="formCont">
            <label for="montantAppelDecompteTravauxN2">Montant appel Décompte travaux N-2</label>
            <input type="number" step="0.01" name="montantAppelDecompteTravauxN2" id="montantAppelDecompteTravauxN2"  value="<?php echo (isset($attr['montantAppelDecompteTravauxN2']))?$attr['montantAppelDecompteTravauxN2']:0;?>">
        </div>
        <div class="formCont">
            <label for="budgetTravauxN2">Budget Travaux N-2</label>
            <input type="number" step="0.01" name="budgetTravauxN2" id="budgetTravauxN2"  value="<?php echo (isset($attr['budgetTravauxN2']))?$attr['budgetTravauxN2']:0;?>">
        </div>

        <div class="formCont">
            <label for="montantDepenseDecompteTravauxN2">Montant dépenses Décompte travaux N-2</label>
            <input type="number" step="0.01" name="montantDepenseDecompteTravauxN2" id="montantDepenseDecompteTravauxN2"  value="<?php echo (isset($attr['montantDepenseDecompteTravauxN2']))?$attr['montantDepenseDecompteTravauxN2']:0;?>">
        </div>
        <div class="formCont">
            <label for="depenseTravauxN2">Dépenses travaux N-2</label>
            <input type="number" step="0.01" name="depenseTravauxN2" id="depenseTravauxN2"  value="<?php echo (isset($attr['depenseTravauxN2']))?$attr['depenseTravauxN2']:0;?>">
        </div>

        <div class="formCont">
            <label for="montantAppelDecompteTravauxN1">Montant appel Décompte travaux N-1</label>
            <input type="number" step="0.01" name="montantAppelDecompteTravauxN1" id="montantAppelDecompteTravauxN1"  value="<?php echo (isset($attr['montantAppelDecompteTravauxN1']))?$attr['montantAppelDecompteTravauxN1']:0;?>">
        </div>
        <div class="formCont">
            <label for="budgetTravauxN1">Budget Travaux N-1</label>
            <input type="number" step="0.01" name="budgetTravauxN1" id="budgetTravauxN1"  value="<?php echo (isset($attr['budgetTravauxN1']))?$attr['budgetTravauxN1']:0;?>">
        </div>

        <div class="formCont">
            <label for="montantDepenseDecompteTravauxN1">Montant dépenses Décompte travaux N-1</label>
            <input type="number" step="0.01" name="montantDepenseDecompteTravauxN1" id="montantDepenseDecompteTravauxN1"  value="<?php echo (isset($attr['montantDepenseDecompteTravauxN1']))?$attr['montantDepenseDecompteTravauxN1']:0;?>">
        </div>
        <div class="formCont">
            <label for="depenseTravauxN1">Dépenses travaux N-1</label>
            <input type="number" step="0.01" name="depenseTravauxN1" id="depenseTravauxN1"  value="<?php echo (isset($attr['depenseTravauxN1']))?$attr['depenseTravauxN1']:0;?>">
        </div>

        
        <div class="formCont">
        <br><br><br>
            <select name="difficulty_level" id="difficulty_level" >
                <option selected value="0" style="color:green">C'était facile</option>
                <option value="1">Je ne suis pas sure de moi</option>
                <option value="2">J'ai besoin d'aide</option>
            </select><br><br>
        </div>
        <div class="formCont" id="contAdminDifficultyMessage">
            <label for="depenseTravauxN1">Problématique du PED</label>
                <textarea id="difficulty_message" name="difficulty_message" placeholder="Message"><?php echo (isset($attr['difficulty_message']))?$attr['difficulty_message']:'';?></textarea>
            </div>
        
        <input class="cta-standard" type="submit" id="formAdminSubmitBtnVisualialiser" value="Générer & visualiser PED">
        <input class="cta-standard" type="submit" id="formAdminSubmitBtn" value="Confirmer la commande PED">

        <div class="formCont">
            <input type="hidden" value="true" id="check" name="check">
            <table>
                <tr>
                    <td>Différence budgets et dépenses N-1 (QP appellée)</td>
                    <td id="diffBugetN1Ask"></td>
                </tr>
                <tr>
                    <td>Différence budgets et dépenses N-1 (QP réelle)</td>
                    <td id="diffBugetN1Real"></td>
                </tr>
                <tr>
                    <td>Différence budgets et dépenses N-2 (QP appellée)</td>
                    <td id="diffBugetN2Ask"></td>
                </tr>
                <tr>
                    <td>Différence budgets et dépenses N-2 (QP réelle)</td>
                    <td id="diffBugetN2Real"></td>
                </tr>
                <tr>
                    <td>Impayés de charges copropriété</td>
                    <td id="AlertDettesCopro"></td>
                </tr>
                <tr>
                    <td>Dettes fournisseurs</td>
                    <td id="AlertDettesFournisseur"></td>
                </tr>
                <tr>
                    <td>Fonds travaux</td>
                    <td id="AlertFondsTravaux"></td>
                </tr>
                <tr>
                    <td>Avance de treso</td>
                    <td id="AlertAvanceTreso"></td>
                </tr>
            </table>
        </div>
    </div>
</form>