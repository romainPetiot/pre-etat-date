<div class="adminAction-grid">
    <fieldset>
        <legend>Valider le PED</legend>
        <form name="formAdminConfirm" id="formAdminConfirm" action="#" method="POST">
            <input type="hidden" name="product_id" id="product_id" required value="<?php echo $_GET['product_id'];?>">
            <input type="hidden" name="order_id" id="order_id" required value="<?php echo $_GET['order_id'];?>">
            <input type="hidden" name="action" id="formAdminConfirmAction" value="">
            
            <input type="submit" class="button validationBtnPED" value="PED à Corriger" id="formAdminCheckSubmitBtn"  onclick="document.getElementById('formAdminConfirmAction').value = 'to_check';">
            <input type="submit" class="button validationBtnPED" value="Mettre statut en cours" id="formAdminProcessingSubmitBtn"  onclick="document.getElementById('formAdminConfirmAction').value = 'processing';">
            <?php
            $order = new IHAG_WC_Order($_GET['order_id']);
            $items = $order->get_items();
            foreach ( $items as $item ) {
                $product = new WC_Product($item->get_product_id());
                if($product->is_downloadable() && sizeof($product->get_files()) > 0): ?>
                    <input type="submit" class="button validationBtnPED" value="Valider ce PED" id="formAdminCompletedSubmitBtn" onclick="document.getElementById('formAdminConfirmAction').value = 'completed';">
                    <?php if($order->get_writing_status() == 'completed'):?>
                        <input type="submit" class="button validationBtnPED" value="Forcer l'envoie du PED" id="formAdminForceCompletedSubmitBtn"  onclick="document.getElementById('formAdminConfirmAction').value = 'force_completed';">
                    <?php endif; ?>
                <?php endif;
            }
            ?>
        </form>
    </fieldset>
    
    <fieldset>
        <legend>Ajouter fichier</legend>
        <form name="formAdminAddFile" id="formAdminAddFile" action="#" method="POST">
            <input type="hidden" name="product_id" id="product_id" required value="<?php echo $_GET['product_id'];?>">
            <input type="hidden" name="order_id" id="order_id" required value="<?php echo $_GET['order_id'];?>">
        
            <?php
            $tab_pdf_gallery = get_post_meta($_GET['product_id'], 'pdf-gallery', true );
            if(($tab_pdf_gallery)){
                foreach($tab_pdf_gallery as $pdf_gallery){
                    echo '<input type="checkbox" name="delete-pdf-gallery-'.$pdf_gallery.'" value="delete" style="display:inline;"> supprimer '.basename ( get_attached_file( $pdf_gallery ) ).'<br>';
                }
            }
            ?>
        
            <input type="file" name="pdf-gallery[]" id="pdf-gallery" multiple>
            <input type="submit" class="button validationBtnPED" value="Envoyer" id="formAdminCorrectSubmitBtn" >
            <p><small><em>L'ajout de fichier modifie le statut de rédaction à "en cours"</em></small></p>
        </form>
    </fieldset>
    
    <fieldset>
        <legend>Attribuer le PED</legend>
        <form name="formAdminRedactor" id="formAdminRedactor" action="#" method="POST">
            <input type="hidden" name="order_id" id="order_id" required value="<?php echo $_GET['order_id'];?>">
            <select name="ped-redactor" id="ped-redactor">
                <option></option>
                <?php
                $users = get_users(array( 'role__in' => array( 'redactor', 'administrator', 'senior_redactor' ) ));
                foreach($users as $redactor){
                    echo '<option value="'.$redactor->ID.'" '.selected( get_post_meta($_GET['order_id'], '_referral_person', true), $redactor->ID).'>'.$redactor->display_name.'</option>';
                }
                ?>
            </select>
            <input type="submit" class="button validationBtnPED" value="Valider" id="formAdminRedactor_btn">
        </form>
    </fieldset>
    
    <div>
        <h2>Instructions internes</h2>
        <form name="formAdminInstruction" id="formAdminInstruction" action="#" method="POST">
            <input type="hidden" name="product_id" id="product_id" required value="<?php echo $_GET['product_id'];?>">
            <input type="hidden" name="order_id" id="order_id" required value="<?php echo $_GET['order_id'];?>">
            <textarea id="adminMessageInstruction" name="adminMessageInstruction"><?php echo str_replace('<br />',"",get_post_meta( $_GET['order_id'], 'adminMessageInstruction', true ));?></textarea>
            <input type="submit" class="button validationBtnPED" value="Envoyer" id="formAdminInstruction">
        </form>
    </div>
    
        
</div>