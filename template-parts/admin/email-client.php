<div>
    <h2>
        E-mail client
    </h2>
    <form name="formAdminAsk" id="formAdminAsk" action="#" method="POST">
        <input type="hidden" name="product_id" id="product_id" required value="<?php echo $_GET['product_id'];?>">
        <input type="hidden" name="order_id" id="order_id" required value="<?php echo $_GET['order_id'];?>">
        <?php 
        if( have_rows('ask_mails', 'option') ):
            while( have_rows('ask_mails', 'option') ) : the_row();
                $data_message = str_replace(array('<p>','</p>','<br />'),"",get_sub_field('content_mail'));
                $data_message = str_replace('"',"'",$data_message);

                ?>
                    <button class="button formAdminAskBtn propal_admin" data-title="<?php echo get_bloginfo('name').' - '; the_sub_field('mail-title');?> | <?php echo $_GET['order_id'];?>" data-message="<?php echo $data_message ;?>">
                        <?php the_sub_field('btn');?>
                    </button>
                <?php
            endwhile;
        endif;
        ?>
        <p><label for="email-title">Object du mail : </label><input type="text" id="email-title" name="email-title" required></p>
        <p><label for="email-body">Message du mail : </label><textarea id="email-body" name="email-body" required></textarea></p>
        <input type="submit" class="button" id="formAdminAskSubmitBtn" value="Envoyer">
    </form>
</div>

<div>
    <h2>
        Message dans l'email final
    </h2>
    <form name="formAdminMessageOrder" id="formAdminMessageOrder" action="#" method="POST">
        <input type="hidden" name="order_id" id="order_id" required value="<?php echo $_GET['order_id'];?>">
        <textarea id="adminMessageOrder" name="adminMessageOrder"><?php echo str_replace('<br />',"",get_post_meta( $_GET['order_id'], 'message_ped', true ));?></textarea>
        <input type="submit" class="button" value="Enregistrer"><br>
        <small><em>Le message sera joint à l'email qui est envoyé avec le PED</em></small>
    </form>
</div>