<?php 
 
$showTestimonials = get_field('testimonial_enabled');
if ( $showTestimonials ) {

?>

<section id="bloc-testimonial">
	<div class="wrapper">

		<h2 class="h1-like" id="testimonial-title">Témoignages clients</h2>

		<div id="testimonial-layout">

		<?php
		$args =  array(
			'posts_per_page' 	=> 6,
			'post_type'   		=> 'testimonial',
			'post_status'    	=> 'publish',
			'orderby'        	=> 'rand'
		);
		
		$testimonials = get_posts($args); 
		if ( $testimonials ) {
			foreach ( $testimonials as $post ) : 
				setup_postdata( $post );
				?>
				<article class="testimonial">
					<div class="cover"></div>
					<button class="read_more">Lire plus</button>
					<h3><?php the_title(); ?></h3>
					<date><?php echo get_the_date();?></date>
					
					<p><?php echo strip_tags(get_the_content()); ?></p>
				</article>
				
				<?php
			endforeach; 
			wp_reset_postdata();
			}
		?>
		</div><!-- #testimonial_layout -->

	</div>
</section>

<?php
}
?>