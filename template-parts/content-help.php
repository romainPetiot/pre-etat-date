<aside id="help-container">
	<!-- Popup : Help box -->
	<div id="popup">

		<!-- Pop-up -->
		<div id="popup-layout">
			<!-- Close button -->
			<button id="closePopup" onclick="togglePopup()">
				<label>Fermer</label>	
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/close.svg" alt="" aria-hidden="true">
			</button>

			<!-- Main content -->
			<div id="popup-content" class="white-background">
				<p class="h1-like"><?php the_field("popup-title", "option") ?></p>
				<div><?php the_field("popup-content", "option") ?></div>
				<a href="<?php echo get_the_permalink(get_field("form-assiste", "option")); ?>" class="cta-standard cta-plan-2"><?php the_field("popup-label", "option"); ?></a>
			</div>
		</div>

		<!-- Background gets darker -->
		<div id="overlay"></div>
	</div>
	
	<!-- Button opens Popup -->
	<button id="btn-help-container" onclick="togglePopup()" >Besoin d'aide ?</button>
	
</aside>