<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="template-form">

	<section class="form-header wrapper">
		<div class="page-title">
			<h1 class="print"><?php the_title();?></h1>
            <?php the_field("content");?>
		</div><!-- .page-title -->
		<div class="page-content"> 
			
		</div><!-- .page-content -->
	</section><!-- .form-header -->
	<div class="form-background tablet-hidden mobile-hidden"></div>
	<!-- .form-background | decorative element -->

	<section class="bloc-form wrapper" id="form1">
	 	<form name="createUser" id="createUser" action="#" method="POST">
			<?php
			$current_user = wp_get_current_user();
			if(current_user_can('administrator')):
				echo 'Les administrateurs ne peuvent pas créer de Pré-état daté';
			else:
			?>
                <div class="formCont">
                    <label for="email">Adresse mail</label>
                    <!--<input type="hidden" name="id_ped" id="id_ped" value="<?php echo $post->ID;?>">-->
                    <input type="hidden" name="type_ped" id="type_ped" value="<?php the_field('type_ped');?>">
                    <input type="email" name="email" id="email" placeholder="nom.prenom@exemple.com" required value="<?php echo $current_user->user_email;?>">
                    <i>Votre adresse mail restera confidentielle. Elle ne sera pas utilisée à des fins commerciales ou marketing. Consulter <a href=<?php echo get_privacy_policy_url();?>>notre politique de confidentialité</a></i>
                </div>
                <input class="cta-standard" type="submit" id="createUserSubmitBtn" value="Valider l'adresse-mail">
	    	<?php endif; ?>
		</form>

	</section><!-- .bloc-form -->

</div><!-- .template-form -->


<?php endwhile; endif; ?>
<?php 
if(get_field("footer") == "menu"):
    get_footer();
else:
    get_footer("partner"); 
endif;
?>
