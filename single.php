<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Susty
 */
wp_redirect(home_url(),301);
exit;
get_header();
?>

<div class="page-title wrapper">
	<?php if(is_singular()):?>
		<?php the_title( '<h1 class="print">', '</h1>' ); ?>
	<?php elseif(is_archive()):?>
		<h1 class="print"><?php echo single_cat_title( '', false );?></h1>
	<?php endif;?>
</div>

	<div id="primary">
		<main id="main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );

			//the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
